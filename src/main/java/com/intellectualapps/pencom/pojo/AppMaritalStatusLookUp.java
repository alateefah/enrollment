/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.pojo;

/**
 *
 * @author Lateefah
 */
public class AppMaritalStatusLookUp {
    private String maritalStatusId;    
    private String description;
    
    public AppMaritalStatusLookUp() {
    }

    public AppMaritalStatusLookUp(String maritalStatusId) {
        this.maritalStatusId = maritalStatusId;
    }

    public AppMaritalStatusLookUp(String maritalStatusId, String description) {
        this.maritalStatusId = maritalStatusId;
        this.description = description;
    }
    
    public void setMaritalStatusId(String maritalStatusId) {
        this.maritalStatusId = maritalStatusId;
    }
    
    public String getMaritalStatusId() {
        return maritalStatusId;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDescription() {
        return description;
    }
    
}
