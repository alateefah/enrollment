/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.pojo;

/**
 *
 * @author Lateefah
 */
public class AppGlLookUp {
    private Integer glId;    
    private String description;
    
    public AppGlLookUp() {
    }

    public AppGlLookUp(Integer glId) {
        this.glId = glId;
    }

    public AppGlLookUp(Integer glId, String description) {
        this.glId = glId;
        this.description = description;
    }

    public void setGlId(Integer glId) {
        this.glId = glId;
    }
    
    public Integer getGlId() {
        return glId;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDescription() {
        return description;
    }
}
