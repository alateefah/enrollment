/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.pojo;

/**
 *
 * @author Lateefah
 */
public class AppHonorificLookUp {
    private String honorificId;    
    private String description;
    
    public AppHonorificLookUp() {
    }

    public AppHonorificLookUp(String honorificId) {
        this.honorificId = honorificId;
    }

    public AppHonorificLookUp(String honorificId, String description) {
        this.honorificId = honorificId;
        this.description = description;
    }

    public void setHonorificId(String honorificId) {
        this.honorificId = honorificId;
    }
    
    public String getHonorificId() {
        return honorificId;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDescription() {
        return description;
    }
    
    
}
