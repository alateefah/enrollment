/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.pojo;

/**
 *
 * @author Lateefah
 */
public class AppRetirementModeLookUp {
    private Integer retirementModeId;    
    private String description;
    
    public AppRetirementModeLookUp() {
    }

    public AppRetirementModeLookUp(Integer retirementModeId) {
        this.retirementModeId = retirementModeId;
    }

    public AppRetirementModeLookUp(Integer retirementModeId, String description) {
        this.retirementModeId = retirementModeId;
        this.description = description;
    }
    
    public void setRetirementModeId(Integer retirementModeId) {
        this.retirementModeId = retirementModeId;
    }
    
    public Integer getRetirementModeId() {
        return retirementModeId;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDescription() {
        return description;
    }
    
}
