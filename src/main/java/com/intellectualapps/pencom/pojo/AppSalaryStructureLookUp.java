/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.pojo;

/**
 *
 * @author Lateefah
 */
public class AppSalaryStructureLookUp {
    private Integer salaryStructureId;    
    private String description;
    
    public AppSalaryStructureLookUp() {
    }

    public AppSalaryStructureLookUp(Integer salaryStructureId) {
        this.salaryStructureId = salaryStructureId;
    }

    public AppSalaryStructureLookUp(Integer salaryStructureId, String description) {
        this.salaryStructureId = salaryStructureId;
        this.description = description;
    }

    public void setSalaryStructureId(Integer salaryStructureId) {
        this.salaryStructureId = salaryStructureId;
    }
    
    public Integer getSalaryStructureId() {
        return salaryStructureId;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDescription() {
        return description;
    }
    
}
