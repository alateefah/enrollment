/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.pojo;

import com.intellectualapps.pencom.model.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author buls
 */

public class AppPensionerBasic implements Serializable{
    
    private String pensionerId;
    private String firstName;
    private String middleName;
    private String surname;
    private String genderId;
    private Integer serviceOrgId;
    private String motherMaidenName;
    private String honorificId;
    private Integer pfaId;
    private String rsaNumber;
    private String maritalStatusId;
    private Date dateOfBirth;
    private String email;
    private String contactAddress;
    private String contactPhoneNumber;
    private String academic;
    private String enroledBy;
    private Date dateEnroled;
    private AppPensionerEmployment pensionerEmployment;
    private HashMap<String, AppPensionerBiometric> pensionerBiometrics;

    public AppPensionerBasic() {
        pensionerBiometrics = new HashMap<String, AppPensionerBiometric>();
         pensionerId = "";
         firstName = "";
         middleName = "";
         surname = "";
         genderId = "";
         serviceOrgId = -1;
         motherMaidenName = "";
         honorificId = "";
         pfaId = -1;
         rsaNumber = "";
         maritalStatusId = "";
         dateOfBirth = null;
         email = "";
         contactAddress = "";
         contactPhoneNumber = "";
         academic = "";
         enroledBy = "";
         dateEnroled = null;
         pensionerEmployment = new AppPensionerEmployment();
    }
    
    public String getPensionerId() {
        return pensionerId;
    }

    public void setPensionerId(String pensinerId) {
        this.pensionerId = pensinerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getGenderId() {
        return genderId;
    }

    public void setGenderId(String genderId) {
        this.genderId = genderId;
    }

    public Integer getServiceOrgId() {
        return serviceOrgId;
    }

    public void setServiceOrgId(Integer serviceOrgId) {
        this.serviceOrgId = serviceOrgId;
    }

    public String getMotherMaidenName() {
        return motherMaidenName;
    }

    public void setMotherMaidenName(String motherMaidenName) {
        this.motherMaidenName = motherMaidenName;
    }

    public String getHonorificId() {
        return honorificId;
    }

    public void setHonorificId(String honorificId) {
        this.honorificId = honorificId;
    }

    public Integer getPfaId() {
        return pfaId;
    }

    public void setPfaId(Integer pfaId) {
        this.pfaId = pfaId;
    }

    public String getRsaNumber() {
        return rsaNumber;
    }

    public void setRsaNumber(String rsaNumber) {
        this.rsaNumber = rsaNumber;
    }

    public String getMaritalStatusId() {
        return maritalStatusId;
    }

    public void setMaritalStatusId(String maritalStatusId) {
        this.maritalStatusId = maritalStatusId;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactAddress() {
        return contactAddress;
    }

    public void setContactAddress(String contactAddress) {
        this.contactAddress = contactAddress;
    }

    public String getContactPhoneNumber() {
        return contactPhoneNumber;
    }

    public void setContactPhoneNumber(String contactPhoneNumber) {
        this.contactPhoneNumber = contactPhoneNumber;
    }

    public String getAcademic() {
        return academic;
    }

    public void setAcademic(String academic) {
        this.academic = academic;
    }

    public String getEnroledBy() {
        return enroledBy;
    }

    public void setEnroledBy(String enroledBy) {
        this.enroledBy = enroledBy;
    }

    public Date getDateEnroled() {
        return dateEnroled;
    }

    public void setDateEnroled(Date dateEnroled) {
        this.dateEnroled = dateEnroled;
    }    
    
    public AppPensionerEmployment getPensionerEmployment() {
        return pensionerEmployment;
    }

    public void setPensionerEmployment(AppPensionerEmployment pensionerEmployment) {
        if (pensionerEmployment != null) {
            this.pensionerEmployment = pensionerEmployment;
        }
    }     

    public HashMap<String, AppPensionerBiometric> getPensionerBiometrics() {
        return pensionerBiometrics;
    }

    public void setPensionerBiometrics(HashMap<String, AppPensionerBiometric> pensionerBiometrics) {
        this.pensionerBiometrics = pensionerBiometrics;
    }
    
    public void setPensionerBiometric(AppPensionerBiometric pensionerBiometrics) {
        if (pensionerBiometrics != null) {
            this.pensionerBiometrics.put(pensionerBiometrics.getSubTypeId(), pensionerBiometrics);
        }
    }
    
}
