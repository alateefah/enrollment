/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Role.java
 *
 * Created on Jul 4, 2016, 4:29:57 PM
 */
package com.intellectualapps.pencom.pojo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.intellectualapps.pencom.model.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author buls
 */

public class AppRole implements Serializable {
        
    private String roleId;
    private String description;
    private List<AppPrivilege> privileges;
    
    public AppRole() {
        privileges = new ArrayList<AppPrivilege>();
    }

    public AppRole(String roleId) {
        this.roleId = roleId;
    }

    public AppRole(String roleId, String description) {
        this.roleId = roleId;
        this.description = description;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<AppPrivilege> getPrivileges() {
        return privileges;
    }

    public void setPrivileges(List<AppPrivilege> privileges) {
        this.privileges = privileges;
    }
    
    public void setPrivilege(AppPrivilege privilege) {
        this.privileges.add(privilege);
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (roleId != null ? roleId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AppRole)) {
            return false;
        }
        AppRole other = (AppRole) object;
        if ((this.roleId == null && other.roleId != null) || (this.roleId != null && !this.roleId.equals(other.roleId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.intellectualapps.pencom.pojo.AppRole[ roleId=" + roleId + " ]";
    }

    public String getRolePrivilegesAsJson(){
        ObjectMapper jsonMapper = new ObjectMapper();
        String rolePrivilegesInString = "";
        try {
            rolePrivilegesInString = jsonMapper.writeValueAsString(getPrivileges());
        } catch (Exception e){
            
        }
        return rolePrivilegesInString;
    }
}
