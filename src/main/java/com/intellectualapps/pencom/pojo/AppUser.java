/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.pojo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.intellectualapps.pencom.model.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author buls
 */

public class AppUser implements Serializable{
    
    private String username;    
    private String firstName;    
    private String otherNames;    
    private String password;
    private List<AppRole> userRoles;
    
    public AppUser() {
        userRoles = new ArrayList<AppRole>();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getOtherNames() {
        return otherNames;
    }

    public void setOtherNames(String otherNames) {
        this.otherNames = otherNames;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<AppRole> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(List<AppRole> userRoles) {
        this.userRoles = userRoles;
    }
    
    public String getUserRolesAsJson(){
        ObjectMapper jsonMapper = new ObjectMapper();
        String userRolesInString = "";
        try {
            userRolesInString = jsonMapper.writeValueAsString(getUserRoles());
        } catch (Exception e){
            
        }
        
        return userRolesInString;
    }
}
