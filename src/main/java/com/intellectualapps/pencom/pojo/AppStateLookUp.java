/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.pojo;

/**
 *
 * @author Lateefah
 */
public class AppStateLookUp {
    private String stateId;    
    private String name;
    
    public AppStateLookUp() {
    }

    public AppStateLookUp(String stateId) {
        this.stateId = stateId;
    }

    public AppStateLookUp(String stateId, String name) {
        this.stateId = stateId;
        this.name = name;
    }
    
    public void setStateId(String stateId) {
        this.stateId = stateId;
    }
    
    public String getStateId() {
        return stateId;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    
}
