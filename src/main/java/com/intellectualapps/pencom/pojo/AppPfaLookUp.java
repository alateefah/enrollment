/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.pojo;

/**
 *
 * @author Lateefah
 */
public class AppPfaLookUp {
    private Integer pfaId;    
    private String name;
    
    public AppPfaLookUp() {
    }

    public AppPfaLookUp(Integer pfaId) {
        this.pfaId = pfaId;
    }

    public AppPfaLookUp(Integer pfaId, String name) {
        this.pfaId = pfaId;
        this.name = name;
    }
    
    public void setPfaId(Integer pfaId) {
        this.pfaId = pfaId;
    }
    
    public Integer getPfaId() {
        return pfaId;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
}
