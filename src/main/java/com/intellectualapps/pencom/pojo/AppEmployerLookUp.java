/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.pojo;

/**
 *
 * @author Lateefah
 */
public class AppEmployerLookUp {
    private Integer employerId;    
    private String employerName;
    
    public AppEmployerLookUp() {
    }

    public AppEmployerLookUp(Integer employerId) {
        this.employerId = employerId;
    }

    public AppEmployerLookUp(Integer employerId, String employerName) {
        this.employerId = employerId;
        this.employerName = employerName;
    }
    
    public void setEmployerId(Integer employerId) {
        this.employerId = employerId;
    }
    
    public Integer getEmployerId() {
        return employerId;
    }

    public void setEmployerName(String employerName) {
        this.employerName = employerName;
    }
    
    public String getEmployerName() {
        return employerName;
    }
    
}
