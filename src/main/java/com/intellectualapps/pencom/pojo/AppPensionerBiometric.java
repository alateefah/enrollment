/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Lateefah
 */
public class AppPensionerBiometric implements Serializable{
    
    private String pensionerId;    
    private String typeId;    
    private String subTypeId;    
    private String biometricData;
    private String enroledBy;
    private Date dateEnroled;
    
    public String getPensionerId() {
        return pensionerId;
    }

    public void setPensionerId(String pensionerId) {
        this.pensionerId = pensionerId;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getSubTypeId() {
        return subTypeId;
    }

    public void setSubTypeId(String subTypeId) {
        this.subTypeId = subTypeId;
    }
    
    public String getBiometricData() {
        return biometricData;
    }
    
    public void setBiometricData(String biometricData) {
        this.biometricData = biometricData;
    }

    public String getEnroledBy() {
        return enroledBy;
    }

    public void setEnroledBy(String enroledBy) {
        this.enroledBy = enroledBy;
    }        

    public Date getDateEnroled() {
        return dateEnroled;
    }

    public void setDateEnroled(Date dateEnroled) {
        this.dateEnroled = dateEnroled;
    }
        
}
