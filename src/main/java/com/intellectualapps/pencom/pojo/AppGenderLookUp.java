/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.pojo;

/**
 *
 * @author Lateefah
 */
public class AppGenderLookUp {
    private String genderId;    
    private String description;
    
    public AppGenderLookUp() {
    }

    public AppGenderLookUp(String genderId) {
        this.genderId = genderId;
    }

    public AppGenderLookUp(String genderId, String description) {
        this.genderId = genderId;
        this.description = description;
    }
    
    public void setGenderId(String genderId) {
        this.genderId = genderId;
    }
    
    public String getGenderId() {
        return genderId;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDescription() {
        return description;
    }
    
}
