/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.pojo;

import com.intellectualapps.pencom.model.*;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author buls
 */

public class AppPensionerEmployment implements Serializable{
    
    Integer employmentId;
    Integer employerId;    
    String employerFullAddress;
    String stateOfService;
    String staffId;
    Date expectedRetirementDate;
    Date firstAppointmentDate;
    Integer retirementModeId;
    Integer salaryStructure2004;
    Integer gradeLevel2004;
    Integer step2004;
    Integer salaryStructureConsolidated;
    Integer gradeLevelConsolidated;
    Integer stepConsolidated;
    Integer enhancedSalaryStructureConsolidated;
    Integer enhancedGradeLevelConsolidated;
    Integer enhancedStepConsolidated;
    Integer rankId;    

    public AppPensionerEmployment() {
        employmentId = -1;
        employerId = -1;    
        employerFullAddress = "";
        stateOfService = "";
        staffId = "";
        expectedRetirementDate = null;
        firstAppointmentDate = null;
        retirementModeId = -1;
        salaryStructure2004 = -1;
        gradeLevel2004 = -1;
        step2004 = 0;
        salaryStructureConsolidated = -1;
        gradeLevelConsolidated = -1;
        stepConsolidated = 0;
        enhancedSalaryStructureConsolidated = -1;
        enhancedGradeLevelConsolidated = -1;
        enhancedStepConsolidated = 0;
        rankId = -1;        
    }
    
    public Integer getEmploymentId() {
        return employmentId;
    }

    public void setEmploymentId(Integer employmentId) {
        this.employmentId = employmentId;
    }

    public Integer getEmployerId() {
        return employerId;
    }

    public void setEmployerId(Integer employerId) {
        this.employerId = employerId;
    }
    
    public String getEmployerFullAddress() {
        return employerFullAddress;
    }

    public void setEmployerFullAddress(String employerFullAddress) {
        this.employerFullAddress = employerFullAddress;
    }

    public String getStateOfService() {
        return stateOfService;
    }

    public void setStateOfService(String stateOfService) {
        this.stateOfService = stateOfService;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public Date getExpectedRetirementDate() {
        return expectedRetirementDate;
    }

    public void setExpectedRetirementDate(Date expectedRetirementDate) {
        this.expectedRetirementDate = expectedRetirementDate;
    }

    public Date getFirstAppointmentDate() {
        return firstAppointmentDate;
    }

    public void setFirstAppointmentDate(Date firstAppointmentDate) {
        this.firstAppointmentDate = firstAppointmentDate;
    }

    public Integer getRetirementModeId() {
        return retirementModeId;
    }

    public void setRetirementModeId(Integer retirementModeId) {
        this.retirementModeId = retirementModeId;
    }

    public Integer getSalaryStructure2004() {
        return salaryStructure2004;
    }

    public void setSalaryStructure2004(Integer salaryStructure2004) {
        this.salaryStructure2004 = salaryStructure2004;
    }

    public Integer getGradeLevel2004() {
        return gradeLevel2004;
    }

    public void setGradeLevel2004(Integer gradeLevel2004) {
        this.gradeLevel2004 = gradeLevel2004;
    }

    public Integer getStep2004() {
        return step2004;
    }

    public void setStep2004(Integer step2004) {
        this.step2004 = step2004;
    }

    public Integer getSalaryStructureConsolidated() {
        return salaryStructureConsolidated;
    }

    public void setSalaryStructureConsolidated(Integer salaryStructureConsolidated) {
        this.salaryStructureConsolidated = salaryStructureConsolidated;
    }

    public Integer getGradeLevelConsolidated() {
        return gradeLevelConsolidated;
    }

    public void setGradeLevelConsolidated(Integer gradeLevelConsolidated) {
        this.gradeLevelConsolidated = gradeLevelConsolidated;
    }

    public Integer getStepConsolidated() {
        return stepConsolidated;
    }

    public void setStepConsolidated(Integer stepConsolidated) {
        this.stepConsolidated = stepConsolidated;
    }

    public Integer getEnhancedSalaryStructureConsolidated() {
        return enhancedSalaryStructureConsolidated;
    }

    public void setEnhancedSalaryStructureConsolidated(Integer enhancedSalaryStructureConsolidated) {
        this.enhancedSalaryStructureConsolidated = enhancedSalaryStructureConsolidated;
    }

    public Integer getEnhancedGradeLevelConsolidated() {
        return enhancedGradeLevelConsolidated;
    }

    public void setEnhancedGradeLevelConsolidated(Integer enhancedGradeLevelConsolidated) {
        this.enhancedGradeLevelConsolidated = enhancedGradeLevelConsolidated;
    }

    public Integer getEnhancedStepConsolidated() {
        return enhancedStepConsolidated;
    }

    public void setEnhancedStepConsolidated(Integer enhancedStepConsolidated) {
        this.enhancedStepConsolidated = enhancedStepConsolidated;
    }

    public Integer getRankId() {
        return rankId;
    }

    public void setRankId(Integer rankId) {
        this.rankId = rankId;
    }
    
    
    
}
