/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.pojo;

/**
 *
 * @author Lateefah
 */
public class AppRankLookUp {
    private String rankId;    
    private String description;
    
    public AppRankLookUp() {
    }

    public AppRankLookUp(String rankId) {
        this.rankId = rankId;
    }

    public AppRankLookUp(String rankId, String description) {
        this.rankId = rankId;
        this.description = description;
    }

    public void setRankId(String rankId) {
        this.rankId = rankId;
    }
    
    public String getRankId() {
        return rankId;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDescription() {
        return description;
    }
    
}
