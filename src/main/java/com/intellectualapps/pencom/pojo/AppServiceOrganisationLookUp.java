/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.pojo;

/**
 *
 * @author Lateefah
 */
public class AppServiceOrganisationLookUp {
    private Integer serviceOrgId;    
    private String description;
    
    public AppServiceOrganisationLookUp() {
    }

    public AppServiceOrganisationLookUp(Integer serviceOrgId) {
        this.serviceOrgId = serviceOrgId;
    }

    public AppServiceOrganisationLookUp(Integer serviceOrgId, String description) {
        this.serviceOrgId = serviceOrgId;
        this.description = description;
    }

    public void setServiceOrgId(Integer serviceOrgId) {
        this.serviceOrgId = serviceOrgId;
    }
    
    public Integer getServiceOrgId() {
        return serviceOrgId;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDescription() {
        return description;
    }
}
