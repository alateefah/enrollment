/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.report.model;

import java.awt.Image;

/**
 *
 * @author buls
 */
public class PensionerSlip {
    
    private String pfa;
    private String rsaNumber;
    private String fullName;
    private String dateOfBirth;
    private String gender;
    private String employer;        
    private String employerCode;        
    private String gradeStep2004;
    private String gradeStep2007;
    private String gradeStep2010;
    private String retirementDate;
    private String retirementMode;
    private String dateOfFirstAppointment;
    private String pensionerId;
    private String registrationDate;
    private String registrationOfficer;    
    private Image photo;
    private Image logo;
    private Image watermark;

    public String getPfa() {
        return pfa;
    }

    public void setPfa(String pfa) {
        this.pfa = pfa;
    }

    public String getRsaNumber() {
        return rsaNumber;
    }

    public void setRsaNumber(String rsaNumber) {
        this.rsaNumber = rsaNumber;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmployer() {
        return employer;
    }

    public void setEmployer(String employer) {
        this.employer = employer;
    }

    public String getEmployerCode() {
        return employerCode;
    }

    public void setEmployerCode(String employerCode) {
        this.employerCode = employerCode;
    }

    public String getGradeStep2004() {
        return gradeStep2004;
    }

    public void setGradeStep2004(String gradeStep2004) {
        this.gradeStep2004 = gradeStep2004;
    }

    public String getGradeStep2007() {
        return gradeStep2007;
    }

    public void setGradeStep2007(String gradeStep2007) {
        this.gradeStep2007 = gradeStep2007;
    }

    public String getGradeStep2010() {
        return gradeStep2010;
    }

    public void setGradeStep2010(String gradeStep2010) {
        this.gradeStep2010 = gradeStep2010;
    }

    public String getRetirementDate() {
        return retirementDate;
    }

    public void setRetirementDate(String retirementDate) {
        this.retirementDate = retirementDate;
    }

    public String getRetirementMode() {
        return retirementMode;
    }

    public void setRetirementMode(String retirementMode) {
        this.retirementMode = retirementMode;
    }

    public String getDateOfFirstAppointment() {
        return dateOfFirstAppointment;
    }

    public void setDateOfFirstAppointment(String dateOfFirstAppointment) {
        this.dateOfFirstAppointment = dateOfFirstAppointment;
    }

    public String getPensionerId() {
        return pensionerId;
    }

    public void setPensionerId(String pensionerId) {
        this.pensionerId = pensionerId;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getRegistrationOfficer() {
        return registrationOfficer;
    }

    public void setRegistrationOfficer(String registrationOfficer) {
        this.registrationOfficer = registrationOfficer;
    }

    public Image getPhoto() {
        return photo;
    }

    public void setPhoto(Image photo) {
        this.photo = photo;
    }

    public Image getLogo() {
        return logo;
    }

    public void setLogo(Image logo) {
        this.logo = logo;
    }        

    public Image getWatermark() {
        return watermark;
    }

    public void setWatermark(Image watermark) {
        this.watermark = watermark;
    }        
    
}
