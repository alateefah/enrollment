/*
 * 20/07/2012
 */

package com.intellectualapps.pencom.report.manager;

import com.intellectualapps.pencom.util.ReportTemplateManager;
import com.intellectualapps.pencom.manager.AbstractReportManager;
import com.intellectualapps.pencom.report.model.PensionerSlip;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 *
 * @author buls
 */
public class PensionerSlipReportManager extends AbstractReportManager {
    private PensionerSlip pensionerSlip;
    private List<PensionerSlip> pensionerSlips;
    private String LOGO = "logo";
    private String WATERMARK = "watermark";
            
    public PensionerSlipReportManager(PensionerSlip pensionerSlip) {
        this.pensionerSlip = pensionerSlip;
        pensionerSlips = new ArrayList<PensionerSlip>();
        pensionerSlips.add(pensionerSlip);
    }

    public JasperPrint getJasperPrint () throws JRException {        
        Map<String, Object> params = new HashMap<String, Object>();
        
        params.put(LOGO, pensionerSlip.getLogo());
        params.put(WATERMARK, pensionerSlip.getWatermark());
        
        JasperPrint jasperPrint = JasperFillManager.fillReport(
                ReportTemplateManager.get().getJasperFile("slip"),
                params,                                   
                new JRBeanCollectionDataSource(pensionerSlips));

        return jasperPrint;
    }
}
