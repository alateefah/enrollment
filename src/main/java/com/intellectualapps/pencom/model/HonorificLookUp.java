/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.persistence.Id;

/**
 *
 * @author Lateefah
 */
@Entity
@Table(name = "honorific")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HonorificLookUp.findAll", query = "SELECT h FROM HonorificLookUp h")})

public class HonorificLookUp {
    @Id
    @NotNull    
    @Column(name = "honorific_id")
    private String honorificId;
    
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "descr")
    private String description;
    
    public HonorificLookUp() {
        
    }

    public void setHonorificId(String honorificId) {
        this.honorificId = honorificId;
    }
    
    public String getHonorificId() {
        return honorificId;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDescription() {
        return description;
    }
    
}
