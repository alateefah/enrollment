/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * RoleAndPrivilegePK.java
 *
 * Created on Jul 4, 2011, 4:22:50 PM
 */
package com.intellectualapps.pencom.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author buls
 */
@Embeddable
public class RolePrivilegePK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "role_id", nullable = false, length = 15)
    private String roleId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 32)
    @Column(name = "privelege_id", nullable = false, length = 32)
    private String privilegeId;

    public RolePrivilegePK() {
    }

    public RolePrivilegePK(String roleId, String privilegeId) {
        this.roleId = roleId;
        this.privilegeId = privilegeId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getPrivilegeId() {
        return privilegeId;
    }

    public void setPrivilegeId(String privilegeId) {
        this.privilegeId = privilegeId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (roleId != null ? roleId.hashCode() : 0);
        hash += (privilegeId != null ? privilegeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RolePrivilegePK)) {
            return false;
        }
        RolePrivilegePK other = (RolePrivilegePK) object;
        if ((this.roleId == null && other.roleId != null) || (this.roleId != null && !this.roleId.equals(other.roleId))) {
            return false;
        }
        if ((this.privilegeId == null && other.privilegeId != null) || (this.privilegeId != null && !this.privilegeId.equals(other.privilegeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.intellectualapps.pencom.model.RoleAndPrivilegePK[ roleId=" + roleId + ", privilegeId=" + privilegeId + " ]";
    }
    
}
