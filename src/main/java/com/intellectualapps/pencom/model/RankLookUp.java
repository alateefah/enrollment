/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Lateefah
 */

@Entity
@Table(name = "rank")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RankLookUp.findAll", query = "SELECT r FROM RankLookUp r")})

public class RankLookUp {
    @javax.persistence.Id
    @NotNull    
    @Column(name = "rank_id")
    private String rankId;
    
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "descr")
    private String description;
    
    public RankLookUp() {
        
    }

    public void setRankId(String rankId) {
        this.rankId = rankId;
    }
    
    public String getRankId() {
        return rankId;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDescription() {
        return description;
    }
    
}
