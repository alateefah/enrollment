/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.model;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Lateefah
 */

@Entity
@Table(name = "biometric")
public class PensionerBiometric implements Serializable{
    
    @Id
    @NotNull
    @Column(name = "pensioner_id")
    private String pensionerId;
    
    @NotNull
    @Column(name = "type_id")
    private String typeId;
    
    @NotNull
    @Column(name = "subtype_id")
    private String subTypeId;
    
    
    @Lob
    @Basic
    @NotNull
    @Column(name = "biometric_data")
    private String biometricData;
    
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "enroled_by")
    private String enroledBy;
    
    @NotNull
    @Column(name = "date_enroled")
    @Temporal(TemporalType.DATE)
    private Date dateEnroled;
    
    
    public PensionerBiometric() {
        
    }

    public String getPensionerId() {
        return pensionerId;
    }

    public void setPensionerId(String pensionerId) {
        this.pensionerId = pensionerId;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getSubTypeId() {
        return subTypeId;
    }

    public void setSubTypeId(String subTypeId) {
        this.subTypeId = subTypeId;
    }
    
    public String getBiometricData() {
        return biometricData;
    }
    
    public void setBiometricData(String biometricData) {
        this.biometricData = biometricData;
    }        

    public String getEnroledBy() {
        return enroledBy;
    }

    public void setEnroledBy(String enroledBy) {
        this.enroledBy = enroledBy;
    }        

    public Date getDateEnroled() {
        return dateEnroled;
    }

    public void setDateEnroled(Date dateEnroled) {
        this.dateEnroled = dateEnroled;
    }
    
}

