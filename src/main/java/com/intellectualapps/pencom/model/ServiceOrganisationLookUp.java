/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Lateefah
 */

@Entity
@Table(name = "service_org")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ServiceOrganisationLookUp.findAll", query = "SELECT so FROM ServiceOrganisationLookUp so")})

public class ServiceOrganisationLookUp implements Serializable {
    
    @Id
    @NotNull    
    @Column(name = "service_org_id")
    private Integer serviceOrgId;
    
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "descr")
    private String description;
    
    public ServiceOrganisationLookUp() {
        
    }

    public void setServiceOrgId(Integer serviceOrgId) {
        this.serviceOrgId = serviceOrgId;
    }
    
    public Integer getServiceOrgId() {
        return serviceOrgId;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDescription() {
        return description;
    }

}
