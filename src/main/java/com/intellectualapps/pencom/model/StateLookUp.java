/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
/**
 *
 * @author Lateefah
 */

@Entity
@Table(name = "state")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StateLookUp.findAll", query = "SELECT s FROM StateLookUp s")})

public class StateLookUp {
    @Id
    @NotNull    
    @Column(name = "state_id")
    private String stateId;
    
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "name")
    private String name;
    
    public StateLookUp() {
        
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }
    
    public String getStateId() {
        return stateId;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    
}
