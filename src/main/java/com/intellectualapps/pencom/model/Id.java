/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author buls
 */

@Entity
@Table(name = "id")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Id.findAll", query = "SELECT p FROM Id p")})
public class Id implements Serializable{
    
    @javax.persistence.Id
    @Basic(optional = false)
    @Column(name = "id")
    private String id;

    public Id(String id) {
        this.id = id;
    }
    
    public Id() {
        
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }        
    
}
