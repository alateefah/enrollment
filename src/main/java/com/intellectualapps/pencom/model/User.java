/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author buls
 */

@Entity
@Table(name = "user")

public class User implements Serializable{
    
    @Id
    @NotNull
    @Size(min = 1, max = 64)
    @Column(name = "user_id")
    private String username;
    
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "firstname")
    private String firstName;
    
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "othernames")
    private String otherNames;
    
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "password")
    private String password;
    
    public User() {
        
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getOtherNames() {
        return otherNames;
    }

    public void setOtherNames(String otherNames) {
        this.otherNames = otherNames;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
        
}
