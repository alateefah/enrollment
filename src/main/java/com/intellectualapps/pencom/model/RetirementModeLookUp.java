/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.persistence.Id;

/**
 *
 * @author Lateefah
 */

@Entity
@Table(name = "retirement_mode")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RetirementModeLookUp.findAll", query = "SELECT rm FROM RetirementModeLookUp rm"),
    @NamedQuery(name = "RetirementModeLookUp.findByRetirementModeId", query = "SELECT rm FROM RetirementModeLookUp rm WHERE rm.retirementModeId = :retirementModeId")})

public class RetirementModeLookUp {
    @Id
    @NotNull    
    @Size(min = 1, max = 5)
    @Column(name = "retirement_mode_id")
    private Integer retirementModeId;
    
    @NotNull
    @Size(min = 1, max = 7)
    @Column(name = "descr")
    private String description;
    
    public RetirementModeLookUp() {
        
    }

    public void setRetirementModeId(Integer retirementModeId) {
        this.retirementModeId = retirementModeId;
    }
    
    public Integer getRetirementModeId() {
        return retirementModeId;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDescription() {
        return description;
    }
    
}
