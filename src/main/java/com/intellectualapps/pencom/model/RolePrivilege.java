/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * RoleAndPrivilege.java
 *
 * Created on Jul 4, 2016, 4:19:50 PM
 */
package com.intellectualapps.pencom.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author buls
 */
@Entity
@Table(name = "role_privelege")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RolePrivilege.findAll", query = "SELECT r FROM RolePrivilege r"),
    @NamedQuery(name = "RolePrivilege.findByRoleId", query = "SELECT r FROM RolePrivilege r WHERE r.roleAndPrivilegePK.roleId = :roleId"),
    @NamedQuery(name = "RolePrivilege.findByPrivilegeId", query = "SELECT r FROM RolePrivilege r WHERE r.roleAndPrivilegePK.privilegeId = :privilegeId"),
    @NamedQuery(name = "RolePrivilege.findByRoleIdAndPrivilegeId", query = "SELECT r FROM RolePrivilege r WHERE r.roleAndPrivilegePK.privilegeId = :privilegeId AND r.roleAndPrivilegePK.roleId = :roleId")})
public class RolePrivilege implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RolePrivilegePK roleAndPrivilegePK;    
    @JoinColumn(name = "privelege_id", referencedColumnName = "privelege_id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Privilege privilege;
    @JoinColumn(name = "role_id", referencedColumnName = "role_id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Role role;

    public RolePrivilege() {
    }

    public RolePrivilege(RolePrivilegePK roleAndPrivilegePK) {
        this.roleAndPrivilegePK = roleAndPrivilegePK;
    }

    public RolePrivilege(String roleId, String privilegeId) {
        this.roleAndPrivilegePK = new RolePrivilegePK(roleId, privilegeId);
    }

    public RolePrivilegePK getRoleAndPrivilegePK() {
        return roleAndPrivilegePK;
    }

    public void setRoleAndPrivilegePK(RolePrivilegePK roleAndPrivilegePK) {
        this.roleAndPrivilegePK = roleAndPrivilegePK;
    }

    public Privilege getPrivilege() {
        return privilege;
    }

    public void setPrivilege(Privilege privilege) {
        this.privilege = privilege;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (roleAndPrivilegePK != null ? roleAndPrivilegePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RolePrivilege)) {
            return false;
        }
        RolePrivilege other = (RolePrivilege) object;
        if ((this.roleAndPrivilegePK == null && other.roleAndPrivilegePK != null) || (this.roleAndPrivilegePK != null && !this.roleAndPrivilegePK.equals(other.roleAndPrivilegePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.intellectualapps.pencom.model.RolePrivilege[ roleAndPrivilegePK=" + roleAndPrivilegePK + " ]";
    }
    
}
