/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.persistence.Id;

/**
 *
 * @author Lateefah
 */

@Entity
@Table(name = "pfa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PfaLookUp.findAll", query = "SELECT p FROM PfaLookUp p"),
    @NamedQuery(name = "PfaLookUp.findByPfaId", query = "SELECT p FROM PfaLookUp p WHERE p.pfaId = :pfaId")})

public class PfaLookUp implements Serializable{
    @Id
    @NotNull    
    @Column(name = "pfa_id")
    private Integer pfaId;
    
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "name")
    private String name;
    
    public PfaLookUp() {
        
    }

    public void setPfaId(Integer pfaId) {
        this.pfaId = pfaId;
    }
    
    public Integer getPfaId() {
        return pfaId;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
}
