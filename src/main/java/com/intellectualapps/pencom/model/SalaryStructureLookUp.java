/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.persistence.Id;

/**
 *
 * @author Lateefah
 */
@Entity
@Table(name = "salary_structure")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SalaryStructureLookUp.findAll", query = "SELECT s FROM SalaryStructureLookUp s"),
    @NamedQuery(name = "SalaryStructureLookUp.findByYear", query = "SELECT s FROM SalaryStructureLookUp s WHERE s.year = :year")})


public class SalaryStructureLookUp {
    @Id
    @NotNull    
    @Column(name = "salary_structure_id")
    private Integer salaryStructureId;
    
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "descr")
    private String description;
            
    @Column(name = "year")
    private Integer year;
    
    public SalaryStructureLookUp() {
        
    }

    public void setSalaryStructureId(Integer salaryStructureId) {
        this.salaryStructureId = salaryStructureId;
    }
    
    public Integer getSalaryStructureId() {
        return salaryStructureId;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDescription() {
        return description;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }
    
}
