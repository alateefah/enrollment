/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.persistence.Id;
/**
 *
 * @author Lateefah
 */
@Entity
@Table(name = "marital_status")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MaritalStatusLookUp.findAll", query = "SELECT m FROM MaritalStatusLookUp m")})

public class MaritalStatusLookUp {
    @Id
    @NotNull    
    @Size(min = 1, max = 5)
    @Column(name = "marital_status_id")
    private String maritalStatusId;
    
    @NotNull
    @Size(min = 1, max = 7)
    @Column(name = "descr")
    private String description;
    
    public MaritalStatusLookUp() {
        
    }

    public void setMaritalStatusId(String maritalStatusId) {
        this.maritalStatusId = maritalStatusId;
    }
    
    public String getMaritalStatusId() {
        return maritalStatusId;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDescription() {
        return description;
    }
}
