/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.persistence.Id;

/**
 *
 * @author Lateefah
 */
@Entity
@Table(name = "gl")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GlLookUp.findAll", query = "SELECT gl FROM GlLookUp gl")})


public class GlLookUp {
    @Id
    @NotNull    
    @Column(name = "gl_id")
    private Integer glId;
    
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "descr")
    private String description;
    
    public GlLookUp() {
        
    }

    public void setGlId(Integer glId) {
        this.glId = glId;
    }
    
    public Integer getGlId() {
        return glId;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDescription() {
        return description;
    }
    
}
