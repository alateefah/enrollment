/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * RolePrivilege.java
 *
 * Created on Jul 04, 2016, 2:55:50 PM
 */
package com.intellectualapps.pencom.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author buls
 */
@Entity
@Table(name = "privelege")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Privilege.findAll", query = "SELECT p FROM Privilege p"),
    @NamedQuery(name = "Privilege.findByPrivilegeId", query = "SELECT p FROM Privilege p WHERE p.privilegeId = :privilegeId"),
    @NamedQuery(name = "Privilege.findByDescription", query = "SELECT p FROM Privilege p WHERE p.description = :description")})
public class Privilege implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 32)
    @Column(name = "privelege_id", nullable = false, length = 32)
    private String privilegeId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "description", nullable = false, length = 255)
    private String description;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "menu_link", nullable = false, length = 255)
    private String menuLink;
    @Basic(optional = false)
    @Size(min = 1, max = 255)
    @Column(name = "menu_icon", nullable = false, length = 255)
    private String menuIcon;
    //@OneToMany(cascade = CascadeType.MERGE, mappedBy = "privelege")
    //private List<RolePrivilege> roleAndPrivilegeList;

    public Privilege() {
    }

    public Privilege(String privilegeId) {
        this.privilegeId = privilegeId;
    }

    public Privilege(String privilegeId, String description) {
        this.privilegeId = privilegeId;
        this.description = description;
    }

    public String getPrivilegeId() {
        return privilegeId;
    }

    public void setPrivilegeId(String privilegeId) {
        this.privilegeId = privilegeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMenuLink() {
        return menuLink;
    }

    public void setMenuLink(String menuLink) {
        this.menuLink = menuLink;
    }       
    
    public String getMenuIcon() {
        return menuIcon;
    }      
    
    public void setMenuIcon(String menuIcon) {
        this.menuIcon = menuIcon;
    }

 /*   @XmlTransient
    public List<RolePrivilege> getRoleAndPrivilegeList() {
        return roleAndPrivilegeList;
    }

    public void setRoleAndPrivilegeList(List<RolePrivilege> roleAndPrivilegeList) {
        this.roleAndPrivilegeList = roleAndPrivilegeList;
    }
*/
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (privilegeId != null ? privilegeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Privilege)) {
            return false;
        }
        Privilege other = (Privilege) object;
        if ((this.privilegeId == null && other.privilegeId != null) || (this.privilegeId != null && !this.privilegeId.equals(other.privilegeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.intellectualapps.pencom.model.Privilege[ privilegeId=" + privilegeId + " ]";
    }
    
}
