/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author buls
 */

@Entity
@Table(name = "pensioner")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PensionerBasic.findAll", query = "SELECT p FROM PensionerBasic p"),
    @NamedQuery(name = "PensionerBasic.findByPensionerId", query = "SELECT p FROM PensionerBasic p WHERE p.pensionerId = :pensionerId"),
    @NamedQuery(name = "PensionerBasic.findByPensionerIdFirstMiddleSurname", query = "SELECT p FROM PensionerBasic p WHERE p.pensionerId LIKE :pensionerId OR p.firstName LIKE :firstName OR p.middleName LIKE :middleName OR p.surname LIKE :surname"),
    @NamedQuery(name = "PensionerBasic.findByFirstMiddleSurname", query = "SELECT p FROM PensionerBasic p WHERE p.firstName LIKE :firstName OR p.middleName LIKE :middleName OR p.surname LIKE :surname")})
public class PensionerBasic implements Serializable{
    
    @Id
    @Basic(optional = false)
    @Column(name = "pensioner_id")
    private String pensionerId;
    @NotNull
    @Size(max = 50)
    @Column(name = "first_name")
    private String firstName;
    @NotNull
    @Size(max = 50)
    @Column(name = "middle_name")
    private String middleName;
    @NotNull
    @Size(max = 50)
    @Column(name = "surname")
    private String surname;
    @NotNull
    @Size(max = 50)
    @Column(name = "gender_id")
    private String genderId;
    @NotNull
    //@Size(max = 11)
    @Column(name = "service_org_id")
    private Integer serviceOrgId;
    @NotNull
    @Size(max = 150)
    @Column(name = "mother_maiden_name")
    private String motherMaidenName;
    @NotNull
    @Size(max = 4)
    @Column(name = "honorific_id")
    private String honorificId;
    @NotNull
    @Column(name = "pfa_id")
    private Integer pfaId;
    @NotNull
    @Size(max = 50)
    @Column(name = "rsa_number")
    private String rsaNumber;
    @NotNull
    @Size(max = 2)
    @Column(name = "marital_status_id")
    private String maritalStatusId;
    @NotNull
    @Column(name = "dob")
    @Temporal(TemporalType.DATE)
    private Date dateOfBirth;
    @NotNull
    @Size(max = 100)
    @Column(name = "email")
    private String email;
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(name = "contact_address")
    private String contactAddress;
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "contact_phone_number")
    private String contactPhoneNumber;
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "academic")
    private String academic;
    @NotNull    
    @Column(name = "employment_id")    
    private Integer employmentId;
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "enroled_by")
    private String enroledBy;
    @NotNull
    @Column(name = "date_enroled")
    @Temporal(TemporalType.DATE)
    private Date dateEnroled;    
    
    public String getPensionerId() {
        return pensionerId;
    }

    public void setPensionerId(String pensionerId) {
        this.pensionerId = pensionerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firsrtName) {
        this.firstName = firsrtName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getGenderId() {
        return genderId;
    }

    public void setGenderId(String genderId) {
        this.genderId = genderId;
    }

    public int getServiceOrgId() {
        return serviceOrgId;
    }

    public void setServiceOrgId(int serviceOrgId) {
        this.serviceOrgId = serviceOrgId;
    }

    public String getMotherMaidenName() {
        return motherMaidenName;
    }

    public void setMotherMaidenName(String motherMaidenName) {
        this.motherMaidenName = motherMaidenName;
    }

    public String getHonorificId() {
        return honorificId;
    }

    public void setHonorificId(String honorificId) {
        this.honorificId = honorificId;
    }

    public int getPfaId() {
        return pfaId;
    }

    public void setPfaId(int pfaId) {
        this.pfaId = pfaId;
    }

    public String getRsaNumber() {
        return rsaNumber;
    }

    public void setRsaNumber(String rsaNumber) {
        this.rsaNumber = rsaNumber;
    }

    public String getMaritalStatusId() {
        return maritalStatusId;
    }

    public void setMaritalStatusId(String maritalStatusId) {
        this.maritalStatusId = maritalStatusId;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactAddress() {
        return contactAddress;
    }

    public void setContactAddress(String contactAddress) {
        this.contactAddress = contactAddress;
    }

    public String getContactPhoneNumber() {
        return contactPhoneNumber;
    }

    public void setContactPhoneNumber(String contactPhoneNumber) {
        this.contactPhoneNumber = contactPhoneNumber;
    }

    public String getAcademic() {
        return academic;
    }

    public void setAcademic(String academic) {
        this.academic = academic;
    }

    public Integer getEmploymentId() {
        return employmentId;
    }

    public void setEmploymentId(Integer employmentId) {
        this.employmentId = employmentId;
    }

    public String getEnroledBy() {
        return enroledBy;
    }

    public void setEnroledBy(String enroledBy) {
        this.enroledBy = enroledBy;
    }    

    public Date getDateEnroled() {
        return dateEnroled;
    }

    public void setDateEnroled(Date dateEnroled) {
        this.dateEnroled = dateEnroled;
    }
    
    
}
