/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.persistence.Id;
/**
 *
 * @author Lateefah
 */
@Entity
@Table(name = "gender")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GenderLookUp.findAll", query = "SELECT g FROM GenderLookUp g")})

public class GenderLookUp {
    @Id
    @NotNull    
    @Size(min = 1, max = 5)
    @Column(name = "gender_id")
    private String genderId;
    
    @NotNull
    @Size(min = 1, max = 7)
    @Column(name = "descr")
    private String description;
    
    public GenderLookUp() {
        
    }

    public void setGenderId(String genderId) {
        this.genderId = genderId;
    }
    
    public String getGenderId() {
        return genderId;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDescription() {
        return description;
    }
}
