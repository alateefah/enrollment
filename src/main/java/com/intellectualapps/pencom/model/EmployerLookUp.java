/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.persistence.Id;
/**
 *
 * @author Lateefah
 */
@Entity
@Table(name = "employer")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EmployerLookUp.findAll", query = "SELECT e FROM EmployerLookUp e"),
    @NamedQuery(name = "EmployerLookUp.findByEmployerId", query = "SELECT e FROM EmployerLookUp e WHERE e.employerId = :employerId")})

public class EmployerLookUp {
    
    @Id
    @NotNull    
    @Column(name = "employer_id")
    private Integer employerId;
    
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "employer_name")
    private String employerName;
    
    public EmployerLookUp() {
        
    }

    public void setEmployerId(Integer employerId) {
        this.employerId = employerId;
    }
    
    public Integer getEmployerId() {
        return employerId;
    }

    public void setEmployerName(String employerName) {
        this.employerName = employerName;
    }
    
    public String getEmployerName() {
        return employerName;
    }
    
}
