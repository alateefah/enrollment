/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.config;

import com.intellectualapps.pencom.service.BiometricService;
import com.intellectualapps.pencom.service.PrivilegeService;
import com.intellectualapps.pencom.service.RoleService;
import com.intellectualapps.pencom.service.UserService;
import com.intellectualapps.pencom.web.controller.WebBackend;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author buls
 */

@javax.ws.rs.ApplicationPath("/api")
public class ApplicationConfig extends Application {

    @Override    
    public Set<Class<?>> getClasses() {
        Set<Class<?>> s = new HashSet<Class<?>>();
        s.add(UserService.class);
        s.add(PrivilegeService.class);        
        s.add(RoleService.class);
        s.add(BiometricService.class);
        return s;
    }
    
     

}