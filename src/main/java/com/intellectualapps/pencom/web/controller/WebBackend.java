/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.web.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.intellectualapps.pencom.manager.EmployerLookUpManagerLocal;
import com.intellectualapps.pencom.manager.GenderLookUpManagerLocal;
import com.intellectualapps.pencom.manager.GlLookUpManagerLocal;
import com.intellectualapps.pencom.manager.HonorificLookUpManagerLocal;
import com.intellectualapps.pencom.manager.IdGeneratorManagerLocal;
import com.intellectualapps.pencom.manager.MaritalStatusLookUpManagerLocal;
import com.intellectualapps.pencom.manager.PfaLookUpManager;
import com.intellectualapps.pencom.service.UserService;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.intellectualapps.pencom.manager.UserManagerLocal;
import com.intellectualapps.pencom.pojo.AppPrivilege;
import com.intellectualapps.pencom.pojo.AppRole;
import com.intellectualapps.pencom.pojo.AppUser;
import com.intellectualapps.pencom.pojo.AppPensionerBiometric;
import java.util.List;
import javax.servlet.http.HttpSession;
import com.intellectualapps.pencom.manager.PensionerManagerLocal;
import com.intellectualapps.pencom.pojo.AppPensionerBasic;
import java.text.ParseException;
import com.intellectualapps.pencom.manager.PensionerBiometricManagerLocal;
import com.intellectualapps.pencom.manager.PensionerSlipManagerLocal;
import com.intellectualapps.pencom.manager.PrivilegeManagerLocal;
import com.intellectualapps.pencom.manager.RoleManagerLocal;
import com.intellectualapps.pencom.manager.ServiceOrganisationLookUpManagerLocal;
import com.intellectualapps.pencom.model.Role;
import com.intellectualapps.pencom.model.ServiceOrganisationLookUp;
import com.intellectualapps.pencom.model.User;
import com.intellectualapps.pencom.model.UserRole;
import com.intellectualapps.pencom.pojo.AppPfaLookUp;
import com.intellectualapps.pencom.pojo.AppServiceOrganisationLookUp;
import com.intellectualapps.pencom.util.ServerResponse;
import com.intellectualapps.pencom.util.StatusType;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ArrayList;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import com.intellectualapps.pencom.manager.PfaLookUpManagerLocal;
import com.intellectualapps.pencom.manager.RankLookUpManagerLocal;
import com.intellectualapps.pencom.manager.RetirementModeLookUpManagerLocal;
import com.intellectualapps.pencom.manager.SalaryStructureLookUpManagerLocal;
import com.intellectualapps.pencom.manager.StateLookUpManagerLocal;
import com.intellectualapps.pencom.pojo.AppEmployerLookUp;
import com.intellectualapps.pencom.pojo.AppGenderLookUp;
import com.intellectualapps.pencom.pojo.AppGlLookUp;
import com.intellectualapps.pencom.pojo.AppHonorificLookUp;
import com.intellectualapps.pencom.pojo.AppMaritalStatusLookUp;
import com.intellectualapps.pencom.pojo.AppRankLookUp;
import com.intellectualapps.pencom.pojo.AppRetirementModeLookUp;
import com.intellectualapps.pencom.pojo.AppSalaryStructureLookUp;
import com.intellectualapps.pencom.pojo.AppStateLookUp;


/**
 *
 * @author buls
 */
@WebServlet(name = "WebBackend", urlPatterns = {"/webbackend/*"})
public class WebBackend extends HttpServlet {  
    
    @EJB    
    UserManagerLocal userManager;
    @EJB
    PensionerBiometricManagerLocal biometricManager;
    @EJB
    PensionerManagerLocal pensionerManager;
    @EJB
    IdGeneratorManagerLocal idGeneratormanager;
    @EJB
    RoleManagerLocal roleManager;
    @EJB
    PrivilegeManagerLocal privilegeManager;
    @EJB
    PensionerSlipManagerLocal pensionerSlipManager;
    @EJB
    ServiceOrganisationLookUpManagerLocal serviceOrganisationLookUpManager;
    @EJB
    PfaLookUpManagerLocal pfaLookUpManager;
    @EJB
    HonorificLookUpManagerLocal honorificLookUpManager;
    @EJB
    GenderLookUpManagerLocal genderLookUpManager;
    @EJB
    MaritalStatusLookUpManagerLocal maritalStatusLookUpManager;
    @EJB
    EmployerLookUpManagerLocal employerLookUpManager;
    @EJB
    StateLookUpManagerLocal stateLookUpManager;
    @EJB
    RetirementModeLookUpManagerLocal retirementModeLookUpManager;
    @EJB
    SalaryStructureLookUpManagerLocal salaryStructureLookUpManager;
    @EJB
    GlLookUpManagerLocal glLookUpManager;
    @EJB
    RankLookUpManagerLocal rankLookUpManager;
    
    private String PAGE_SECTION_KEY = "main";
    private String PRIVILEGES_KEY = "privileges";
    private String DASHBOARD_PAGE = "WEB-INF/include/dashboard.jsp";
    private String MANAGE_PAGE = "WEB-INF/include/management.jsp";
    private String ENROLMENT_PAGE = "WEB-INF/include/enrollment.jsp";
    private String SEARCH_PAGE = "WEB-INF/include/searchresult.jsp";
    private String PAGE_FRAME = "page-frame.jsp";
    private String INDEX_PAGE = "index.jsp";
    private String USERNAME = "username";
    private String PASSWORD = "password";
    private String ERROR_MSG_KEY = "errormsg";
    private String SUCCESS_MSG_KEY = "successmsg";
    private String ERROR_MSG = "Invalid username or password";
    private String ACCESS_DENIED_MSG = "Access denied";
    private String SESSION_EXPIRED_MSG = "Your session has expired";
    private String UNKNOWN_INTENTION_MSG = "Unknown action";
    private String LOG_OUT_MSG = "You have been successfully logged out";
    private String USER_ID_KEY = "userId";
    private String REGISTER_INTENTION = "register";
    private String MANAGE_INTENTION = "manage";
    private String SHOW_DASHBOARD = "dashboard";
    private String SEARCH_INTENTION = "search";
    private String PRINT_SLIP_INTENTION = "print-slip";
    private String LOG_OUT = "logout";
    private String INTENTION_KEY = "intention";
    private String PENSIONER_KEY = "pensioner";
    private String APP_USERS_KEY = "system-users";
    private String LOGIN_INTENTION = "login";
    private String SAVE_BIOMETRIC_INTENTION = "save-biometric";
    private String SAVE_PENSIONER_INTENTION = "save-pensioner";
    private String PENSIONER_ID = "pensioner-id";
    private String SUBTYPE_ID = "subtype-id";
    private String TYPE_ID = "type-id";
    private String BIOMETRIC_DATA = "biometric-data";    
    private String FIRST_NAME = "first-name";
    private String MIDDLE_NAME = "middle-name";
    private String SURNAME = "surname";
    private String GENDER_ID = "gender-id";
    private String SERVICE_ORG_ID = "service-org-id";
    private String MOTHER_MAIDEN_NAME = "mother-maiden-name";
    private String HONORIFIC_ID = "honorific-id";
    private String PFA_ID = "pfa-id";
    private String RSA_NUMBER = "rsa-number";
    private String MARITAL_STATUD_ID = "marital-status-id";
    private String DATE_OF_BIRTH = "date-of-birth";
    private String EMAIL = "email";
    private String CONTACT_ADDRESS = "contact-address";
    private String CONTACT_PHONE_NUMBER = "contact-phone-number";
    private String ACADEMIC = "academic";    
    private String EMPLOYER_ID = "employer-id";
    private String EMPLOYER_ADDRESS = "employer-address";
    private String STATE_OF_SERVICE = "state-of-service";
    private String STAFF_ID = "staff-id";
    private String EXPECTED_RETIREMENT_DATE = "expected-retirement-date";
    private String FIRST_APPOINTMENT_DATE = "first-appointment-date";
    private String RETIREMENT_MODE_ID = "retirement-mode-id";
    private String SAL_STRUCTURE_2004 = "sal-structure-2004";
    private String GRADE_LEVEL_2004 = "grade-level-2004";
    private String STEP_2004 = "step-2004";
    private String SAL_STRUCTURE_CONSOLIDATED = "sal-structure-con";
    private String GRADE_LEVEL_CONSOLIDATED = "grade-level-con";
    private String STEP_CONSOLIDATED = "step-con";
    private String SAL_STRUCTURE_ENHANCED_CONSOLIDATED = "sal-structure-ehn-con";
    private String GRADE_LEVEL_ENHANCED_CONSOLIDATED = "grade-level-ehn-con";
    private String STEP_ENHANCED_CONSOLIDATED = "step-ehn-con";
    private String RANK_ID = "rank-id";    
    private String ADD_USER_INTENTION = "add-user";
    private String OTHERNAMES = "other-names";
    private String ADD_USER_SUCCESS_MSG = "User added successfully";
    private String ADD_USER_ERROR_MSG = "User cannot be added at this time";
    private String GET_USERS_INTENTION = "get-users";
    private String DELETE_USER_INTENTION = "delete-user";
    private String SEARCH_INPUT = "search-input";
    private String GENERATE_IDS = "generate-ids";
    private String UPDATE_USER_INTENTION = "update-user";
    private String GET_ROLES_INTENTION = "get-roles";
    private String APP_ROLES_KEY = "system-roles";
    private String ADD_ROLE_INTENTION = "add-role";
    private String ROLE_ID = "role-id";
    private String ROLE_DESCRIPTION = "role-description";
    private String DELETE_ROLE_INTENTION = "delete-role";
    private String UPDATE_ROLE_INTENTION = "update-role";
    private String APP_PRIVILEGE_KEY="system-privileges";
    private String GET_PRIVILEGES_INTENTION = "get-privileges";
    private String ADD_PRIVILEGE_INTENTION = "add-privilege";
    private String PRIVILEGE_ID = "privilege-id";
    private String PRIVILEGE_DESCRIPTION = "privilege-description";
    private String PRIVILEGE_MENU_LINK = "privilege-menuLink";
    private String PRIVILEGE_MENU_ICON = "privilege-menuIcon";
    private String DELETE_PRIVILEGE_INTENTION = "delete-privilege";
    private String UPDATE_PRIVILEGE_INTENTION = "update-privilege";
    private String ADD_USERROLE_INTENTION = "add-user-role";
    private String DELETE_USERROLE_INTENTION = "delete-user-role";
    private String LOGO_PATH = "/assets/images/logo.png";
    private String WATERMARK_PATH = "/assets/images/coa_light.png";
    private String ADD_ROLE_PRIVILEGE_INTENTION = "add-role-privilege";
    private String DELETE_ROLE_PRIVILEGE_INTENTION = "delete-role-privilege";
    private String SERVICE_ORGANISATIONS_KEY = "service-organisations";
    private String PFAS_KEY = "pfas";
    private String HONORIFICS_KEY = "honorifics";
    private String GENDERS_KEY = "genders";
    private String MARITAL_STATUSES_KEY = "marital-statuses";
    private String EMPLOYERS_KEY = "employers";
    private String STATES_KEY= "states";
    private String RETIREMENT_MODES_KEY= "retirement-modes";
    private String SALARY_STRUCTURES_KEY = "salary-structures";
    private String CONSOLIDATED_SALARY_STRUCTURES_KEY = "consolidated-salary-structures";
    private String ENHANCED_CONSOLIDATED_SALARY_STRUCTURES_KEY = "enhanced-consolidated-salary-structures";
    private String GLS_KEY = "gls";
    private String RANKS_KEY = "ranks";
    private String VIEW_PENSIONER_INTENTION = "view-pensioner";
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {        
        String intention = request.getParameter("intention");
        
        ObjectMapper jsonMapper = new ObjectMapper();        
                
        if (intention.equals(REGISTER_INTENTION)) {
            AppUser appUser = getAppUserFromSession(request);
            if (appUser != null) {
                if (userHasPrivilege(intention, request, appUser)) { 
                    showRegistrationPage(appUser, null, request, response);
                } else {
                    request.setAttribute(PRIVILEGES_KEY, appUser.getUserRoles());
                    request.setAttribute(PAGE_SECTION_KEY, DASHBOARD_PAGE);
                    request.getRequestDispatcher(PAGE_FRAME).forward(request, response);
                }            
            } else {
                request.setAttribute(ERROR_MSG_KEY, ACCESS_DENIED_MSG);
                request.getRequestDispatcher(INDEX_PAGE).forward(request, response); 
            }
        } else if (intention.equals(SEARCH_INTENTION)) {
            AppUser appUser = getAppUserFromSession(request);
            if (appUser != null) {
                if (userHasPrivilege(intention, request, appUser)) { 
                    request.setAttribute(PRIVILEGES_KEY, appUser.getUserRoles());
                    request.setAttribute(PAGE_SECTION_KEY, SEARCH_PAGE); 
                    request.setAttribute(INTENTION_KEY, SEARCH_INTENTION); 
                    request.getRequestDispatcher(PAGE_FRAME).forward(request, response);
                } else {
                    request.setAttribute(PRIVILEGES_KEY, appUser.getUserRoles());
                    request.setAttribute(PAGE_SECTION_KEY, DASHBOARD_PAGE);
                    request.getRequestDispatcher(PAGE_FRAME).forward(request, response);
                }            
            } else {
                request.setAttribute(ERROR_MSG_KEY, ACCESS_DENIED_MSG);
                request.getRequestDispatcher(INDEX_PAGE).forward(request, response); 
            }
        } else if (intention.equals(SHOW_DASHBOARD)) {            
            AppUser appUser = getAppUserFromSession(request);
            if (appUser != null) {
                request.setAttribute(PRIVILEGES_KEY, appUser.getUserRoles());
                request.setAttribute(PAGE_SECTION_KEY, DASHBOARD_PAGE);
                request.getRequestDispatcher(PAGE_FRAME).forward(request, response);
            } else {
                request.setAttribute(ERROR_MSG_KEY, ACCESS_DENIED_MSG);
                request.getRequestDispatcher(INDEX_PAGE).forward(request, response); 
            }
        } else if (intention.equals(MANAGE_INTENTION)) {            
            AppUser appUser = getAppUserFromSession(request);
            
            if (appUser != null) {
                if (userHasPrivilege(intention, request, appUser)) { 
                    List<AppUser> appUsers = userManager.getAllUsers();
                    List<AppRole> appRoles = roleManager.getAllRoles();
                    List<AppPrivilege> appPrivileges = privilegeManager.getAllPrivileges();
                    request.setAttribute(PRIVILEGES_KEY, appUser.getUserRoles());
                    request.setAttribute(PAGE_SECTION_KEY, MANAGE_PAGE);
                    request.setAttribute(APP_USERS_KEY, appUsers);
                    request.setAttribute(APP_ROLES_KEY, appRoles);
                    request.setAttribute(APP_PRIVILEGE_KEY, appPrivileges);
                    request.getRequestDispatcher(PAGE_FRAME).forward(request, response);
                } else {
                    request.setAttribute(PRIVILEGES_KEY, appUser.getUserRoles());
                    request.setAttribute(PAGE_SECTION_KEY, DASHBOARD_PAGE);
                    request.getRequestDispatcher(PAGE_FRAME).forward(request, response);
                }
            } else {
                request.setAttribute(ERROR_MSG_KEY, ACCESS_DENIED_MSG);
                request.getRequestDispatcher(INDEX_PAGE).forward(request, response); 
            }
        } else if(intention.equals(GET_USERS_INTENTION)){
            AppUser appUser = getAppUserFromSession(request);
            ServerResponse serverResponse = new ServerResponse();
           
            if (appUser != null) {
                PrintWriter out = response.getWriter();
                if (userHasPrivilege(MANAGE_INTENTION, request, appUser)) {
                    List<AppUser> appUsers = userManager.getAllUsers();
                    serverResponse.setStatus(StatusType.SUCCESS.getValue());
                    serverResponse.setStatusMessage(StatusType.SUCCESS_MESSAGE.getValue());
                    List<Object> payload = new ArrayList<Object>();
                    payload.add(appUsers);
                    serverResponse.setPayLoad(payload);                   
                } else {
                    serverResponse.setStatus(StatusType.FAILED.getValue());
                    serverResponse.setStatusMessage(StatusType.FAILED_MESSAGE.getValue());                    
                }
                
                out.write(jsonMapper.writeValueAsString(serverResponse));
            } else {
                request.setAttribute(ERROR_MSG_KEY, ACCESS_DENIED_MSG);
                request.getRequestDispatcher(PAGE_FRAME).forward(request, response); 
            }
        } else if(intention.equals(GET_PRIVILEGES_INTENTION)){
            AppUser appUser = getAppUserFromSession(request);
            ServerResponse serverResponse = new ServerResponse();
           
            if (appUser != null) {
                PrintWriter out = response.getWriter();
                if (userHasPrivilege(MANAGE_INTENTION, request, appUser)) {
                    List<AppPrivilege> appPrivileges = privilegeManager.getAllPrivileges();
                    serverResponse.setStatus(StatusType.SUCCESS.getValue());
                    serverResponse.setStatusMessage(StatusType.SUCCESS_MESSAGE.getValue());
                    List<Object> payload = new ArrayList<Object>();
                    payload.add(appPrivileges);
                    serverResponse.setPayLoad(payload);                                       
                } else {
                    serverResponse.setStatus(StatusType.FAILED.getValue());
                    serverResponse.setStatusMessage(StatusType.FAILED_MESSAGE.getValue());                    
                }
                out.write(jsonMapper.writeValueAsString(serverResponse)); 
            } else {
                request.setAttribute(ERROR_MSG_KEY, ACCESS_DENIED_MSG);
                request.getRequestDispatcher(PAGE_FRAME).forward(request, response); 
            }
        } else if(intention.equals(GET_ROLES_INTENTION)){
            AppUser appUser = getAppUserFromSession(request);
            ServerResponse serverResponse = new ServerResponse();
           
            if (appUser != null) {
                PrintWriter out = response.getWriter();
                if (userHasPrivilege(MANAGE_INTENTION, request, appUser)) {
                    List<AppRole> appRoles = roleManager.getAllRoles();
                    serverResponse.setStatus(StatusType.SUCCESS.getValue());
                    serverResponse.setStatusMessage(StatusType.SUCCESS_MESSAGE.getValue());
                    List<Object> payload = new ArrayList<Object>();
                    payload.add(appRoles);
                    serverResponse.setPayLoad(payload);                                       
                } else {
                    serverResponse.setStatus(StatusType.FAILED.getValue());
                    serverResponse.setStatusMessage(StatusType.FAILED_MESSAGE.getValue());                    
                }
                out.write(jsonMapper.writeValueAsString(serverResponse)); 
            } else {
                request.setAttribute(ERROR_MSG_KEY, ACCESS_DENIED_MSG);
                request.getRequestDispatcher(PAGE_FRAME).forward(request, response); 
            }
        } else if (intention.equals(LOG_OUT)) {
            HttpSession session = request.getSession(false);
            if (session != null) {
                session.invalidate();
                session = null;
            }
            request.setAttribute(ERROR_MSG_KEY, LOG_OUT_MSG);
            request.getRequestDispatcher(INDEX_PAGE).forward(request, response);         
        } else if (intention.equals(GENERATE_IDS)) {
            idGeneratormanager.generateIds(100);
        } else if (intention.equals(PRINT_SLIP_INTENTION)){
            String pensionerId = request.getParameter(PENSIONER_ID);                        
            
            AppUser appUser = getAppUserFromSession(request);
            if (appUser != null) {                
                if (userHasPrivilege(REGISTER_INTENTION, request, appUser)) {                                        
                    response.setContentType("text/html;charset=UTF-8");                    
                    ServletOutputStream reportOutputStream = response.getOutputStream();

                    byte[] report = null;                            
                    URL logoUrl = getServletContext().getResource(LOGO_PATH);
                    URL watermarkUrl = getServletContext().getResource(WATERMARK_PATH);                    
                    if (pensionerId != null && !pensionerId.equals("undefined") && !pensionerId.equals("")) {
                        report = pensionerSlipManager.getPensionerSlip(pensionerId, logoUrl, watermarkUrl);
                    } else {
                        reportOutputStream.println("No Pensioner ID supplied");
                        return;
                    }
                    
                    response.setContentType("application/pdf");
                    response.addHeader("Content-Disposition", "inline; filename=\"" + pensionerId + ".pdf");
                    response.setContentLength(report.length);

                    ByteArrayInputStream bais = new ByteArrayInputStream(report);
                    BufferedInputStream bufStream = new BufferedInputStream(bais);

                    try {
                        int readBytes = 0;
                        while ((readBytes = bufStream.read()) != -1) {
                            reportOutputStream.write(readBytes);
                        }
                    } finally {
                        reportOutputStream.close();
                        bais.close();
                        bufStream.close();
                    }
                } else {
                    request.setAttribute(PRIVILEGES_KEY, appUser.getUserRoles());
                    request.setAttribute(PAGE_SECTION_KEY, DASHBOARD_PAGE);
                    request.getRequestDispatcher(PAGE_FRAME).forward(request, response);
                }
            } else {
                request.setAttribute(ERROR_MSG_KEY, ACCESS_DENIED_MSG);
                request.getRequestDispatcher(INDEX_PAGE).forward(request, response); 
            } 
        } else if (intention.equals(VIEW_PENSIONER_INTENTION)) {            
            AppUser appUser = getAppUserFromSession(request);
            
            if (appUser != null) {
                if (userHasPrivilege(SEARCH_INTENTION, request, appUser)) { 
                    String pensionerId = request.getParameter(PENSIONER_ID);                    
                    AppPensionerBasic appPensioner = pensionerManager.getAppPensionerBasic(pensionerId);                    
                    showRegistrationPage(appUser, appPensioner, request, response);
                } else {
                    request.setAttribute(PRIVILEGES_KEY, appUser.getUserRoles());
                    request.setAttribute(PAGE_SECTION_KEY, DASHBOARD_PAGE);
                    request.getRequestDispatcher(PAGE_FRAME).forward(request, response);
                }
            } else {
                request.setAttribute(ERROR_MSG_KEY, ACCESS_DENIED_MSG);
                request.getRequestDispatcher(INDEX_PAGE).forward(request, response); 
            }
        } else {
            request.setAttribute(ERROR_MSG_KEY, UNKNOWN_INTENTION_MSG);
            request.getRequestDispatcher(INDEX_PAGE).forward(request, response); 
        } 
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {   
        ObjectMapper jsonMapper = new ObjectMapper();
        PrintWriter out = response.getWriter();
        String intention = request.getParameter("intention");        
                
        if (intention.equals(LOGIN_INTENTION)) {
            String username = request.getParameter(USERNAME);
            String password = request.getParameter(PASSWORD);

            AppUser appUser = userManager.authenticateUser(username, password);
            if (appUser != null) {
                HttpSession session = request.getSession(true);
                session.setAttribute(appUser.getUsername(), appUser);
                session.setAttribute(USER_ID_KEY, appUser.getUsername());            
                request.setAttribute(PRIVILEGES_KEY, appUser.getUserRoles());
                request.setAttribute(PAGE_SECTION_KEY, DASHBOARD_PAGE);
                request.getRequestDispatcher(PAGE_FRAME).forward(request, response);
            } else {
                request.setAttribute(ERROR_MSG_KEY, ERROR_MSG);
                request.getRequestDispatcher(INDEX_PAGE).forward(request, response);
            }
        } else if (intention.equals(REGISTER_INTENTION)){ 
            String pensionerId = request.getParameter(PENSIONER_ID);
            String firstName = request.getParameter(FIRST_NAME);
            String middleName = request.getParameter(MIDDLE_NAME);
            String surname = request.getParameter(SURNAME);
            String genderId = request.getParameter(GENDER_ID);
            String serviceOrgId = request.getParameter(SERVICE_ORG_ID);
            String motherMaidenName = request.getParameter(MOTHER_MAIDEN_NAME);
            String honorificId = request.getParameter(HONORIFIC_ID);
            String pfaId = request.getParameter(PFA_ID);
            String rsaNumber = request.getParameter(RSA_NUMBER);
            String maritalStatusId = request.getParameter(MARITAL_STATUD_ID);
            String dateOfBirth = request.getParameter(DATE_OF_BIRTH);
            String email = request.getParameter(EMAIL);
            String contactAddress = request.getParameter(CONTACT_ADDRESS);
            String contactPhoneNumber = request.getParameter(CONTACT_PHONE_NUMBER);
            String academic = request.getParameter(ACADEMIC);
            
            String employerId = request.getParameter(EMPLOYER_ID);
            String employerAddress = request.getParameter(EMPLOYER_ADDRESS);
            String stateOfService = request.getParameter(STATE_OF_SERVICE);
            String staffId = request.getParameter(STAFF_ID);
            String expectedRetirementDate = request.getParameter(EXPECTED_RETIREMENT_DATE);
            String firstAppointmentDate = request.getParameter(FIRST_APPOINTMENT_DATE);
            String retirementModeId = request.getParameter(RETIREMENT_MODE_ID);
            String salaryStructure2004 = request.getParameter(SAL_STRUCTURE_2004);
            String gradeLevel2004 = request.getParameter(GRADE_LEVEL_2004);
            String step2004 = request.getParameter(STEP_2004);
            String salaryStructureConsolidated = request.getParameter(SAL_STRUCTURE_CONSOLIDATED);
            String gradeLevelConsolidated = request.getParameter(GRADE_LEVEL_CONSOLIDATED);
            String stepConsolidated = request.getParameter(STEP_CONSOLIDATED);
            String salaryStructureEnhancedConsolidated = request.getParameter(SAL_STRUCTURE_ENHANCED_CONSOLIDATED);
            String gradeLevelEnhancedConsolidated = request.getParameter(GRADE_LEVEL_ENHANCED_CONSOLIDATED);
            String stepEnhancedConsolidated = request.getParameter(STEP_ENHANCED_CONSOLIDATED);
            //String rankId = request.getParameter(RANK_ID);
            
            String subtypeId = request.getParameter(SUBTYPE_ID);
            String typeId = request.getParameter(TYPE_ID);
            String biometricData = request.getParameter(BIOMETRIC_DATA);
            
            ServerResponse serverResponse = new ServerResponse();
            AppUser appUser = getAppUserFromSession(request);
            if (appUser != null) {                
                if (userHasPrivilege(intention, request, appUser)) {
                    AppPensionerBasic appPensionerBasic = null;
                    try {
                        appPensionerBasic = pensionerManager
                            .savePensioner(pensionerId, firstName, middleName, surname, 
                                    genderId, serviceOrgId, motherMaidenName, 
                                    honorificId, pfaId, rsaNumber, maritalStatusId, 
                                    dateOfBirth, email, contactAddress, contactPhoneNumber,
                                    academic, employerId, employerAddress, stateOfService, 
                                    staffId, expectedRetirementDate, firstAppointmentDate, 
                                    retirementModeId, salaryStructure2004, gradeLevel2004, 
                                    step2004, salaryStructureConsolidated, gradeLevelConsolidated, 
                                    stepConsolidated, salaryStructureEnhancedConsolidated, 
                                    gradeLevelEnhancedConsolidated, stepEnhancedConsolidated,
                                    typeId, subtypeId, biometricData, appUser.getUsername());
                    } catch(ParseException pe) {
                        serverResponse.setStatus(StatusType.FAILED.getValue());
                        //change to parse error message
                        serverResponse.setStatusMessage(StatusType.FAILED_MESSAGE.getValue());
                        List<Object> payload = new ArrayList<Object>();
                        payload.add(pe.getMessage());
                        serverResponse.setPayLoad(payload);
                        out.write(jsonMapper.writeValueAsString(serverResponse));
                    }
                    if (appPensionerBasic != null) {                        
                        serverResponse.setStatus(StatusType.SUCCESS.getValue());
                        serverResponse.setStatusMessage(StatusType.SUCCESS_MESSAGE.getValue());
                        List<Object> payload = new ArrayList<Object>();
                        payload.add(appPensionerBasic);
                        serverResponse.setPayLoad(payload);
                        out.write(jsonMapper.writeValueAsString(serverResponse));                        
                    } else {
                        serverResponse.setStatus(StatusType.FAILED.getValue());
                        serverResponse.setStatusMessage(StatusType.FAILED_MESSAGE.getValue());
                        out.write(jsonMapper.writeValueAsString(serverResponse));
                    }
                    
                } else {
                    request.setAttribute(PRIVILEGES_KEY, appUser.getUserRoles());
                    request.setAttribute(PAGE_SECTION_KEY, DASHBOARD_PAGE);
                    request.getRequestDispatcher(PAGE_FRAME).forward(request, response);  
                    //send json message instead
                }
            } else {
                request.setAttribute(ERROR_MSG_KEY, ACCESS_DENIED_MSG);
                request.getRequestDispatcher(INDEX_PAGE).forward(request, response); 
                //send json message instead
            }            
        } else if (intention.equals(ADD_USER_INTENTION)) {
            String username = request.getParameter(USERNAME);
            String firstname = request.getParameter(FIRST_NAME);
            String othernames = request.getParameter(OTHERNAMES);
            
            ServerResponse serverResponse = new ServerResponse();
            
            AppUser appUser = getAppUserFromSession(request);
            if (appUser != null) {                
                if (userHasPrivilege(MANAGE_INTENTION, request, appUser)) {
                    AppUser user = userManager.addUser(username, firstname, othernames);
                    if (user != null) {           
                        serverResponse.setStatus(StatusType.SUCCESS.getValue());
                        serverResponse.setStatusMessage(StatusType.SUCCESS_MESSAGE.getValue());
                        List<Object> payload = new ArrayList<Object>();
                        payload.add(user);
                        serverResponse.setPayLoad(payload);                                                                        
                    } else {
                        serverResponse.setStatus(StatusType.FAILED.getValue());
                        serverResponse.setStatusMessage(StatusType.FAILED_MESSAGE.getValue());
                    }
                    out.write(jsonMapper.writeValueAsString(serverResponse));
                } else {
                    request.setAttribute(PRIVILEGES_KEY, appUser.getUserRoles());                
                    request.setAttribute(PAGE_SECTION_KEY, DASHBOARD_PAGE);
                    request.getRequestDispatcher(PAGE_FRAME).forward(request, response);  
                    //send json message instead
                }
            } else {
                request.setAttribute(ERROR_MSG_KEY, ACCESS_DENIED_MSG);
                request.getRequestDispatcher(INDEX_PAGE).forward(request, response); 
                //send json message instead
            }
        
        } else if( intention.equals(DELETE_USER_INTENTION)){
            String userId = request.getParameter(USER_ID_KEY);            
            ServerResponse serverResponse = new ServerResponse();
            
            AppUser appUser = getAppUserFromSession(request);
            if (appUser != null) {                
                if (userHasPrivilege(MANAGE_INTENTION, request, appUser)) {
                    Boolean deleteUser = null;
                    deleteUser = userManager.deleteUser(userId);
                    if (deleteUser) {           
                        serverResponse.setStatus(StatusType.SUCCESS.getValue());
                        serverResponse.setStatusMessage(StatusType.SUCCESS_MESSAGE.getValue());                                            
                    }  else {
                        serverResponse.setStatus(StatusType.FAILED.getValue());
                        serverResponse.setStatusMessage(StatusType.FAILED_MESSAGE.getValue());
                    }
                    out.write(jsonMapper.writeValueAsString(serverResponse));                    
                } else {
                    request.setAttribute(PRIVILEGES_KEY, appUser.getUserRoles());                
                    request.setAttribute(PAGE_SECTION_KEY, DASHBOARD_PAGE);
                    request.getRequestDispatcher(PAGE_FRAME).forward(request, response);  
                }
            } else {
                request.setAttribute(ERROR_MSG_KEY, ACCESS_DENIED_MSG);
                request.getRequestDispatcher(INDEX_PAGE).forward(request, response); 
                //send json message instead
            }
        } else if (intention.equals(UPDATE_USER_INTENTION)){
            String username = request.getParameter(USERNAME);
            String firstname = request.getParameter(FIRST_NAME);
            String othernames = request.getParameter(OTHERNAMES);
            
            ServerResponse serverResponse = new ServerResponse();
            
            AppUser appUser = getAppUserFromSession(request);
            if (appUser != null) {                
                if (userHasPrivilege(MANAGE_INTENTION, request, appUser)) {
                    AppUser user = userManager.updateUser(username, firstname, othernames);    
                    
                    if (user != null) {           
                        serverResponse.setStatus(StatusType.SUCCESS.getValue());
                        serverResponse.setStatusMessage(StatusType.SUCCESS_MESSAGE.getValue());
                        List<Object> payload = new ArrayList<Object>();
                        payload.add(user);
                        serverResponse.setPayLoad(payload);                                                                        
                    } else {
                        serverResponse.setStatus(StatusType.FAILED.getValue());
                        serverResponse.setStatusMessage(StatusType.FAILED_MESSAGE.getValue());
                    }
                    out.write(jsonMapper.writeValueAsString(serverResponse));
                } else {
                    request.setAttribute(PRIVILEGES_KEY, appUser.getUserRoles());                
                    request.setAttribute(PAGE_SECTION_KEY, DASHBOARD_PAGE);
                    request.getRequestDispatcher(PAGE_FRAME).forward(request, response);  
                    //send json message instead
                }
            } else {
                request.setAttribute(ERROR_MSG_KEY, ACCESS_DENIED_MSG);
                request.getRequestDispatcher(INDEX_PAGE).forward(request, response); 
                //send json message instead
            } 
        } else if (intention.equals(ADD_ROLE_INTENTION)){
            String roleId = request.getParameter(ROLE_ID);
            String description = request.getParameter(ROLE_DESCRIPTION);
            
            ServerResponse serverResponse = new ServerResponse();
            
            AppUser appUser = getAppUserFromSession(request);
            if (appUser != null) {                
                if (userHasPrivilege(MANAGE_INTENTION, request, appUser)) {
                    AppRole role = roleManager.addRole(roleId, description);
                    if (role != null) {           
                        serverResponse.setStatus(StatusType.SUCCESS.getValue());
                        serverResponse.setStatusMessage(StatusType.SUCCESS_MESSAGE.getValue());
                        List<Object> payload = new ArrayList<Object>();
                        payload.add(role);
                        serverResponse.setPayLoad(payload);                                                                        
                    } else {
                        serverResponse.setStatus(StatusType.FAILED.getValue());
                        serverResponse.setStatusMessage(StatusType.FAILED_MESSAGE.getValue());
                    }
                    out.write(jsonMapper.writeValueAsString(serverResponse));
                } else {
                    request.setAttribute(PRIVILEGES_KEY, appUser.getUserRoles());                
                    request.setAttribute(PAGE_SECTION_KEY, DASHBOARD_PAGE);
                    request.getRequestDispatcher(PAGE_FRAME).forward(request, response);  
                    //send json message instead
                }
            } else {
                request.setAttribute(ERROR_MSG_KEY, ACCESS_DENIED_MSG);
                request.getRequestDispatcher(INDEX_PAGE).forward(request, response); 
                //send json message instead
            } 
        } else if(intention.equals(DELETE_ROLE_INTENTION)) {
            String roleId = request.getParameter(ROLE_ID);  
            
            ServerResponse serverResponse = new ServerResponse();
            
            AppUser appUser = getAppUserFromSession(request);
            if (appUser != null) {                
                if (userHasPrivilege(MANAGE_INTENTION, request, appUser)) {
                    Boolean deleteRole = roleManager.deleteRole(roleId);
                    if (deleteRole) {           
                        serverResponse.setStatus(StatusType.SUCCESS.getValue());
                        serverResponse.setStatusMessage(StatusType.SUCCESS_MESSAGE.getValue());                                            
                    }  else {
                        serverResponse.setStatus(StatusType.FAILED.getValue());
                        serverResponse.setStatusMessage(StatusType.FAILED_MESSAGE.getValue());
                    }
                    out.write(jsonMapper.writeValueAsString(serverResponse));                    
                } else {
                    request.setAttribute(PRIVILEGES_KEY, appUser.getUserRoles());                
                    request.setAttribute(PAGE_SECTION_KEY, DASHBOARD_PAGE);
                    request.getRequestDispatcher(PAGE_FRAME).forward(request, response);  
                }
            } else {
                request.setAttribute(ERROR_MSG_KEY, ACCESS_DENIED_MSG);
                request.getRequestDispatcher(INDEX_PAGE).forward(request, response); 
            }
        } else if (intention.equals(UPDATE_ROLE_INTENTION)){
            String roleId = request.getParameter(ROLE_ID);
            String description = request.getParameter(ROLE_DESCRIPTION);
            
            ServerResponse serverResponse = new ServerResponse();
            
            AppUser appUser = getAppUserFromSession(request);
            if (appUser != null) {                
                if (userHasPrivilege(MANAGE_INTENTION, request, appUser)) {
                    AppRole role = roleManager.updateRole(roleId, description);    
                    
                    if (role != null) {           
                        serverResponse.setStatus(StatusType.SUCCESS.getValue());
                        serverResponse.setStatusMessage(StatusType.SUCCESS_MESSAGE.getValue());
                        List<Object> payload = new ArrayList<Object>();
                        payload.add(role);
                        serverResponse.setPayLoad(payload);                                                                        
                    } else {
                        serverResponse.setStatus(StatusType.FAILED.getValue());
                        serverResponse.setStatusMessage(StatusType.FAILED_MESSAGE.getValue());
                    }
                    out.write(jsonMapper.writeValueAsString(serverResponse));
                } else {
                    request.setAttribute(PRIVILEGES_KEY, appUser.getUserRoles());                
                    request.setAttribute(PAGE_SECTION_KEY, DASHBOARD_PAGE);
                    request.getRequestDispatcher(PAGE_FRAME).forward(request, response);  
                }
            } else {
                request.setAttribute(ERROR_MSG_KEY, ACCESS_DENIED_MSG);
                request.getRequestDispatcher(INDEX_PAGE).forward(request, response); 
                //send json message instead
            } 
        } else if (intention.equals(ADD_PRIVILEGE_INTENTION)){
            String privilegeId = request.getParameter(PRIVILEGE_ID);
            String description = request.getParameter(PRIVILEGE_DESCRIPTION);
            String menuLink = request.getParameter(PRIVILEGE_MENU_LINK);
            
            ServerResponse serverResponse = new ServerResponse();
            
            AppUser appUser = getAppUserFromSession(request);
            if (appUser != null) {                
                if (userHasPrivilege(MANAGE_INTENTION, request, appUser)) {
                    AppPrivilege privilege = privilegeManager.addPrivilege(privilegeId, description, menuLink);
                    if (privilege != null) {           
                        serverResponse.setStatus(StatusType.SUCCESS.getValue());
                        serverResponse.setStatusMessage(StatusType.SUCCESS_MESSAGE.getValue());
                        List<Object> payload = new ArrayList<Object>();
                        payload.add(privilege);
                        serverResponse.setPayLoad(payload);                                                                        
                    } else {
                        serverResponse.setStatus(StatusType.FAILED.getValue());
                        serverResponse.setStatusMessage(StatusType.FAILED_MESSAGE.getValue());
                    }
                    out.write(jsonMapper.writeValueAsString(serverResponse));
                } else {
                    request.setAttribute(PRIVILEGES_KEY, appUser.getUserRoles());                
                    request.setAttribute(PAGE_SECTION_KEY, DASHBOARD_PAGE);
                    request.getRequestDispatcher(PAGE_FRAME).forward(request, response);  
                    //send json message instead
                }
            }
        } else if (intention.equals(SEARCH_INTENTION)) {
            String searchInput = request.getParameter(SEARCH_INPUT);
            AppUser appUser = getAppUserFromSession(request);
            if (appUser != null) {
                ServerResponse serverResponse = new ServerResponse();
                if (userHasPrivilege(intention, request, appUser)) { 
                    List<AppPensionerBasic> pensionersFound = pensionerManager.searchPensioner(searchInput);
                    serverResponse.setStatus(StatusType.SUCCESS.getValue());
                    serverResponse.setStatusMessage(StatusType.SUCCESS_MESSAGE.getValue());
                    List<Object> payload = new ArrayList<Object>();
                    payload.add(pensionersFound);
                    serverResponse.setPayLoad(payload);                                       
                } else {
                    serverResponse.setStatus(StatusType.FAILED.getValue());
                    serverResponse.setStatusMessage(StatusType.FAILED_MESSAGE.getValue());                    
                }
                
                out.write(jsonMapper.writeValueAsString(serverResponse));
            } else {
                request.setAttribute(ERROR_MSG_KEY, ACCESS_DENIED_MSG);
                request.getRequestDispatcher(INDEX_PAGE).forward(request, response); 
                //send json message instead
            } 
        } else if(intention.equals(DELETE_PRIVILEGE_INTENTION)) {
            String privilegeId = request.getParameter(PRIVILEGE_ID); 
            
            ServerResponse serverResponse = new ServerResponse();
            
            AppUser appUser = getAppUserFromSession(request);
            if (appUser != null) {                
                if (userHasPrivilege(MANAGE_INTENTION, request, appUser)) {
                    Boolean deletePrivilege = privilegeManager.deletePrivilege(privilegeId);
                    if (deletePrivilege) {           
                        serverResponse.setStatus(StatusType.SUCCESS.getValue());
                        serverResponse.setStatusMessage(StatusType.SUCCESS_MESSAGE.getValue());                                            
                    }  else {
                        serverResponse.setStatus(StatusType.FAILED.getValue());
                        serverResponse.setStatusMessage(StatusType.FAILED_MESSAGE.getValue());
                    }
                    out.write(jsonMapper.writeValueAsString(serverResponse));                    
                } else {
                    request.setAttribute(PRIVILEGES_KEY, appUser.getUserRoles());                
                    request.setAttribute(PAGE_SECTION_KEY, DASHBOARD_PAGE);
                    request.getRequestDispatcher(PAGE_FRAME).forward(request, response);  
                }
            } else {
                request.setAttribute(ERROR_MSG_KEY, ACCESS_DENIED_MSG);
                request.getRequestDispatcher(INDEX_PAGE).forward(request, response); 
            }
        } else if (intention.equals(UPDATE_PRIVILEGE_INTENTION)){
            String privilegeId = request.getParameter(PRIVILEGE_ID);
            String description = request.getParameter(PRIVILEGE_DESCRIPTION);
            String menuLink = request.getParameter(PRIVILEGE_MENU_LINK);
            String menuIcon = request.getParameter(PRIVILEGE_MENU_ICON);
            
            ServerResponse serverResponse = new ServerResponse();
            
            AppUser appUser = getAppUserFromSession(request);
            if (appUser != null) {                
                if (userHasPrivilege(MANAGE_INTENTION, request, appUser)) {
                    AppPrivilege privilege = privilegeManager.updatePrivilege(privilegeId, description, menuLink, menuIcon);
                    
                    if (privilege != null) {           
                        serverResponse.setStatus(StatusType.SUCCESS.getValue());
                        serverResponse.setStatusMessage(StatusType.SUCCESS_MESSAGE.getValue());
                        List<Object> payload = new ArrayList<Object>();
                        payload.add(privilege);
                        serverResponse.setPayLoad(payload);                                                                        
                    } else {
                        serverResponse.setStatus(StatusType.FAILED.getValue());
                        serverResponse.setStatusMessage(StatusType.FAILED_MESSAGE.getValue());
                    }
                    out.write(jsonMapper.writeValueAsString(serverResponse));
                } else {
                    request.setAttribute(PRIVILEGES_KEY, appUser.getUserRoles());                
                    request.setAttribute(PAGE_SECTION_KEY, DASHBOARD_PAGE);
                    request.getRequestDispatcher(PAGE_FRAME).forward(request, response);  
                }
            } else {
                request.setAttribute(ERROR_MSG_KEY, ACCESS_DENIED_MSG);
                request.getRequestDispatcher(INDEX_PAGE).forward(request, response); 
                //send json message instead
            } 
        } else if (intention.equals(ADD_USERROLE_INTENTION)){
            String userId = request.getParameter(USER_ID_KEY);
            String roleId = request.getParameter(ROLE_ID);
            
            ServerResponse serverResponse = new ServerResponse();
            
            AppUser appUser = getAppUserFromSession(request);
            if (appUser != null) {                
                if (userHasPrivilege(MANAGE_INTENTION, request, appUser)) {
                    Boolean addUserRole = userManager.addUserRole(userId, roleId);
                    if (addUserRole) {           
                        serverResponse.setStatus(StatusType.SUCCESS.getValue());
                        serverResponse.setStatusMessage(StatusType.SUCCESS_MESSAGE.getValue());                                            
                    }  else {
                        serverResponse.setStatus(StatusType.FAILED.getValue());
                        serverResponse.setStatusMessage(StatusType.FAILED_MESSAGE.getValue());
                    }
                    out.write(jsonMapper.writeValueAsString(serverResponse)); 
                } else {
                    request.setAttribute(PRIVILEGES_KEY, appUser.getUserRoles());                
                    request.setAttribute(PAGE_SECTION_KEY, DASHBOARD_PAGE);
                    request.getRequestDispatcher(PAGE_FRAME).forward(request, response);  
                }
            } else {
                request.setAttribute(ERROR_MSG_KEY, ACCESS_DENIED_MSG);
                request.getRequestDispatcher(INDEX_PAGE).forward(request, response); 
                //send json message instead
            } 
        } else if (intention.equals(DELETE_USERROLE_INTENTION)){
            String userId = request.getParameter(USER_ID_KEY);
            String roleId = request.getParameter(ROLE_ID);
            
            ServerResponse serverResponse = new ServerResponse();
            
            AppUser appUser = getAppUserFromSession(request);
            if (appUser != null) {                
                if (userHasPrivilege(MANAGE_INTENTION, request, appUser)) {                                        
                    Boolean deleteUserRole = userManager.deleteUserRole(userId, roleId);
                    if (deleteUserRole) {           
                        serverResponse.setStatus(StatusType.SUCCESS.getValue());
                        serverResponse.setStatusMessage(StatusType.SUCCESS_MESSAGE.getValue());                                            
                    }  else {
                        serverResponse.setStatus(StatusType.FAILED.getValue());
                        serverResponse.setStatusMessage(StatusType.FAILED_MESSAGE.getValue());
                    }
                    out.write(jsonMapper.writeValueAsString(serverResponse));    
                } else {
                    request.setAttribute(PRIVILEGES_KEY, appUser.getUserRoles());                
                    request.setAttribute(PAGE_SECTION_KEY, DASHBOARD_PAGE);
                    request.getRequestDispatcher(PAGE_FRAME).forward(request, response);  
                }
            } else {
                request.setAttribute(ERROR_MSG_KEY, ACCESS_DENIED_MSG);
                request.getRequestDispatcher(INDEX_PAGE).forward(request, response); 
            } 
        } else if (intention.equals(ADD_ROLE_PRIVILEGE_INTENTION)){
            String roleId = request.getParameter(ROLE_ID);
            String privilegeId = request.getParameter(PRIVILEGE_ID);
            
            ServerResponse serverResponse = new ServerResponse();
            
            AppUser appUser = getAppUserFromSession(request);
            if (appUser != null) {                
                if (userHasPrivilege(MANAGE_INTENTION, request, appUser)) {                    
                    Boolean addRolePrivilege = roleManager.addRolePrivilege(roleId, privilegeId);
                    if (addRolePrivilege) {           
                        serverResponse.setStatus(StatusType.SUCCESS.getValue());
                        serverResponse.setStatusMessage(StatusType.SUCCESS_MESSAGE.getValue());                                            
                    }  else {
                        serverResponse.setStatus(StatusType.FAILED.getValue());
                        serverResponse.setStatusMessage(StatusType.FAILED_MESSAGE.getValue());
                    }
                    out.write(jsonMapper.writeValueAsString(serverResponse)); 
                } else {
                    request.setAttribute(PRIVILEGES_KEY, appUser.getUserRoles());                
                    request.setAttribute(PAGE_SECTION_KEY, DASHBOARD_PAGE);
                    request.getRequestDispatcher(PAGE_FRAME).forward(request, response);  
                }
            } else {
                request.setAttribute(ERROR_MSG_KEY, ACCESS_DENIED_MSG);
                request.getRequestDispatcher(INDEX_PAGE).forward(request, response); 
                //send json message instead
            } 
        } else if (intention.equals(DELETE_ROLE_PRIVILEGE_INTENTION)){
            String roleId = request.getParameter(ROLE_ID);
            String privilegeId = request.getParameter(PRIVILEGE_ID);
            
            ServerResponse serverResponse = new ServerResponse();
            
            AppUser appUser = getAppUserFromSession(request);
            if (appUser != null) {                
                if (userHasPrivilege(MANAGE_INTENTION, request, appUser)) {                                        
                    Boolean deleteRolePrivilege = roleManager.deleteRolePrivilege(roleId, privilegeId);
                    if (deleteRolePrivilege) {           
                        serverResponse.setStatus(StatusType.SUCCESS.getValue());
                        serverResponse.setStatusMessage(StatusType.SUCCESS_MESSAGE.getValue());                                            
                    }  else {
                        serverResponse.setStatus(StatusType.FAILED.getValue());
                        serverResponse.setStatusMessage(StatusType.FAILED_MESSAGE.getValue());
                    }
                    out.write(jsonMapper.writeValueAsString(serverResponse));    
                } else {
                    request.setAttribute(PRIVILEGES_KEY, appUser.getUserRoles());                
                    request.setAttribute(PAGE_SECTION_KEY, DASHBOARD_PAGE);
                    request.getRequestDispatcher(PAGE_FRAME).forward(request, response);  
                }
            } else {
                request.setAttribute(ERROR_MSG_KEY, ACCESS_DENIED_MSG);
                request.getRequestDispatcher(INDEX_PAGE).forward(request, response); 
            } 
        } else {            
            request.setAttribute(ERROR_MSG_KEY, UNKNOWN_INTENTION_MSG);
            request.getRequestDispatcher(INDEX_PAGE).forward(request, response); 
        }           
    }
    
   private String polish(String pathInfo, HttpServletRequest request, 
           HttpServletResponse response) 
   throws ServletException, IOException{
       String polished = "";
       if (pathInfo != null) {
           polished = pathInfo.substring(pathInfo.lastIndexOf("/"), pathInfo.length()).replace("/", "");
       } else {
           request.getRequestDispatcher(INDEX_PAGE).forward(request, response);
       }
       return polished;
   }
   
   private AppUser getAppUserFromSession(HttpServletRequest request){
       AppUser appUser = null;
       HttpSession session = request.getSession(false);
       if (session != null) {
           String userId = (String) session.getAttribute(USER_ID_KEY);
           appUser = (AppUser) session.getAttribute(userId);
       }
       return appUser;
   }      
   
   private boolean userHasPrivilege(String intention, HttpServletRequest request, 
           AppUser appUser) {
       boolean hasPrivilege = false;
       for (AppRole appRole : appUser.getUserRoles()) {
           List<AppPrivilege> appUserPrivileges = appRole.getPrivileges();
           for (AppPrivilege appPrivilege : appUserPrivileges) {
               if (appPrivilege.getMenuLink().equals(intention)) {
                   hasPrivilege = true;
               }
           }
       }
       return hasPrivilege;
   }
   
   private void showRegistrationPage(AppUser appUser, AppPensionerBasic appPensioner, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        List<AppServiceOrganisationLookUp> serviceOrganisationLookUp = serviceOrganisationLookUpManager.getServiceOrganisations();
        List<AppPfaLookUp> pfaLookUp = pfaLookUpManager.getPfas();
        List<AppHonorificLookUp> honorificLookUp = honorificLookUpManager.getHonorifics();
        List<AppGenderLookUp> genderLookUp = genderLookUpManager.getGenders();
        List<AppMaritalStatusLookUp> maritalStatusLookUp = maritalStatusLookUpManager.getMaritalStatuses();
        List<AppEmployerLookUp> employerLookUp = employerLookUpManager.getEmployers();
        List<AppStateLookUp> stateLookUp = stateLookUpManager.getStates();
        List<AppRetirementModeLookUp> retirementModeLookUp = retirementModeLookUpManager.getRetirementModes();
        List<AppSalaryStructureLookUp> salaryStructureLookUp = salaryStructureLookUpManager.getAll2004SalaryStructures();
        List<AppSalaryStructureLookUp> consolidateSalaryStructureLookUp = salaryStructureLookUpManager.getAllConsolidatedSalaryStructures();
        List<AppSalaryStructureLookUp> enhancedConsolidatedSalaryStructureLookUp = salaryStructureLookUpManager.getAllEnhancedConsolidatedSalaryStructures();
        List<AppGlLookUp> glLookUp = glLookUpManager.getGls();
        List<AppRankLookUp> rankLookUp = rankLookUpManager.getRanks();
        request.setAttribute(PRIVILEGES_KEY, appUser.getUserRoles());
        request.setAttribute(PAGE_SECTION_KEY, ENROLMENT_PAGE); 
        request.setAttribute(SERVICE_ORGANISATIONS_KEY, serviceOrganisationLookUp);
        request.setAttribute(PFAS_KEY, pfaLookUp);
        request.setAttribute(HONORIFICS_KEY, honorificLookUp);
        request.setAttribute(GENDERS_KEY, genderLookUp);
        request.setAttribute(MARITAL_STATUSES_KEY, maritalStatusLookUp);
        request.setAttribute(EMPLOYERS_KEY, employerLookUp);
        request.setAttribute(STATES_KEY, stateLookUp);
        request.setAttribute(RETIREMENT_MODES_KEY, retirementModeLookUp);
        request.setAttribute(SALARY_STRUCTURES_KEY, salaryStructureLookUp);
        request.setAttribute(CONSOLIDATED_SALARY_STRUCTURES_KEY, consolidateSalaryStructureLookUp);
        request.setAttribute(ENHANCED_CONSOLIDATED_SALARY_STRUCTURES_KEY, enhancedConsolidatedSalaryStructureLookUp);
        request.setAttribute(GLS_KEY, glLookUp);
        request.setAttribute(RANKS_KEY, rankLookUp);
        request.setAttribute(INTENTION_KEY, REGISTER_INTENTION);
        if (appPensioner != null) {
            request.setAttribute(PENSIONER_KEY, appPensioner);    
        }
        request.getRequestDispatcher(PAGE_FRAME).forward(request, response);
   }
    
    @Override
    public String getServletInfo() {
        return "Web Backend Servlet";
    }

    
    
    
    
}
