 /*
 * Copyright Intellectual Apps Ltd.
 */
package com.intellectualapps.pencom.data.manager;

 import com.intellectualapps.pencom.data.provider.DataProviderLocal;
 import com.intellectualapps.pencom.model.Privilege;
 import com.intellectualapps.pencom.model.RolePrivilege;
 import com.intellectualapps.pencom.model.User;
 import java.util.HashMap;
 import javax.ejb.EJB;
 import javax.ejb.Stateless;
 import java.util.List;

/**
 *
 * @author buls
 */
@Stateless
public class PrivilegeDataManager implements PrivilegeDataManagerLocal {

    @EJB
    private DataProviderLocal crud;    

    @Override
    public Privilege create(Privilege privilege) {
        return crud.create(privilege);
    }

    @Override
    public Privilege update(Privilege privilege) {
        return crud.update(privilege);
    }

    @Override
    public Privilege get(String privilegeId) {
        return crud.find(privilegeId, Privilege.class);
    }

    @Override
    public void delete(Privilege privilege) {
        crud.delete(privilege);
    }

    @Override
    public List<Privilege> getAllPrivileges() {
        return crud.findAll(Privilege.class);
    }    
    
    

}
