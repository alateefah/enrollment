/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.data.manager;

import com.intellectualapps.pencom.model.SalaryStructureLookUp;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Lateefah
 */
@Local
public interface SalaryStructureLookUpDataManagerLocal {
    List<SalaryStructureLookUp> getAllSalaryStructures();
    
    List<SalaryStructureLookUp> getAll2004SalaryStructures();
    
    List<SalaryStructureLookUp> getAllConsolidatedSalaryStructures();
    
    List<SalaryStructureLookUp> getAllEnhancedConsolidatedSalaryStructures();
}
