 /*
 * Copyright 2016 Intellectual Apps Ltd.
 */
package com.intellectualapps.pencom.data.manager;

import com.intellectualapps.pencom.model.Id;
 import com.intellectualapps.pencom.model.User;

 import javax.ejb.Local;
 import java.util.List;

/**
 *
 * @author buls
 */
@Local
public interface IdGeneratorDataManagerLocal {
    
    Id create(Id id);

    Id update(Id id);

    Id get(String id);

    void delete(Id id);

    List<Id> getAllIds();
    
    Id getId();

}
