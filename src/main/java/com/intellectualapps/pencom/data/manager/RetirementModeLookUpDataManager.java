/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.data.manager;

import com.intellectualapps.pencom.data.provider.DataProviderLocal;
import com.intellectualapps.pencom.model.RetirementModeLookUp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Lateefah
 */
@Stateless
public class RetirementModeLookUpDataManager implements RetirementModeLookUpDataManagerLocal{
    @EJB
    private DataProviderLocal crud; 
    
    @Override
    public List<RetirementModeLookUp> getAllRetirementModes() {
        return crud.findAll(RetirementModeLookUp.class);
    }  

    @Override
    public List<RetirementModeLookUp> getRetirementMode(Integer retirementModeId) {        
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("retirementModeId", retirementModeId);
        return crud.findByNamedQuery("RetirementModeLookUp.findByRetirementModeId",
                parameters, RetirementModeLookUp.class);
    }    
}
