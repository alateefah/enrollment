 /*
 * Copyright Intellectual Apps Ltd.
 */
package com.intellectualapps.pencom.data.manager;

 import com.intellectualapps.pencom.data.provider.DataProviderLocal;
import com.intellectualapps.pencom.model.PensionerBasic;
 import com.intellectualapps.pencom.model.User;
import com.intellectualapps.pencom.model.UserRole;
import java.util.HashMap;
 import javax.ejb.EJB;
 import javax.ejb.Stateless;
 import java.util.List;
import java.util.Map;

/**
 *
 * @author buls
 */
@Stateless
public class PensionerDataManager implements PensionerDataManagerLocal {

    @EJB
    private DataProviderLocal crud;    

    @Override
    public PensionerBasic create(PensionerBasic pensionerBasic) {
        return crud.create(pensionerBasic);
    }

    @Override
    public PensionerBasic update(PensionerBasic pensionerBasic) {
        return crud.update(pensionerBasic);
    }

    @Override
    public PensionerBasic get(String pensionerId) {
        return crud.find(pensionerId, PensionerBasic.class);
    }

    @Override
    public void delete(String pensionerId) {
        crud.delete(pensionerId);
    }

    @Override
    public List<PensionerBasic> getAll() {
        return crud.findAll(PensionerBasic.class);
    }

    @Override
    public List<PensionerBasic> searchPensioner(String searchInput) {
        Map<String, Object> parameters = new HashMap<String, Object>();
        int pensionerId = -1;
        try {
            pensionerId = Integer.parseInt(searchInput);
        } catch (NumberFormatException nfe) {
            //do nothing
        }
        if (pensionerId == -1) {            
            parameters.put("firstName", "%" + searchInput + "%");
            parameters.put("middleName", "%" + searchInput + "%");
            parameters.put("surname", "%" + searchInput + "%");
            
            return crud.findByNamedQuery("PensionerBasic.findByFirstMiddleSurname",
                parameters, PensionerBasic.class);
        } else {
            parameters.put("pensionerId", "%" + pensionerId + "%");
            parameters.put("firstName", "%" + searchInput + "%");
            parameters.put("middleName", "%" + searchInput + "%");
            parameters.put("surname", "%" + searchInput + "%");
            
            return crud.findByNamedQuery("PensionerBasic.findByPensionerIdFirstMiddleSurname",
                parameters, PensionerBasic.class);
        }

    }

    
    
}
