 /*
 * Copyright 2016 Intellectual Apps Ltd.
 */
package com.intellectualapps.pencom.data.manager;

 import com.intellectualapps.pencom.model.User;

 import javax.ejb.Local;
 import java.util.List;

/**
 *
 * @author buls
 */
@Local
public interface UserDataManagerLocal {
    
    User create(User user);

    User update(User user);

    User get(String userId);

    void delete(User user);

    List<User> getAllUsers();

}
