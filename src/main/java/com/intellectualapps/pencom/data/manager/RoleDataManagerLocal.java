 /*
 * Copyright 2016 Intellectual Apps Ltd.
 */
package com.intellectualapps.pencom.data.manager;

 import com.intellectualapps.pencom.model.Privilege;
 import com.intellectualapps.pencom.model.Role;
 import com.intellectualapps.pencom.model.RolePrivilege;
 import com.intellectualapps.pencom.model.User;

 import javax.ejb.Local;
 import java.util.List;

/**
 *
 * @author buls
 */
@Local
public interface RoleDataManagerLocal {
    
    Role create(Role role);

    Role update(Role role);

    Role get(String roleId);

    void delete(Role role);

    List<Role> getAllRoles();
    
    List<RolePrivilege> getByRole(String roleId);
    
    RolePrivilege addRolePrivilege(RolePrivilege rolePrivilege);

    List<RolePrivilege> getRoleAndPrivilege(String roleId, String privilegeId);
    
    void deleteRolePrivilege(RolePrivilege rolePrivilege);
}
