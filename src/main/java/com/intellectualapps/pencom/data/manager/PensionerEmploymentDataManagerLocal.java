 /*
 * Copyright 2016 Intellectual Apps Ltd.
 */
package com.intellectualapps.pencom.data.manager;

import com.intellectualapps.pencom.model.PensionerBasic;
import com.intellectualapps.pencom.model.PensionerEmployment;
 import com.intellectualapps.pencom.model.User;
import com.intellectualapps.pencom.model.UserRole;

 import javax.ejb.Local;
 import java.util.List;

/**
 *
 * @author buls
 */
@Local
public interface PensionerEmploymentDataManagerLocal {
    
    PensionerEmployment create(PensionerEmployment pensionerBasic);

    PensionerEmployment update(PensionerEmployment pensionerBasic);

    PensionerEmployment get(Integer pensionerEmploymentId);

    void delete(PensionerEmployment pensionerEmploymentId);

    List<PensionerEmployment> getAll();

}
