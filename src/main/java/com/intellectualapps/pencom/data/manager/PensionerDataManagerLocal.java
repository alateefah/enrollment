 /*
 * Copyright 2016 Intellectual Apps Ltd.
 */
package com.intellectualapps.pencom.data.manager;

import com.intellectualapps.pencom.model.PensionerBasic;
 import com.intellectualapps.pencom.model.User;
import com.intellectualapps.pencom.model.UserRole;

 import javax.ejb.Local;
 import java.util.List;

/**
 *
 * @author buls
 */
@Local
public interface PensionerDataManagerLocal {
    
    PensionerBasic create(PensionerBasic pensionerBasic);

    PensionerBasic update(PensionerBasic pensionerBasic);

    PensionerBasic get(String userId);

    void delete(String userId);

    List<PensionerBasic> getAll();
    
    List<PensionerBasic> searchPensioner(String searchInput);

}
