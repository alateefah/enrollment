 /*
 * Copyright Intellectual Apps Ltd.
 */
package com.intellectualapps.pencom.data.manager;

 import com.intellectualapps.pencom.data.provider.DataProviderLocal;
import com.intellectualapps.pencom.model.PensionerBasic;
import com.intellectualapps.pencom.model.PensionerEmployment;
 import com.intellectualapps.pencom.model.User;
import com.intellectualapps.pencom.model.UserRole;
import java.util.HashMap;
 import javax.ejb.EJB;
 import javax.ejb.Stateless;
 import java.util.List;

/**
 *
 * @author buls
 */
@Stateless
public class PensionerEmploymentDataManager implements PensionerEmploymentDataManagerLocal {

    @EJB
    private DataProviderLocal crud;    

    @Override
    public PensionerEmployment create(PensionerEmployment pensionerBasic) {
        return crud.create(pensionerBasic);
    }

    @Override
    public PensionerEmployment update(PensionerEmployment pensionerBasic) {
        return crud.update(pensionerBasic);
    }

    @Override
    public PensionerEmployment get(Integer pensionerEmploymentId) {
        return crud.find(pensionerEmploymentId, PensionerEmployment.class);
    }

    @Override
    public void delete(PensionerEmployment pensionerEmployment) {
        crud.delete(pensionerEmployment);
    }

    @Override
    public List<PensionerEmployment> getAll() {
        return crud.findAll(PensionerEmployment.class);
    }

    
}
