 /*
 * Copyright Intellectual Apps Ltd.
 */
package com.intellectualapps.pencom.data.manager;

 import com.intellectualapps.pencom.data.provider.DataProviderLocal;
 import com.intellectualapps.pencom.model.Privilege;
 import com.intellectualapps.pencom.model.Role;
 import com.intellectualapps.pencom.model.RolePrivilege;
 import com.intellectualapps.pencom.model.User;
 import java.util.HashMap;
 import javax.ejb.EJB;
 import javax.ejb.Stateless;
 import java.util.List;

/**
 *
 * @author buls
 */
@Stateless
public class RoleDataManager implements RoleDataManagerLocal {

    @EJB
    private DataProviderLocal crud;    

    @Override
    public Role create(Role role) {
        return crud.create(role);
    }

    @Override
    public Role update(Role role) {
        return crud.update(role);
    }

    @Override
    public Role get(String roleId) {
        return crud.find(roleId, Role.class);
    }

    @Override
    public void delete(Role role) {
        crud.delete(role);
    }

    @Override
    public List<Role> getAllRoles() {
        return crud.findAll(Role.class);
    }

    @Override
    public List<RolePrivilege> getByRole(String roleId) {
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("roleId", roleId);
        List<RolePrivilege> rolePrivileges = crud
            .findByNamedQuery("RolePrivilege.findByRoleId",
                parameters,
                RolePrivilege.class);          
        return rolePrivileges;
    }

    @Override
    public RolePrivilege addRolePrivilege(RolePrivilege rolePrivilege){        
        return crud.create(rolePrivilege);        
    }
    
    @Override
    public void deleteRolePrivilege(RolePrivilege rolePrivilege){        
        crud.delete(rolePrivilege);        
    }
    
    @Override
    public List<RolePrivilege> getRoleAndPrivilege(String roleId, String privilegeId){
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("roleId", roleId);
        parameters.put("privilegeId", privilegeId);
        List<RolePrivilege> rolePrivileges = crud
            .findByNamedQuery("RolePrivilege.findByRoleIdAndPrivilegeId",
                parameters,
                RolePrivilege.class);

        return rolePrivileges;
    }
}
