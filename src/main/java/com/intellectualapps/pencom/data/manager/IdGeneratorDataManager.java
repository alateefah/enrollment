 /*
 * Copyright Intellectual Apps Ltd.
 */
package com.intellectualapps.pencom.data.manager;

 import com.intellectualapps.pencom.data.provider.DataProviderLocal;
import com.intellectualapps.pencom.model.Id;
import com.intellectualapps.pencom.model.PensionerBasic;
 import com.intellectualapps.pencom.model.User;
 import javax.ejb.EJB;
 import javax.ejb.Stateless;
 import java.util.List;

/**
 *
 * @author buls
 */
@Stateless
public class IdGeneratorDataManager implements IdGeneratorDataManagerLocal {

    @EJB
    private DataProviderLocal crud;    

    @Override
    public Id create(Id id) {
        return crud.create(id);
    }

    @Override
    public Id update(Id id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Id get(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Id id) {
        crud.delete(id);
    }

    @Override
    public List<Id> getAllIds() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Id getId() {
        List<Id> ids = crud.findRange(Id.class, 1, 1);        
        Id id = null;
        if (ids.size() > 0) {
            id = ids.get(0);
        }
        return id;
    }

    
}
