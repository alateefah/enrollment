/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.data.manager;

import com.intellectualapps.pencom.data.provider.DataProviderLocal;
import com.intellectualapps.pencom.model.PensionerBasic;
import com.intellectualapps.pencom.model.SalaryStructureLookUp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Lateefah
 */
@Stateless
public class SalaryStructureLookUpDataManager implements SalaryStructureLookUpDataManagerLocal{
    @EJB
    private DataProviderLocal crud; 
    
    @Override
    public List<SalaryStructureLookUp> getAllSalaryStructures() {
        return crud.findAll(SalaryStructureLookUp.class);
    }  

    @Override
    public List<SalaryStructureLookUp> getAll2004SalaryStructures() {
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("year", 2004);        
        return crud.findByNamedQuery("SalaryStructureLookUp.findByYear",
                parameters, SalaryStructureLookUp.class);        
    }

    @Override
    public List<SalaryStructureLookUp> getAllConsolidatedSalaryStructures() {
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("year", 2007);        
        return crud.findByNamedQuery("SalaryStructureLookUp.findByYear",
                parameters, SalaryStructureLookUp.class);        
    }

    @Override
    public List<SalaryStructureLookUp> getAllEnhancedConsolidatedSalaryStructures() {
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("year", 2010);        
        return crud.findByNamedQuery("SalaryStructureLookUp.findByYear",
                parameters, SalaryStructureLookUp.class);        
    }
    
}
