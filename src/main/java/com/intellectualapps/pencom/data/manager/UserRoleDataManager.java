 /*
 * Copyright Intellectual Apps Ltd.
 */
package com.intellectualapps.pencom.data.manager;

 import com.intellectualapps.pencom.data.provider.DataProviderLocal;
 import com.intellectualapps.pencom.model.User;
import com.intellectualapps.pencom.model.UserRole;
import java.util.HashMap;
 import javax.ejb.EJB;
 import javax.ejb.Stateless;
 import java.util.List;

/**
 *
 * @author buls
 */
@Stateless
public class UserRoleDataManager implements UserRoleDataManagerLocal {

    @EJB
    private DataProviderLocal crud;    

    @Override
    public UserRole create(UserRole userRole) {
        return crud.create(userRole);
    }

    @Override
    public UserRole update(UserRole userRole) {
        return crud.update(userRole);
    }

    @Override
    public UserRole get(String userId) {
        return crud.find(userId, UserRole.class);
    }

    @Override
    public void delete(UserRole userRole) {
        crud.delete(userRole);
    }

    @Override
    public List<UserRole> getByUser(String username) {
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("username", username);
        List<UserRole> userRoles = crud
            .findByNamedQuery("UserRole.findByUsername",
                parameters,
                UserRole.class);

        return userRoles;
    }

    @Override
    public List<UserRole> getUsernameAndRoleId(String username, String roleId){
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("username", username);
        parameters.put("roleId", roleId);
        List<UserRole> userRoles = crud
            .findByNamedQuery("UserRole.findByUsernameAndRoleId",
                parameters,
                UserRole.class);

        return userRoles;
    }
    
}
