/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.data.manager;

import com.intellectualapps.pencom.data.provider.DataProviderLocal;
import com.intellectualapps.pencom.model.EmployerLookUp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Lateefah
 */
@Stateless
public class EmployerLookUpDataManager implements EmployerLookUpDataManagerLocal {
    @EJB
    private DataProviderLocal crud; 
    
    @Override
    public List<EmployerLookUp> getAllEmployers() {
        return crud.findAll(EmployerLookUp.class);
    }  

    @Override
    public List<EmployerLookUp> getEmployer(Integer employerId) {
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("employerId", employerId);
        return crud.findByNamedQuery("EmployerLookUp.findByEmployerId",
                parameters, EmployerLookUp.class);
    }
}
