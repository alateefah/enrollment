/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.data.manager;

import com.intellectualapps.pencom.data.provider.DataProviderLocal;
import com.intellectualapps.pencom.model.ServiceOrganisationLookUp;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Lateefah
 */

@Stateless
public class ServiceOrganisationLookUpDataManager implements ServiceOrganisationLookUpDataManagerLocal {
 
    @EJB
    private DataProviderLocal crud; 
    
    @Override
    public List<ServiceOrganisationLookUp> getAllServiceOrganisations() {
        return crud.findAll(ServiceOrganisationLookUp.class);
    }  
    
}
