 /*
 * Copyright Intellectual Apps Ltd.
 */
package com.intellectualapps.pencom.data.manager;

 import com.intellectualapps.pencom.data.provider.DataProviderLocal;
 import com.intellectualapps.pencom.model.User;
 import javax.ejb.EJB;
 import javax.ejb.Stateless;
 import java.util.List;

/**
 *
 * @author buls
 */
@Stateless
public class UserDataManager implements UserDataManagerLocal {

    @EJB
    private DataProviderLocal crud;    

    @Override
    public User create(User user) {
        return crud.create(user);
    }

    @Override
    public User update(User user) {
        return crud.update(user);
    }

    @Override
    public User get(String userId) {
        return crud.find(userId, User.class);
    }

    @Override
    public void delete(User user) {
        crud.delete(user);
    }

    @Override
    public List<User> getAllUsers() {
        return crud.findAll(User.class);
    }    

}
