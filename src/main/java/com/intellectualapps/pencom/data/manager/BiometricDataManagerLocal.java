/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.data.manager;

 import com.intellectualapps.pencom.model.PensionerBiometric;

/**
 *
 * @author Lateefah
 */

public interface BiometricDataManagerLocal {
    PensionerBiometric create(PensionerBiometric biometric);

    PensionerBiometric update(PensionerBiometric biometric);

    PensionerBiometric get(String pensionerId);

    void delete(String pensionerId);

}
