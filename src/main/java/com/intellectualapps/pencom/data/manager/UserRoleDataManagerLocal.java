 /*
 * Copyright 2016 Intellectual Apps Ltd.
 */
package com.intellectualapps.pencom.data.manager;

 import com.intellectualapps.pencom.model.User;
import com.intellectualapps.pencom.model.UserRole;

 import javax.ejb.Local;
 import java.util.List;

/**
 *
 * @author buls
 */
@Local
public interface UserRoleDataManagerLocal {
    
    UserRole create(UserRole userRole);

    UserRole update(UserRole userRole);

    UserRole get(String userId);

    void delete(UserRole userRole);

    List<UserRole> getByUser(String userId); 
    
    List<UserRole> getUsernameAndRoleId(String username, String roleId);

}
