/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.data.manager;

import com.intellectualapps.pencom.data.provider.DataProviderLocal;
import com.intellectualapps.pencom.model.RankLookUp;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Lateefah
 */
@Stateless
public class RankLookUpDataManager implements RankLookUpDataManagerLocal{
    @EJB
    private DataProviderLocal crud; 
    
    @Override
    public List<RankLookUp> getAllRanks() {
        return crud.findAll(RankLookUp.class);
    } 
    
}
