/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.data.manager;

import com.intellectualapps.pencom.data.provider.DataProviderLocal;
import com.intellectualapps.pencom.model.PensionerBiometric;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Lateefah
*/

@Stateless
public class BiometricDataManager implements BiometricDataManagerLocal{
    
    @EJB
    private DataProviderLocal crud;    

    @Override
    public PensionerBiometric create(PensionerBiometric biometric) {
        return crud.create(biometric);
    }

    @Override
    public PensionerBiometric update(PensionerBiometric biometric) {
        return crud.update(biometric);
    }

    @Override
    public PensionerBiometric get(String pensionerId) {
        return crud.find(pensionerId, PensionerBiometric.class);
    }

    @Override
    public void delete(String pensionerId) {
        crud.delete(get(pensionerId));
    }
}
