 /*
 * Copyright 2016 Intellectual Apps Ltd.
 */
package com.intellectualapps.pencom.data.manager;

 import com.intellectualapps.pencom.model.Privilege;
 import com.intellectualapps.pencom.model.RolePrivilege;
 import com.intellectualapps.pencom.model.User;
 import com.intellectualapps.pencom.pojo.AppPrivilege;

 import javax.ejb.Local;
 import java.util.List;

/**
 *
 * @author buls
 */
@Local
public interface PrivilegeDataManagerLocal {
    
    Privilege create(Privilege privilege);

    Privilege update(Privilege privilege);

    Privilege get(String privilegeId);

    void delete(Privilege privilege);

    List<Privilege> getAllPrivileges();

}
