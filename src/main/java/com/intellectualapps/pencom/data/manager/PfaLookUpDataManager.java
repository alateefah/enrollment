/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.data.manager;

import com.intellectualapps.pencom.data.provider.DataProviderLocal;
import com.intellectualapps.pencom.model.PensionerBasic;
import com.intellectualapps.pencom.model.PfaLookUp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Lateefah
 */
@Stateless
public class PfaLookUpDataManager implements PfaLookUpDataManagerLocal{
    
    @EJB
    private DataProviderLocal crud; 
    
    @Override
    public List<PfaLookUp> getAllPfas() {
        return crud.findAll(PfaLookUp.class);
    }  

    @Override
    public List<PfaLookUp> getPfa(Integer pfaId) {
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("pfaId", pfaId);
        return crud.findByNamedQuery("PfaLookUp.findByPfaId",
                parameters, PfaLookUp.class);

    }
    
}
