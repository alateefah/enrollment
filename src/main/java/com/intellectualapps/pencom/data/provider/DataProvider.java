/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.data.provider;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

/**
 *
 * @author buls
 */
@Stateless
public class DataProvider implements DataProviderLocal {

    @PersistenceContext
    private EntityManager em;

    @Override
    public <T> T create(T t) {                
        em.persist(t);
        em.flush();
        return t;
    }

    @Override
    public <T> T find(Object id, Class<T> type) {
        return (T) em.find(type, id);
    }

    @Override
    public <T> T find(Object id, Class<T> type, Map<String, Object> properties) {
        return (T) em.find(type, id, properties);
    }

    @Override
    public void delete(Object t) {
        em.remove(em.merge(t));
    }

    @Override
    public <T> T update(T t) {
        return (T) em.merge(t);
    }

    @Override
    public <T> List<T> findAll(Class<T> type) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(type));
        return em.createQuery(cq).getResultList();
    }

    @Override
    public <T> List<T> findRange(Class<T> type, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(type));
        Query q = em.createQuery(cq);
        q.setMaxResults(maxResults);
        q.setFirstResult(firstResult);
        return q.getResultList();
    }

    @Override
    public <T> List<T> findByNamedQuery(String namedQueryName,
            Map<String, Object> parameters, Class<T> type) {
        return findByNamedQuery(namedQueryName, parameters, 0, type);
    }
    
    @Override
    public <T> List<T> findByNamedQuery(String namedQueryName,
            Map<String, Object> parameters,
                int resultLimit, Class<T> type) {
        Set<Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = this.em.createNamedQuery(namedQueryName);
        if (resultLimit > 0) {
            query.setMaxResults(resultLimit);
        }
        for (Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        return query.getResultList();
    }
    
    @Override
    public <T> int count(Class<T> type) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<T> rt = cq.from(type);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
        //return findAll(type).size();
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }    
    
}
