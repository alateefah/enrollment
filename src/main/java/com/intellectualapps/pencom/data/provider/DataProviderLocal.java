/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.intellectualapps.pencom.data.provider;

import javax.ejb.Local;
import javax.persistence.EntityManager;
import javax.persistence.metamodel.SingularAttribute;
import java.util.List;
import java.util.Map;

/**
 *
 * @author buls
 */
@Local
public interface DataProviderLocal {

    <T> int count(Class<T> type);

    <T> T create(T t);

    void delete(Object t);

    <T> T find(Object id, Class<T> type);

    <T> T find(Object id, Class<T> type, Map<String,Object> properties);

    <T> List<T> findAll(Class<T> type);
    
    <T> T update(T t);

    EntityManager getEntityManager();
    
    <T> List<T> findRange(Class<T> type, int maxResults, int firstResult);
    
    <T> List<T> findByNamedQuery(String namedQueryName,
            Map<String, Object> parameters, Class<T> type);
    
    <T> List<T> findByNamedQuery(String namedQueryName,
            Map<String, Object> parameters, int resultLimit, Class<T> type);

}