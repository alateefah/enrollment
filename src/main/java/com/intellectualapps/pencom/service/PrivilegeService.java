/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.service;


import com.intellectualapps.pencom.manager.PrivilegeManagerLocal;
import com.intellectualapps.pencom.manager.UserManager;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import com.intellectualapps.pencom.manager.UserManagerLocal;
import com.intellectualapps.pencom.model.Privilege;
import com.intellectualapps.pencom.pojo.AppPrivilege;
import java.util.List;

/**
 *
 * @author buls
 */

@Path("/privilege")
public class PrivilegeService {
    @Context
    HttpServletRequest request;       
    PrivilegeManagerLocal privilegeManager;
    
    private String PRIVILEGE_MANAGER_JNDI_NAME = 
            "java:global/enrol/PrivilegeManager!com.intellectualapps.pencom.manager.PrivilegeManagerLocal";
        
    public PrivilegeService() {        
        try {
            privilegeManager = (PrivilegeManagerLocal) new InitialContext().lookup(PRIVILEGE_MANAGER_JNDI_NAME);
        } catch (NamingException ex) {
            Logger.getLogger(PrivilegeService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/all")
    public List<AppPrivilege> getAllPrivilieges() {                        
        return (privilegeManager.getAllPrivileges());
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{privilegeId}")
    public Privilege getPriviliege(@PathParam("privilegeId") String privilegeId) {                        
        return (privilegeManager.getPrivilege(privilegeId));
    }
        
}
