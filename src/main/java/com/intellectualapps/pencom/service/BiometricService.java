/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.service;


import com.intellectualapps.pencom.manager.PensionerBiometricManager;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import com.intellectualapps.pencom.pojo.AppPensionerBiometric;
import javax.ws.rs.POST;
import com.intellectualapps.pencom.manager.PensionerBiometricManagerLocal;

/**
 *
 * @author Lateefah
 */

@Path("/biometric")
public class BiometricService {
    @Context
    HttpServletRequest request;       
    PensionerBiometricManagerLocal biometricManager;
    
    private String BIOMETRIC_MANAGER_JNDI_NAME = 
            "java:global/enrol/BiometricManager!com.intellectualapps.pencom.manager.BiometricManagerLocal";
        
    public BiometricService() {        
        try {
            biometricManager = (PensionerBiometricManager) new InitialContext().lookup(BIOMETRIC_MANAGER_JNDI_NAME);
        } catch (NamingException ex) {
            Logger.getLogger(BiometricService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/biometric/{pensionerId}/{typeId}/{subtypeId}/{biometricData}") //change to queryparams
    public AppPensionerBiometric addBiometric(
            @PathParam("pensionerId") String pensionerId, 
            @PathParam("typeId") String typeId,
            @PathParam("subtypeId") String subtypeId,
            @PathParam("biometricData") String biometricData) {                        
        return (biometricManager.addBiometric(pensionerId, typeId, subtypeId, biometricData, "getuserfromqueryparam"));        
    }
       
}
