/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.service;


import com.intellectualapps.pencom.manager.UserManager;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import com.intellectualapps.pencom.manager.UserManagerLocal;
import com.intellectualapps.pencom.model.Privilege;
import com.intellectualapps.pencom.pojo.AppUser;

/**
 *
 * @author buls
 */

@Path("/user")
public class UserService {
    @Context
    HttpServletRequest request;       
    UserManagerLocal userManager;
    
    private String USER_MANAGER_JNDI_NAME = 
            "java:global/enrol/UserManager!com.intellectualapps.pencom.manager.UserManagerLocal";
        
    public UserService() {        
        try {
            userManager = (UserManagerLocal) new InitialContext().lookup(USER_MANAGER_JNDI_NAME);
        } catch (NamingException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/authenticate/{username}/{password}")
    public AppUser authenticateUser(@PathParam("username") String username, 
            @PathParam("password") String password) {                        
        return (userManager.authenticateUser(username, password));        
    }
       
}
