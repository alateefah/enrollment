/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.service;


import com.intellectualapps.pencom.manager.PrivilegeManagerLocal;
import com.intellectualapps.pencom.manager.RoleManagerLocal;
import com.intellectualapps.pencom.manager.UserManager;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import com.intellectualapps.pencom.manager.UserManagerLocal;
import com.intellectualapps.pencom.model.Privilege;
import com.intellectualapps.pencom.model.Role;
import com.intellectualapps.pencom.model.RolePrivilege;
import com.intellectualapps.pencom.pojo.AppRole;
import java.util.List;

/**
 *
 * @author buls
 */

@Path("/role")
public class RoleService {
    @Context
    HttpServletRequest request;       
    RoleManagerLocal roleManager;
    
    private String ROLE_MANAGER_JNDI_NAME = 
            "java:global/enrol/RoleManager!com.intellectualapps.pencom.manager.RoleManagerLocal";
        
    public RoleService() {        
        try {
            roleManager = (RoleManagerLocal) new InitialContext().lookup(ROLE_MANAGER_JNDI_NAME);
        } catch (NamingException ex) {
            Logger.getLogger(RoleService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/all")
    public List<AppRole> getAllRoles() {                        
        return (roleManager.getAllRoles());
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{roleId}")
    public Role getRole(@PathParam("roleId") String roleId) {                        
        return (roleManager.getRole(roleId));
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/privileges/{roleId}")
    public List<RolePrivilege> getRolePrivilieges(@PathParam("roleId") String roleId) {                        
        return (roleManager.getRole(roleId).getRoleAndPrivilegeList());
    }
        
}
