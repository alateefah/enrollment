/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.util;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author buls
 */
public class ServerResponse implements Serializable{
    
    private String status;
    private String statusMessage;
    private List<Object> payLoad;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public List<Object> getPayLoad() {
        return payLoad;
    }

    public void setPayLoad(List<Object> payLoad) {
        this.payLoad = payLoad;
    }
    
    
}
