/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.util;

/**
 *
 * @author buls
 */
public enum StatusType {    

    SUCCESS("200"),
    FAILED("501"),
    SUCCESS_MESSAGE("Action successful"),
    FAILED_MESSAGE("Action failed");

    private String value;

    StatusType(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    
}
