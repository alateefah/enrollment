/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author buls
 */
public class DateParser {
    
    public Date parseDate(String stringDate) throws ParseException{
        SimpleDateFormat parser =new SimpleDateFormat("dd MMMM, yyyy");
        Date parsedDate = parser.parse(stringDate);
        
        return parsedDate;
    }
    
}
