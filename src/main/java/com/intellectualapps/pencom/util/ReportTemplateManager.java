/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.intellectualapps.pencom.util;

import java.io.File;
import java.io.InputStream;

/**
 *
 * @author buls
 */
public class ReportTemplateManager {
    private static final ReportTemplateManager singleton;

    static {
        singleton = new ReportTemplateManager();
    }

    public InputStream getJasperFile(String reportFile) {
        return getClass().getClassLoader().getResourceAsStream("reports" + File.separator + reportFile + ".jasper");
    }

    public InputStream getJrxmlFile(String reportFile) {
        return getClass().getClassLoader().getResourceAsStream("reports" + File.separator + reportFile + ".jrxml");
    }

    public static ReportTemplateManager get() {
        return singleton;
    }
}
