/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.util;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;



/**
 *
 * @author buls
 */
public class ImageUtil {
    
    public static Image convertToImage(String base64String) {
        if (base64String.contains(",")) {
            String[] base64StringParts = base64String.split(",");
            base64String = base64StringParts[1];
        }
        byte[] imageBytes = DatatypeConverter.parseBase64Binary(base64String);
        BufferedImage image = null;
        try {
            image = ImageIO.read(new ByteArrayInputStream(imageBytes));
        } catch (IOException e) {
            Logger.getLogger("convert to image", "ImageUtil").log(Level.SEVERE, e.getMessage());
        }
        
        return image;
    }
    
    public static byte[] convertBase64ToByteArray(String base64String) {
        byte[] imageBytes = DatatypeConverter.parseBase64Binary(base64String);
        
        return imageBytes;
    }
    
    public static Image getLogo(URL logoUrl) {
        Image logo = null;
        try {
            logo = ImageIO.read(logoUrl);
        } catch(Exception e) {
            Logger.getLogger("get logo", "ImageUtil").log(Level.SEVERE, e.getMessage());
        }
        
        return logo;
    }
    
    public static Image getWatermark(URL watermarkUrl) {
        Image watermark = null;
        try {
            watermark = ImageIO.read(watermarkUrl);
        } catch(Exception e) {
            Logger.getLogger("get watermark", "ImageUtil").log(Level.SEVERE, e.getMessage());
        }
        
        return watermark;
    }
    
}
