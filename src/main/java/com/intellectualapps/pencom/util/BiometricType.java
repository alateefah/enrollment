/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.util;

/**
 *
 * @author buls
 */
public enum BiometricType {    

    PHOTO("ps"),
    RIGHT_THUMB("rt"),
    RIGHT_INDEX("ri"),
    RIGHT_MIDDLE("rm"),
    RIGHT_RING("rr"),
    RIGHT_LITTLE("rl"),
    LEFT_THUMB("lt"),
    LEFT_INDEX("li"),
    LEFT_MIDDLE("lm"),
    LEFT_RING("lr"),
    LEFT_LITTLE("ll");

    private String value;

    BiometricType(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    
}
