/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.manager;

import com.intellectualapps.pencom.data.manager.MaritalStatusLookUpDataManagerLocal;
import com.intellectualapps.pencom.model.MaritalStatusLookUp;
import com.intellectualapps.pencom.pojo.AppMaritalStatusLookUp;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Lateefah
 */
@Stateless
public class MaritalStatusLookUpManager implements MaritalStatusLookUpManagerLocal{
    @EJB
    private MaritalStatusLookUpDataManagerLocal maritalStatusLookUpDataManager;
    
    @Override
    public List<AppMaritalStatusLookUp> getMaritalStatuses() {
        List<AppMaritalStatusLookUp> appMaritalStatuses = new ArrayList<AppMaritalStatusLookUp>();
        List<MaritalStatusLookUp> maritalStatuses = maritalStatusLookUpDataManager.getAllMaritalStatuses();
        for (MaritalStatusLookUp maritalStatus : maritalStatuses) {
            appMaritalStatuses.add(getAppMaritalStatusLookUp(maritalStatus));
        }
        return appMaritalStatuses;
    }
           
    @Override
    public AppMaritalStatusLookUp getAppMaritalStatusLookUp(MaritalStatusLookUp maritalStatus){
        AppMaritalStatusLookUp appMaritalStatus = new AppMaritalStatusLookUp();
        
        appMaritalStatus.setMaritalStatusId(maritalStatus.getMaritalStatusId());
        appMaritalStatus.setDescription(maritalStatus.getDescription());
        
        return appMaritalStatus;
    }
    
}
