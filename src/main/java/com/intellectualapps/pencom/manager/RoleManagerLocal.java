/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.manager;

import com.intellectualapps.pencom.model.Privilege;
import com.intellectualapps.pencom.model.Role;
import com.intellectualapps.pencom.model.RolePrivilege;
import com.intellectualapps.pencom.pojo.AppRole;
import java.util.List;

/**
 *
 * @author buls
 */
public interface RoleManagerLocal {
    
    Role getRole (String roleId);
    
    List<AppRole> getAllRoles();
    
    AppRole getAppRole(Role role);
    
    AppRole addRole(String roleId, String description);
    
    Boolean deleteRole(String roleId);
    
    AppRole updateRole(String roleId, String description);
    
    List<RolePrivilege> getRolePrivileges(String roleId);
    
    Boolean addRolePrivilege(String roleId, String privilegeId);
    
    Boolean deleteRolePrivilege(String roleId, String privilegeId);
    
    
}
