/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.manager;

import com.intellectualapps.pencom.data.manager.ServiceOrganisationLookUpDataManagerLocal;
import com.intellectualapps.pencom.model.PfaLookUp;
import com.intellectualapps.pencom.model.ServiceOrganisationLookUp;
import com.intellectualapps.pencom.pojo.AppPfaLookUp;
import com.intellectualapps.pencom.pojo.AppServiceOrganisationLookUp;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import com.intellectualapps.pencom.data.manager.PfaLookUpDataManagerLocal;

/**
 *
 * @author Lateefah
 */
@Stateless
public class PfaLookUpManager implements PfaLookUpManagerLocal {
    @EJB
    private PfaLookUpDataManagerLocal pfaLookUpDataManager;
    
    @Override
    public List<AppPfaLookUp> getPfas() {
        List<AppPfaLookUp> appPfas = new ArrayList<AppPfaLookUp>();
        List<PfaLookUp> pfas = pfaLookUpDataManager.getAllPfas();
        for (PfaLookUp pfa : pfas) {
            appPfas.add(getAppPfaLookUp(pfa));
        }
        return appPfas;
    }
           
    @Override
    public AppPfaLookUp getAppPfaLookUp(PfaLookUp pfa){
        AppPfaLookUp appPfa = new AppPfaLookUp();
        
        appPfa.setPfaId(pfa.getPfaId());
        appPfa.setName(pfa.getName());
                
        return appPfa;
    }
    
    @Override
    public AppPfaLookUp getAppPfaLookUp(Integer pfaId){
        AppPfaLookUp appPfa = new AppPfaLookUp();
        List<PfaLookUp> pfas = pfaLookUpDataManager.getPfa(pfaId);
        appPfa = getAppPfaLookUp(pfas.get(0));
                
        return appPfa;
    }
}
