/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.intellectualapps.pencom.manager;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.view.JasperViewer;


/** 
 *
 * @author buls
 */
public abstract class AbstractReportManager implements ReportManager {

    public AbstractReportManager() {
    }

    @Override
    public void exportToPdf(String destFileName) throws JRException {
        JasperExportManager.exportReportToPdfFile(getJasperPrint(), destFileName);
    }

    @Override
    public void exportToStream(OutputStream outputStream) throws JRException {
        JasperExportManager.exportReportToPdfStream(getJasperPrint(), outputStream);
    }

    @Override
    public byte[] getPdf() throws JRException {
        try {
            return JasperExportManager.exportReportToPdf(getJasperPrint());
        } catch(IllegalArgumentException ex) {
            
        }
        
        return JasperExportManager.exportReportToPdf(getJasperPrint());
    }

    @Override
    public byte[] getExcel() throws JRException {
        JRXlsExporter exporterXLS = new JRXlsExporter();
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        
        exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, getJasperPrint());
        exporterXLS.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
        exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
        exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
        exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.FALSE);
        exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, output);
        exporterXLS.setParameter(JRXlsExporterParameter.IS_IGNORE_GRAPHICS, Boolean.FALSE);

        exporterXLS.exportReport();
        
        return output.toByteArray();
    }
    
    @Override
    public void printPreview() throws JRException {
        //create jasper viewer
        JasperViewer jasperViewer = new JasperViewer(getJasperPrint(), true);
        jasperViewer.setVisible(true);
    }

    @Override
    public abstract JasperPrint getJasperPrint() throws JRException;
}
