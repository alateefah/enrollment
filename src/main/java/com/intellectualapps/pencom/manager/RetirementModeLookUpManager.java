/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.manager;

import com.intellectualapps.pencom.data.manager.RetirementModeLookUpDataManagerLocal;
import com.intellectualapps.pencom.model.RetirementModeLookUp;
import com.intellectualapps.pencom.pojo.AppRetirementModeLookUp;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Lateefah
 */
@Stateless
public class RetirementModeLookUpManager implements RetirementModeLookUpManagerLocal{
    @EJB
    private RetirementModeLookUpDataManagerLocal retirementModeLookUpDataManager;
    
    @Override
    public List<AppRetirementModeLookUp> getRetirementModes() {
        List<AppRetirementModeLookUp> appRetirementModes = new ArrayList<AppRetirementModeLookUp>();
        List<RetirementModeLookUp> retirementModes = retirementModeLookUpDataManager.getAllRetirementModes();
        for (RetirementModeLookUp retirementMode : retirementModes) {
            appRetirementModes.add(getAppRetirementModeLookUp(retirementMode));
        }
        return appRetirementModes;
    }
           
    @Override
    public AppRetirementModeLookUp getAppRetirementModeLookUp(RetirementModeLookUp retirementMode){
        AppRetirementModeLookUp appRetirementMode = new AppRetirementModeLookUp();
        
        appRetirementMode.setRetirementModeId(retirementMode.getRetirementModeId());
        appRetirementMode.setDescription(retirementMode.getDescription());
        
        return appRetirementMode;
    }

    @Override
    public AppRetirementModeLookUp getAppRetirementModeLookUp(Integer retirementModeId) {
        AppRetirementModeLookUp appRetirementMode = new AppRetirementModeLookUp();
        List<RetirementModeLookUp> retirementModes = retirementModeLookUpDataManager.getRetirementMode(retirementModeId);
        
        appRetirementMode = getAppRetirementModeLookUp(retirementModes.get(0));
        return appRetirementMode;
    }
}
