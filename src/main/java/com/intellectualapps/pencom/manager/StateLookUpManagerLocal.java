/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.manager;

import com.intellectualapps.pencom.model.StateLookUp;
import com.intellectualapps.pencom.pojo.AppStateLookUp;
import java.util.List;

/**
 *
 * @author Lateefah
 */
public interface StateLookUpManagerLocal {
    List<AppStateLookUp> getStates();
    
    AppStateLookUp getAppStateLookUp(StateLookUp state);
}
