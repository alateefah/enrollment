/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.manager;

import com.intellectualapps.pencom.data.manager.BiometricDataManagerLocal;        
import com.intellectualapps.pencom.model.PensionerBiometric;
import com.intellectualapps.pencom.pojo.AppPensionerBiometric;
import java.util.Date;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Lateefah
 */

@Stateless
public class PensionerBiometricManager implements PensionerBiometricManagerLocal{
    
    @EJB
    private BiometricDataManagerLocal biometricDataManager;
    
    @Override
    public AppPensionerBiometric addBiometric (String pensionerId, String typeId, 
            String subtypeId, String biometricData, String enroler) {
        PensionerBiometric biometric = new PensionerBiometric();
        if(typeId != null && !typeId.isEmpty() && subtypeId != null && !subtypeId.isEmpty() && biometricData != null && !biometricData.isEmpty()){
            try {
                biometric.setPensionerId(pensionerId);
                biometric.setTypeId(typeId);
                biometric.setSubTypeId(subtypeId);
                biometric.setBiometricData(biometricData);
                biometric.setEnroledBy(enroler);
                biometric.setDateEnroled(new Date());

                biometricDataManager.create(biometric);
                return getAppBiometric(biometric);
            } catch(Exception e) {
                return null; 
            }
            
        } else {
            return null;
        }        
    }
    
    @Override
    public AppPensionerBiometric updateBiometric (String pensionerId, String typeId, 
            String subtypeId, String biometricData, String enroler) {
        
        PensionerBiometric biometric = biometricDataManager.get(pensionerId);
        if(biometric != null){
            try {                
                biometric.setTypeId(typeId);
                biometric.setSubTypeId(subtypeId);
                biometric.setBiometricData(biometricData);
                biometric.setEnroledBy(enroler);
                biometric.setDateEnroled(new Date());

                biometricDataManager.update(biometric);
                return getAppBiometric(biometric);
            } catch(Exception e) {
                return null; 
            }
            
        } else {
            return null;
        }        
    }
    
    @Override
    public AppPensionerBiometric getAppBiometric(String pensionerId) {
        return getAppBiometric(biometricDataManager.get(pensionerId));
    }    

    @Override
    public AppPensionerBiometric getAppBiometric(PensionerBiometric biometric) {
        AppPensionerBiometric appBiometric = null;
        
        if (biometric != null) {
            appBiometric = new AppPensionerBiometric();
            appBiometric.setPensionerId(biometric.getPensionerId());
            appBiometric.setTypeId(biometric.getTypeId());
            appBiometric.setSubTypeId(biometric.getSubTypeId());
            appBiometric.setBiometricData(biometric.getBiometricData());
            appBiometric.setEnroledBy(biometric.getEnroledBy());
            appBiometric.setDateEnroled(biometric.getDateEnroled());
        }
        
        return appBiometric;
    }
}
