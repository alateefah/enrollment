/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.manager;

import com.intellectualapps.pencom.model.ServiceOrganisationLookUp;
import com.intellectualapps.pencom.pojo.AppServiceOrganisationLookUp;
import java.util.List;

/**
 *
 * @author Lateefah
 */
public interface ServiceOrganisationLookUpManagerLocal {
    
    List<AppServiceOrganisationLookUp> getServiceOrganisations();
    
    AppServiceOrganisationLookUp getAppServiceOrganisationLookUp(ServiceOrganisationLookUp serviceOrganisation);
}
