/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.manager;

import com.intellectualapps.pencom.model.RetirementModeLookUp;
import com.intellectualapps.pencom.pojo.AppRetirementModeLookUp;
import java.util.List;

/**
 *
 * @author Lateefah
 */
public interface RetirementModeLookUpManagerLocal {
    List<AppRetirementModeLookUp> getRetirementModes();
    
    AppRetirementModeLookUp getAppRetirementModeLookUp(RetirementModeLookUp retirementMode);
    
    AppRetirementModeLookUp getAppRetirementModeLookUp(Integer retirementModeId);
    
}
