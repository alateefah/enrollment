/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.manager;

import com.intellectualapps.pencom.model.PfaLookUp;
import com.intellectualapps.pencom.pojo.AppPfaLookUp;
import java.util.List;

/**
 *
 * @author Lateefah
 */
public interface PfaLookUpManagerLocal {
    List<AppPfaLookUp> getPfas();
    
    AppPfaLookUp getAppPfaLookUp(PfaLookUp Pfa);
    
    AppPfaLookUp getAppPfaLookUp(Integer pfaId);
}
