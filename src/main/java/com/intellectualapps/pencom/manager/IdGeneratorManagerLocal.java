/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.manager;

import com.intellectualapps.pencom.model.PensionerBiometric;
import com.intellectualapps.pencom.model.Id;
import com.intellectualapps.pencom.pojo.AppPensionerBiometric;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author buls
 */

@Local
public interface IdGeneratorManagerLocal {
    List<Id> generateIds(int numberOfIds);
    
    Id getId();
    
    void removeId(Id id);
}
