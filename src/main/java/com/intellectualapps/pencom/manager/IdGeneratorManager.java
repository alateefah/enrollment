/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.manager;

import com.intellectualapps.pencom.data.manager.IdGeneratorDataManagerLocal;
import com.intellectualapps.pencom.model.Id;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import javax.ejb.EJB;
import javax.ejb.Stateless;


/**
 *
 * @author buls
 */
@Stateless
public class IdGeneratorManager implements IdGeneratorManagerLocal{

    @EJB
    IdGeneratorDataManagerLocal idGeneratorDataManager;
    
    @Override
    public List<Id> generateIds(int numberOfIds) {
        List<Id> ids = new ArrayList<Id>();
        for (int i = 0; i < numberOfIds; i++) {
            Integer randomInt = ThreadLocalRandom.current().nextInt(100000, 999999);
            Id id = new Id(randomInt.toString());
            id = idGeneratorDataManager.create(id);
            ids.add(id);
        }
        
        return ids;
    }

    @Override
    public Id getId() {        
        return idGeneratorDataManager.getId();
    }

    @Override
    public void removeId(Id id) {
        idGeneratorDataManager.delete(id);
    }
    
}
