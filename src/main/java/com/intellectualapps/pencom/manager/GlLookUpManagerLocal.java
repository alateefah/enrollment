/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.manager;

import com.intellectualapps.pencom.model.GlLookUp;
import com.intellectualapps.pencom.pojo.AppGlLookUp;
import java.util.List;

/**
 *
 * @author Lateefah
 */
public interface GlLookUpManagerLocal {
    List<AppGlLookUp> getGls();
    
    AppGlLookUp getAppGlLookUp(GlLookUp gl);
}
