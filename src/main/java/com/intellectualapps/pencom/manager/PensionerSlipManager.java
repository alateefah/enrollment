/*
 */
package com.intellectualapps.pencom.manager;


import com.intellectualapps.pencom.pojo.AppPensionerBasic;
import com.intellectualapps.pencom.report.manager.PensionerSlipReportManager;
import com.intellectualapps.pencom.report.model.PensionerSlip;
import static com.intellectualapps.pencom.util.ImageUtil.convertToImage;
import static com.intellectualapps.pencom.util.ImageUtil.getLogo;
import static com.intellectualapps.pencom.util.ImageUtil.getWatermark;
import java.awt.Image;
import java.net.URL;
import java.text.SimpleDateFormat;
import net.sf.jasperreports.engine.JRException;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author buls
 */
@Stateless
public class PensionerSlipManager implements PensionerSlipManagerLocal {    
    @EJB
    PensionerManagerLocal pensionerBasicManager;
    @EJB
    PensionerEmploymentManagerLocal pensionerEmploymentManager;
    @EJB
    PfaLookUpManagerLocal pfaLookupManager;
    @EJB
    EmployerLookUpManagerLocal employerLookupManager;
    @EJB
    RetirementModeLookUpManagerLocal retirementModeLookupManager;

    @Override
    public byte[] getPensionerSlip(String pensionerId, URL logoUrl, URL watermarkUrl) {     
        AppPensionerBasic pensioner = pensionerBasicManager.getAppPensionerBasic(pensionerId);        
        PensionerSlip pensionerSlip = setupPensionerSlip(pensioner, logoUrl, watermarkUrl);
        PensionerSlipReportManager repMngr = 
                new PensionerSlipReportManager(pensionerSlip);                
        try {
            return repMngr.getPdf();
        } catch (JRException ex) {
            Logger.getLogger(PensionerSlipManager.class.getName()).log(Level.SEVERE,
                    null, ex);
            throw new EJBException(ex);
        }
    }
    
    private PensionerSlip setupPensionerSlip(AppPensionerBasic appPensionerBasic, URL logoUrl, URL watermarkUrl) {  
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
        PensionerSlip pensionerSlip = new PensionerSlip();
        pensionerSlip.setRsaNumber(appPensionerBasic.getRsaNumber());
        pensionerSlip.setPfa(pfaLookupManager.getAppPfaLookUp(appPensionerBasic.getPfaId()).getName()); 
        pensionerSlip.setFullName(appPensionerBasic.getFirstName() + " " + appPensionerBasic.getMiddleName() + " " + appPensionerBasic.getSurname());
        pensionerSlip.setDateOfBirth(dateFormatter.format(appPensionerBasic.getDateOfBirth()));
        pensionerSlip.setGender(appPensionerBasic.getGenderId());
        pensionerSlip.setEmployer(employerLookupManager.getAppEmployerLookUp(appPensionerBasic.getPensionerEmployment().getEmployerId()).getEmployerName()); 
        pensionerSlip.setEmployerCode(appPensionerBasic.getPensionerEmployment().getEmployerId().toString());
        pensionerSlip.setGradeStep2004(appPensionerBasic.getPensionerEmployment().getGradeLevel2004()+"/"+appPensionerBasic.getPensionerEmployment().getStep2004());
        pensionerSlip.setGradeStep2007(appPensionerBasic.getPensionerEmployment().getGradeLevelConsolidated()+"/"+appPensionerBasic.getPensionerEmployment().getStepConsolidated());
        pensionerSlip.setGradeStep2010(appPensionerBasic.getPensionerEmployment().getEnhancedGradeLevelConsolidated()+"/"+appPensionerBasic.getPensionerEmployment().getEnhancedStepConsolidated());
        pensionerSlip.setRetirementDate(dateFormatter.format(appPensionerBasic.getPensionerEmployment().getExpectedRetirementDate()));
        pensionerSlip.setRetirementMode(retirementModeLookupManager.getAppRetirementModeLookUp(appPensionerBasic.getPensionerEmployment().getRetirementModeId()).getDescription()); 
        pensionerSlip.setDateOfFirstAppointment(dateFormatter.format(appPensionerBasic.getPensionerEmployment().getFirstAppointmentDate()));        
        Image pensionerPhoto = convertToImage(appPensionerBasic.getPensionerBiometrics().get("ps").getBiometricData());        
        pensionerSlip.setPhoto(pensionerPhoto);
        pensionerSlip.setPensionerId(appPensionerBasic.getPensionerId());
        pensionerSlip.setRegistrationDate(dateFormatter.format(appPensionerBasic.getDateEnroled()));
        pensionerSlip.setRegistrationOfficer(appPensionerBasic.getEnroledBy());
        pensionerSlip.setLogo(getLogo(logoUrl));
        pensionerSlip.setWatermark(getWatermark(watermarkUrl));
        
        return pensionerSlip;
    }
   
}
