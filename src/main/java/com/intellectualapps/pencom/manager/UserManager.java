/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.manager;

import com.intellectualapps.pencom.data.manager.PrivilegeDataManagerLocal;
import com.intellectualapps.pencom.data.manager.RoleDataManagerLocal;
import com.intellectualapps.pencom.data.manager.UserDataManagerLocal;
import com.intellectualapps.pencom.data.manager.UserRoleDataManagerLocal;
import com.intellectualapps.pencom.model.Privilege;
import com.intellectualapps.pencom.model.Role;
import com.intellectualapps.pencom.model.RolePrivilege;
import com.intellectualapps.pencom.model.User;
import com.intellectualapps.pencom.model.UserRole;
import com.intellectualapps.pencom.model.UserRolePK;
import com.intellectualapps.pencom.pojo.AppPrivilege;
import com.intellectualapps.pencom.pojo.AppRole;
import com.intellectualapps.pencom.pojo.AppUser;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import com.intellectualapps.pencom.util.MD5;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author buls
 */
@Stateless
public class UserManager implements UserManagerLocal{
    
    @EJB
    private UserDataManagerLocal userDataManager;
    @EJB
    private RoleDataManagerLocal roleDataManager;
    @EJB
    private UserRoleDataManagerLocal userRoleDataManager;
    
    @Override
    public AppUser authenticateUser (String username, String password) {        
        User user = userDataManager.get(username);
        if (user != null && user.getPassword().equals(MD5.hash(password))) {
            return getAppUser(user);            
        } else {
            return null;
        }        
    }

    @Override
    public AppUser getAppUser(String userId) {
        AppUser appUser = new AppUser();
        appUser.setUsername(userId);
        return appUser;
        //return null;
    }    

    @Override
    public AppUser getAppUser(User user) {
        AppUser appUser = new AppUser();
        appUser.setUsername(user.getUsername());
        appUser.setFirstName(user.getFirstName());
        appUser.setOtherNames(user.getOtherNames());
        List<AppRole> userRoles = new ArrayList<AppRole>();
        List<UserRole> roles = getUserRoles(user.getUsername());
        for (UserRole ur : roles) {
            AppRole appRole = new AppRole();
            appRole.setRoleId(ur.getRole().getRoleId());
            appRole.setDescription(ur.getRole().getDescription());
            for (RolePrivilege rolePrivilege : ur.getRole().getRoleAndPrivilegeList()) {
                if (rolePrivilege.getRoleAndPrivilegePK().getRoleId().equals(appRole.getRoleId())){
                    AppPrivilege appPrivilege = new AppPrivilege();
                    appPrivilege.setPrivilegeId(rolePrivilege.getPrivilege().getPrivilegeId());
                    appPrivilege.setDescription(rolePrivilege.getPrivilege().getDescription());
                    appPrivilege.setMenuLink(rolePrivilege.getPrivilege().getMenuLink());
                    appPrivilege.setMenuIcon(rolePrivilege.getPrivilege().getMenuIcon());
                    appRole.setPrivilege(appPrivilege);
                }
            }            
            userRoles.add(appRole);
        }
        appUser.setUserRoles(userRoles);
        
        return appUser;
    }

    @Override    
    public List<AppUser> getAllUsers() {
        List<AppUser> appUsers = new ArrayList<>();
        List<User> users = userDataManager.getAllUsers();
        for (User user : users) {
            appUsers.add(getAppUser(user));
        }
        return appUsers;
    }
    
    @Override
    public AppUser addUser(String username, String firstname, String othernames) {        
        AppUser appUser = null; 
        
        if (username != null && firstname != null && othernames != null) {
            User user = new User();
            
            user.setUsername(username);
            user.setFirstName(firstname);
            user.setOtherNames(othernames);
            user.setPassword(MD5.hash("password"));
            
            userDataManager.create(user);
            return getAppUser(user);
        } 
        return appUser;
    }
     
    @Override
    public Boolean deleteUser(String username) {
        if (username != null){
            User user = userDataManager.get(username);
            userDataManager.delete(user);
            return true;
        }
        return false;    
    }
    
    @Override
    public AppUser updateUser(String username, String firstname, String othernames) {        
        AppUser appUser = null;         
        
        if (username != null && firstname != null && othernames != null) {
            User user = userDataManager.get(username);
            
            user.setUsername(username);
            user.setFirstName(firstname);
            user.setOtherNames(othernames);
            
            userDataManager.update(user);
            appUser = getAppUser(user);
            return appUser;
        } 
        return appUser;
    }
    
    @Override
    public List<UserRole> getUserRoles(String userId) {
        return userRoleDataManager.getByUser(userId);
    }
    
    @Override
    public Boolean addUserRole(String username, String roleId){
        if(username != null && roleId != null){     
            UserRole userRole = new UserRole();
            
            Role role = new Role();
            role.setRoleId(roleId);
            
            User user = new User();
            user.setUsername(username);
            
            UserRolePK userRolePK = new UserRolePK();
            userRolePK.setRoleId(roleId);
            userRolePK.setUsername(username);
            
            userRole.setRole(role);
            userRole.setUser(user);
            
            userRole.setUserRolePK(userRolePK);
                                             
            userRoleDataManager.create(userRole);            
            return true;            
        }
        return false;   
    }
    
    @Override
    public Boolean deleteUserRole(String username, String roleId){
        if(username != null && roleId != null){
            List<UserRole> fetchedUserRoles = userRoleDataManager.getUsernameAndRoleId(username, roleId);            
            for (UserRole userRole : fetchedUserRoles){
                userRoleDataManager.delete(userRole); 
            }                      
            return true;     
        }
        return false;
    }
}
