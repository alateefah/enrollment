/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.manager;

import com.intellectualapps.pencom.data.manager.RankLookUpDataManagerLocal;
import com.intellectualapps.pencom.model.RankLookUp;
import com.intellectualapps.pencom.pojo.AppRankLookUp;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Lateefah
 */
@Stateless
public class RankLookUpManager implements RankLookUpManagerLocal {
    @EJB
    private RankLookUpDataManagerLocal rankLookUpDataManager;
    
    @Override
    public List<AppRankLookUp> getRanks() {
        List<AppRankLookUp> appRanks = new ArrayList<AppRankLookUp>();
        List<RankLookUp> ranks = rankLookUpDataManager.getAllRanks();
        for (RankLookUp rank : ranks) {
            appRanks.add(getAppRankLookUp(rank));
        }
        return appRanks;
    }
           
    @Override
    public AppRankLookUp getAppRankLookUp(RankLookUp rank){
        AppRankLookUp appRank = new AppRankLookUp();
        
        appRank.setRankId(rank.getRankId());
        appRank.setDescription(rank.getDescription());
        
        return appRank;
    }
    
}
