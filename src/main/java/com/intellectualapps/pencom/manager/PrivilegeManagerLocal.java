/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.manager;

import com.intellectualapps.pencom.model.Privilege;
import com.intellectualapps.pencom.pojo.AppPrivilege;
import java.util.List;

/**
 *
 * @author buls
 */
public interface PrivilegeManagerLocal {
    
    Privilege getPrivilege (String privilegeId);
    
    List<AppPrivilege> getAllPrivileges ();
    
    AppPrivilege getAppPrivilege(Privilege privilege);
    
    AppPrivilege addPrivilege(String privilegeId, String description, String menuLink);
    
    Boolean deletePrivilege(String privilegeId);
    
    AppPrivilege updatePrivilege(String privilegeId, String description, String menuLink, String menuIcon);
}
