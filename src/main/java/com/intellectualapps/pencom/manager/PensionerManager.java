/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.manager;

import com.intellectualapps.pencom.data.manager.PensionerDataManagerLocal;
import com.intellectualapps.pencom.data.manager.PensionerEmploymentDataManagerLocal;
import com.intellectualapps.pencom.data.manager.PrivilegeDataManagerLocal;
import com.intellectualapps.pencom.data.manager.RoleDataManagerLocal;
import com.intellectualapps.pencom.data.manager.UserDataManagerLocal;
import com.intellectualapps.pencom.data.manager.UserRoleDataManagerLocal;
import com.intellectualapps.pencom.model.Id;
import com.intellectualapps.pencom.model.PensionerBasic;
import com.intellectualapps.pencom.model.PensionerEmployment;
import com.intellectualapps.pencom.model.Privilege;
import com.intellectualapps.pencom.model.RolePrivilege;
import com.intellectualapps.pencom.model.User;
import com.intellectualapps.pencom.model.UserRole;
import com.intellectualapps.pencom.pojo.AppPensionerBasic;
import com.intellectualapps.pencom.pojo.AppPensionerBiometric;
import com.intellectualapps.pencom.pojo.AppPensionerEmployment;
import com.intellectualapps.pencom.pojo.AppPrivilege;
import com.intellectualapps.pencom.pojo.AppRole;
import com.intellectualapps.pencom.pojo.AppUser;
import com.intellectualapps.pencom.util.DateParser;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import com.intellectualapps.pencom.util.MD5;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author buls
 */
@Stateless
public class PensionerManager implements PensionerManagerLocal{
    
    @EJB
    private PensionerDataManagerLocal pensionerDataManager;
    @EJB
    private PensionerEmploymentManagerLocal pensionerEmploymentManager;
    @EJB
    private PensionerBiometricManagerLocal pensionerBiometricManager;    
    @EJB
    private IdGeneratorManagerLocal idGeneratorManager;
            
    @Override
    public AppPensionerBasic savePensioner (String existingPensionerId, String firstName, String middleName, 
            String surname, String genderId, String serviceOrgId, String motherMaidenName, 
            String honorificId, String pfaId, String rsaNumber, String maritalStatusId, 
            String dateOfBirth, String email, String contactAddress, String contactPhoneNumber, 
            String academic, String employerId, String employerAddress, String stateOfService, 
            String staffId, String expectedRetirementDate, String firstAppointmentDate, 
            String retirementModeId, String salaryStructure2004, String gradeLevel2004, 
            String step2004, String salaryStructureConsolidated, String gradeLevelConsolidated, 
            String stepConsolidated, String salaryStructureEnhancedConsolidated, 
            String gradeLevelEnhancedConsolidated, String stepEnhancedConsolidated,
            String typeId, String subtypeId, String biometricData, String username) throws ParseException{
        
        AppPensionerBasic appPensionerBasic = null;
        
        Date formattedDob = null;
        if (firstName != null && middleName != null && surname != null && genderId != null && motherMaidenName != null && 
            honorificId != null && rsaNumber != null && maritalStatusId != null && dateOfBirth  != null && email != null && contactAddress != null && 
            contactPhoneNumber != null && academic != null) {
            
            formattedDob = new DateParser().parseDate(dateOfBirth);                            
            
            PensionerBasic pensionerBasic = null;            
            if (existingPensionerId != null && !existingPensionerId.equals("")) {
                pensionerBasic = pensionerDataManager.get(existingPensionerId);
            } else {
                pensionerBasic = new PensionerBasic();
            }
            

            pensionerBasic.setFirstName(firstName);
            pensionerBasic.setMiddleName(middleName);
            pensionerBasic.setSurname(surname);
            pensionerBasic.setGenderId(genderId);
            pensionerBasic.setServiceOrgId(Integer.parseInt(serviceOrgId));
            pensionerBasic.setMotherMaidenName(motherMaidenName);
            pensionerBasic.setHonorificId(honorificId);
            pensionerBasic.setPfaId(Integer.parseInt(pfaId));
            pensionerBasic.setRsaNumber(rsaNumber);
            pensionerBasic.setMaritalStatusId(maritalStatusId);
            pensionerBasic.setDateOfBirth(formattedDob);
            pensionerBasic.setEmail(email);
            pensionerBasic.setContactAddress(contactAddress);
            pensionerBasic.setContactPhoneNumber(contactPhoneNumber);
            pensionerBasic.setAcademic(academic);
            pensionerBasic.setEnroledBy(username);
            pensionerBasic.setDateEnroled(new Date());
            
            
            Id pensionerId = null;
            if (existingPensionerId != null && !existingPensionerId.equals("")) {
                pensionerId = new Id(existingPensionerId);
            } else {
                pensionerId = idGeneratorManager.getId();
            }            
            
            if (pensionerId != null) {
                AppPensionerEmployment appPensionerEmployment = 
                        pensionerEmploymentManager.savePensionerEmployment(pensionerBasic.getEmploymentId(), employerId, 
                                employerAddress, stateOfService, staffId, expectedRetirementDate, 
                                firstAppointmentDate, retirementModeId, salaryStructure2004, 
                                gradeLevel2004, step2004, salaryStructureConsolidated, 
                                gradeLevelConsolidated, stepConsolidated, 
                                salaryStructureEnhancedConsolidated, gradeLevelEnhancedConsolidated, 
                                stepEnhancedConsolidated);

                pensionerBasic.setEmploymentId(appPensionerEmployment.getEmploymentId());
                pensionerBasic.setPensionerId(pensionerId.getId());
                if (existingPensionerId != null && !existingPensionerId.equals("")) {
                    appPensionerBasic = getAppPensionerBasic(pensionerDataManager.update(pensionerBasic));
                } else {
                    appPensionerBasic = getAppPensionerBasic(pensionerDataManager.create(pensionerBasic));
                }

                AppPensionerBiometric appPensionerBiometric = null;
                if (existingPensionerId != null && !existingPensionerId.equals("")) {
                    appPensionerBiometric = 
                        pensionerBiometricManager.updateBiometric(appPensionerBasic.getPensionerId(), typeId, subtypeId, biometricData, username);
                } else {
                    appPensionerBiometric = 
                        pensionerBiometricManager.addBiometric(appPensionerBasic.getPensionerId(), typeId, subtypeId, biometricData, username);
                }
                
                appPensionerBasic.setPensionerEmployment(appPensionerEmployment);
                appPensionerBasic.setPensionerBiometric(appPensionerBiometric);
                idGeneratorManager.removeId(pensionerId);
            } else {
                return appPensionerBasic;
            }
            
        }
                
        return appPensionerBasic;      
    }

    @Override
    public AppPensionerBasic getAppPensionerBasic(String pensionerId) {
        return getAppPensionerBasic(pensionerDataManager.get(pensionerId));
    }    
    
    public AppPensionerBasic getAppPensionerBasic(PensionerBasic pensionerBasic) {
        AppPensionerBasic appPensionerBasic = new AppPensionerBasic();
        appPensionerBasic.setPensionerId(pensionerBasic.getPensionerId());
        appPensionerBasic.setFirstName(pensionerBasic.getFirstName());
        appPensionerBasic.setMiddleName(pensionerBasic.getMiddleName());
        appPensionerBasic.setSurname(pensionerBasic.getSurname());
        appPensionerBasic.setGenderId(pensionerBasic.getGenderId());
        appPensionerBasic.setServiceOrgId(pensionerBasic.getServiceOrgId());
        appPensionerBasic.setMotherMaidenName(pensionerBasic.getMotherMaidenName());
        appPensionerBasic.setHonorificId(pensionerBasic.getHonorificId());
        appPensionerBasic.setPfaId(pensionerBasic.getPfaId());
        appPensionerBasic.setRsaNumber(pensionerBasic.getRsaNumber());
        appPensionerBasic.setMaritalStatusId(pensionerBasic.getMaritalStatusId());
        appPensionerBasic.setDateOfBirth(pensionerBasic.getDateOfBirth());
        appPensionerBasic.setEmail(pensionerBasic.getEmail());
        appPensionerBasic.setContactAddress(pensionerBasic.getContactAddress());
        appPensionerBasic.setContactPhoneNumber(pensionerBasic.getContactPhoneNumber());
        appPensionerBasic.setAcademic(pensionerBasic.getAcademic());  
        appPensionerBasic.setEnroledBy(pensionerBasic.getEnroledBy());
        appPensionerBasic.setDateEnroled(pensionerBasic.getDateEnroled());
        AppPensionerEmployment appPensionerEmployment = pensionerEmploymentManager.getAppPensionerEmployment(pensionerBasic.getEmploymentId());
        appPensionerBasic.setPensionerEmployment(appPensionerEmployment);
        AppPensionerBiometric appPensionerBiometric = pensionerBiometricManager.getAppBiometric(appPensionerBasic.getPensionerId());
        appPensionerBasic.setPensionerBiometric(appPensionerBiometric);
        
        return appPensionerBasic;
    }

    @Override
    public List<AppPensionerBasic> searchPensioner(String searchInput) {
        List<PensionerBasic> pensioners = pensionerDataManager.searchPensioner(searchInput);
        
        List<AppPensionerBasic> pensionersFound = new ArrayList<AppPensionerBasic>();
        for (PensionerBasic pensioner : pensioners) {
            pensionersFound.add(getAppPensionerBasic(pensioner));
        }
        
        return pensionersFound;
    }
    
}
