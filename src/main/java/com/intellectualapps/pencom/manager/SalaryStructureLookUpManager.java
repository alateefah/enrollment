/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.manager;

import com.intellectualapps.pencom.data.manager.SalaryStructureLookUpDataManagerLocal;
import com.intellectualapps.pencom.model.SalaryStructureLookUp;
import com.intellectualapps.pencom.pojo.AppSalaryStructureLookUp;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
/**
 *
 * @author Lateefah
 */
@Stateless
public class SalaryStructureLookUpManager implements SalaryStructureLookUpManagerLocal{
    @EJB
    private SalaryStructureLookUpDataManagerLocal salaryStructureLookUpDataManager;
    
    @Override
    public List<AppSalaryStructureLookUp> getAllSalaryStructures() {
        List<AppSalaryStructureLookUp> appSalaryStructures = new ArrayList<AppSalaryStructureLookUp>();
        List<SalaryStructureLookUp> salaryStructures = salaryStructureLookUpDataManager.getAllSalaryStructures();
        for (SalaryStructureLookUp salaryStructure : salaryStructures) {
            appSalaryStructures.add(getAppSalaryStructureLookUp(salaryStructure));
        }
        return appSalaryStructures;
    }
           
    @Override
    public AppSalaryStructureLookUp getAppSalaryStructureLookUp(SalaryStructureLookUp salaryStructure){
        AppSalaryStructureLookUp appSalaryStructure = new AppSalaryStructureLookUp();
        
        appSalaryStructure.setSalaryStructureId(salaryStructure.getSalaryStructureId());
        appSalaryStructure.setDescription(salaryStructure.getDescription());
        
        return appSalaryStructure;
    }

    @Override
    public List<AppSalaryStructureLookUp> getAll2004SalaryStructures() {
        List<AppSalaryStructureLookUp> appSalaryStructures = new ArrayList<AppSalaryStructureLookUp>();
        List<SalaryStructureLookUp> salaryStructures = salaryStructureLookUpDataManager.getAll2004SalaryStructures();
        for (SalaryStructureLookUp salaryStructure : salaryStructures) {
            appSalaryStructures.add(getAppSalaryStructureLookUp(salaryStructure));
        }
        return appSalaryStructures;
    }

    @Override
    public List<AppSalaryStructureLookUp> getAllConsolidatedSalaryStructures() {
        List<AppSalaryStructureLookUp> appSalaryStructures = new ArrayList<AppSalaryStructureLookUp>();
        List<SalaryStructureLookUp> salaryStructures = salaryStructureLookUpDataManager.getAllConsolidatedSalaryStructures();
        for (SalaryStructureLookUp salaryStructure : salaryStructures) {
            appSalaryStructures.add(getAppSalaryStructureLookUp(salaryStructure));
        }
        return appSalaryStructures;
    }

    @Override
    public List<AppSalaryStructureLookUp> getAllEnhancedConsolidatedSalaryStructures() {
        List<AppSalaryStructureLookUp> appSalaryStructures = new ArrayList<AppSalaryStructureLookUp>();
        List<SalaryStructureLookUp> salaryStructures = salaryStructureLookUpDataManager.getAllEnhancedConsolidatedSalaryStructures();
        for (SalaryStructureLookUp salaryStructure : salaryStructures) {
            appSalaryStructures.add(getAppSalaryStructureLookUp(salaryStructure));
        }
        return appSalaryStructures;
    }
}
