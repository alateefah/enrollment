/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.manager;

import com.intellectualapps.pencom.model.PensionerBiometric;
import com.intellectualapps.pencom.pojo.AppPensionerBiometric;

/**
 *
 * @author Lateefah
 */
public interface PensionerBiometricManagerLocal {
    AppPensionerBiometric addBiometric(String pensionerId, String typeId, String subtypeId, String biometricData, String enroler);
    
    AppPensionerBiometric updateBiometric(String pensionerId, String typeId, String subtypeId, String biometricData, String enroler);
    
    AppPensionerBiometric getAppBiometric (String pensionerId);
    
    AppPensionerBiometric getAppBiometric (PensionerBiometric biometric);
}
