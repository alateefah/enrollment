/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.manager;

import com.intellectualapps.pencom.data.manager.EmployerLookUpDataManagerLocal;
import com.intellectualapps.pencom.model.EmployerLookUp;
import com.intellectualapps.pencom.pojo.AppEmployerLookUp;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Lateefah
 */
@Stateless
public class EmployerLookUpManager implements EmployerLookUpManagerLocal{
    @EJB
    private EmployerLookUpDataManagerLocal employerLookUpDataManager;
    
    @Override
    public List<AppEmployerLookUp> getEmployers() {
        List<AppEmployerLookUp> appEmployers = new ArrayList<AppEmployerLookUp>();
        List<EmployerLookUp> employers = employerLookUpDataManager.getAllEmployers();
        for (EmployerLookUp employer : employers) {
            appEmployers.add(getAppEmployerLookUp(employer));
        }
        return appEmployers;
    }
           
    @Override
    public AppEmployerLookUp getAppEmployerLookUp(EmployerLookUp employer){
        AppEmployerLookUp appEmployer = new AppEmployerLookUp();
        
        appEmployer.setEmployerId(employer.getEmployerId());
        appEmployer.setEmployerName(employer.getEmployerName());
                
        return appEmployer;
    }

    @Override
    public AppEmployerLookUp getAppEmployerLookUp(Integer employerId) {
        AppEmployerLookUp appEmployer = new AppEmployerLookUp();
        List<EmployerLookUp> employers = employerLookUpDataManager.getEmployer(employerId);
        appEmployer = getAppEmployerLookUp(employers.get(0));
        
        return appEmployer;
    }
}
