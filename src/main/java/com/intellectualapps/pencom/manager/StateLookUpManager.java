/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.manager;

import com.intellectualapps.pencom.data.manager.StateLookUpDataManagerLocal;
import com.intellectualapps.pencom.model.StateLookUp;
import com.intellectualapps.pencom.pojo.AppStateLookUp;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Lateefah
 */
@Stateless
public class StateLookUpManager implements StateLookUpManagerLocal {
    @EJB
    private StateLookUpDataManagerLocal stateLookUpDataManager;
    
    @Override
    public List<AppStateLookUp> getStates() {
        List<AppStateLookUp> appStates = new ArrayList<AppStateLookUp>();
        List<StateLookUp> states = stateLookUpDataManager.getAllStates();
        for (StateLookUp state : states) {
            appStates.add(getAppStateLookUp(state));
        }
        return appStates;
    }
           
    @Override
    public AppStateLookUp getAppStateLookUp(StateLookUp state){
        AppStateLookUp appState = new AppStateLookUp();
        
        appState.setStateId(state.getStateId());
        appState.setName(state.getName());
                
        return appState;
    }
}
