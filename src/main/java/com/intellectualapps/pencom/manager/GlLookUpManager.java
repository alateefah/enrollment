/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.manager;

import com.intellectualapps.pencom.data.manager.GlLookUpDataManagerLocal;
import com.intellectualapps.pencom.model.GlLookUp;
import com.intellectualapps.pencom.pojo.AppGlLookUp;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Lateefah
 */
@Stateless
public class GlLookUpManager implements GlLookUpManagerLocal{
    @EJB
    private GlLookUpDataManagerLocal glLookUpDataManager;
    
    @Override
    public List<AppGlLookUp> getGls() {
        List<AppGlLookUp> appGls = new ArrayList<AppGlLookUp>();
        List<GlLookUp> gls = glLookUpDataManager.getAllGls();
        for (GlLookUp gl : gls) {
            appGls.add(getAppGlLookUp(gl));
        }
        return appGls;
    }
           
    @Override
    public AppGlLookUp getAppGlLookUp(GlLookUp gl){
        AppGlLookUp appGl = new AppGlLookUp();
        
        appGl.setGlId(gl.getGlId());
        appGl.setDescription(gl.getDescription());
        
        return appGl;
    }
    
}
