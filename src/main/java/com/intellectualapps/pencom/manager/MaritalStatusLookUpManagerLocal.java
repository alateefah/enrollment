/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.manager;

import com.intellectualapps.pencom.model.MaritalStatusLookUp;
import com.intellectualapps.pencom.pojo.AppMaritalStatusLookUp;
import java.util.List;

/**
 *
 * @author Lateefah
 */
public interface MaritalStatusLookUpManagerLocal {
    List<AppMaritalStatusLookUp> getMaritalStatuses();
    
    AppMaritalStatusLookUp getAppMaritalStatusLookUp(MaritalStatusLookUp maritalStatus);
}
