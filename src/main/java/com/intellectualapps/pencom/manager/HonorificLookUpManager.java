/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.manager;

import com.intellectualapps.pencom.data.manager.HonorificLookUpDataManagerLocal;
import com.intellectualapps.pencom.model.HonorificLookUp;
import com.intellectualapps.pencom.pojo.AppHonorificLookUp;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Lateefah
 */
@Stateless
public class HonorificLookUpManager implements HonorificLookUpManagerLocal{
    @EJB
    private HonorificLookUpDataManagerLocal honorificLookUpDataManager;
    
    @Override
    public List<AppHonorificLookUp> getHonorifics() {
        List<AppHonorificLookUp> appHonorifics = new ArrayList<AppHonorificLookUp>();
        List<HonorificLookUp> honorifics = honorificLookUpDataManager.getAllHonorifics();
        for (HonorificLookUp honorific : honorifics) {
            appHonorifics.add(getAppHonorificLookUp(honorific));
        }
        return appHonorifics;
    }
           
    @Override
    public AppHonorificLookUp getAppHonorificLookUp(HonorificLookUp honorific){
        AppHonorificLookUp appHonorific = new AppHonorificLookUp();
        
        appHonorific.setHonorificId(honorific.getHonorificId());
        appHonorific.setDescription(honorific.getDescription());
        
        return appHonorific;
    }
}
