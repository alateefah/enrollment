/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.manager;

import com.intellectualapps.pencom.data.manager.PrivilegeDataManagerLocal;
import com.intellectualapps.pencom.data.manager.RoleDataManagerLocal;
import com.intellectualapps.pencom.data.manager.UserDataManagerLocal;
import com.intellectualapps.pencom.model.Privilege;
import com.intellectualapps.pencom.model.Role;
import com.intellectualapps.pencom.model.RolePrivilege;
import com.intellectualapps.pencom.model.RolePrivilegePK;
import com.intellectualapps.pencom.model.User;
import com.intellectualapps.pencom.pojo.AppPrivilege;
import com.intellectualapps.pencom.pojo.AppRole;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import com.intellectualapps.pencom.util.MD5;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author buls
 */
@Stateless
public class RoleManager implements RoleManagerLocal{
        
    @EJB
    private RoleDataManagerLocal roleDataManager;
    private PrivilegeDataManagerLocal privilegeDataManager;
    
    @Override
    public Role getRole(String roleId) {
        return roleDataManager.get(roleId);
    }
    
    @Override
    public List<AppRole> getAllRoles() {
        List<AppRole> appRoles = new ArrayList<>();
        List<Role> roles = roleDataManager.getAllRoles();
        for (Role role : roles) {
            appRoles.add(getAppRole(role));
        }
        return appRoles;
    }
    
    @Override
    public AppRole getAppRole(Role role) {
        AppRole appRole = new AppRole();
        appRole.setRoleId(role.getRoleId());
        appRole.setDescription(role.getDescription());
        
        List<AppPrivilege> rolePrivileges = new ArrayList<AppPrivilege>();
        List<RolePrivilege> privileges = getRolePrivileges(role.getRoleId());        
        
        for (RolePrivilege rolePrivilege : privileges) {
            AppPrivilege appPrivilege = new AppPrivilege();
            appPrivilege.setPrivilegeId(rolePrivilege.getPrivilege().getPrivilegeId());
            appPrivilege.setDescription(rolePrivilege.getPrivilege().getDescription());
            appPrivilege.setMenuLink(rolePrivilege.getPrivilege().getMenuLink());            
            
            rolePrivileges.add(appPrivilege);
        }         
        appRole.setPrivileges(rolePrivileges);
        
        return appRole;
    }
    
    @Override
    public List<RolePrivilege> getRolePrivileges(String roleId) {
        return roleDataManager.getByRole(roleId);
    }
    
    @Override
    public AppRole addRole(String roleId, String description) {        
        AppRole appRole = null; 
        
        if (roleId != null && description != null) {
            Role role = new Role();
            
            role.setRoleId(roleId);
            role.setDescription(description);
                        
            roleDataManager.create(role);
            appRole = getAppRole(role);
            return appRole;
        } 
        return appRole;
    }
   
    @Override
    public Boolean deleteRole(String roleId) {
        if (roleId != null){
            Role role = roleDataManager.get(roleId);
                       
            roleDataManager.delete(role);
            return true;
        }
        return false;    
    }
    
    @Override
    public AppRole updateRole(String roleId, String description) {        
        AppRole appRole = null;         
        
        if (roleId != null && description != null) {
            Role role = roleDataManager.get(roleId);
            
            role.setDescription(description);
            
            roleDataManager.update(role);
            appRole = getAppRole(role);
            return appRole;
        } 
        return appRole;
    }
    
    @Override
    public Boolean addRolePrivilege(String roleId, String privilegeId){
        if(roleId != null && privilegeId != null){   
            Role role = new Role();
            role.setRoleId(roleId);
            
            Privilege privilege = new Privilege();
            privilege.setPrivilegeId(privilegeId);
            
            RolePrivilege rolePrivilege = new RolePrivilege();
            rolePrivilege.setRole(role);
            rolePrivilege.setPrivilege(privilege);
            
            RolePrivilegePK rolePrivilegePK = new RolePrivilegePK();
            rolePrivilegePK.setPrivilegeId(privilegeId);
            rolePrivilegePK.setRoleId(roleId);
            rolePrivilege.setRoleAndPrivilegePK(rolePrivilegePK);
                                 
            roleDataManager.addRolePrivilege(rolePrivilege);
            return true;            
        }
        return false;   
    }
    
    @Override
    public Boolean deleteRolePrivilege(String roleId, String privilegeId){
        if(roleId != null && privilegeId != null){
            List<RolePrivilege> fetchedRolePrivileges = roleDataManager.getRoleAndPrivilege(roleId, privilegeId);            
            for (RolePrivilege rolePrivilege : fetchedRolePrivileges){
                roleDataManager.deleteRolePrivilege(rolePrivilege); 
            }                      
            return true;     
        }
        return false;
    }
}
