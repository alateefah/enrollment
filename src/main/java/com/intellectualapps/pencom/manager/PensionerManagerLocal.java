/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.manager;

import com.intellectualapps.pencom.model.Privilege;
import com.intellectualapps.pencom.model.User;
import com.intellectualapps.pencom.model.UserRole;
import com.intellectualapps.pencom.pojo.AppPensionerBasic;
import com.intellectualapps.pencom.pojo.AppUser;
import java.text.ParseException;
import java.util.List;

/**
 *
 * @author buls
 */
public interface PensionerManagerLocal {
    
    AppPensionerBasic savePensioner(String pensionerId, String firsrtName, String middleName, 
            String surname, String genderId, String serviceOrgId, String motherMaidenName, 
            String honorificId, String pfaId, String rsaNumber, String maritalStatusId, 
            String dateOfBirth, String email, String contactAddress, String contactPhoneNumber, 
            String academic, String employerId, String employerAddress, String stateOfService, 
            String staffId, String expectedRetirementDate, String firstAppointmentDate, 
            String retirementModeId, String salaryStructure2004, String gradeLevel2004, 
            String step2004, String salaryStructureConsolidated, String gradeLevelConsolidated, 
            String stepConsolidated, String salaryStructureEnhancedConsolidated, 
            String gradeLevelEnhancedConsolidated, String stepEnhancedConsolidated,
            String typeId, String subtypeId, String biometricData, String enroler) throws ParseException;
    
    AppPensionerBasic getAppPensionerBasic (String pensionerId);  
    
    List<AppPensionerBasic> searchPensioner (String searchInput);
        
}
