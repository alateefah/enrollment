/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.manager;

import com.intellectualapps.pencom.data.manager.PrivilegeDataManagerLocal;
import com.intellectualapps.pencom.data.manager.UserDataManagerLocal;
import com.intellectualapps.pencom.model.Privilege;
import com.intellectualapps.pencom.model.User;
import com.intellectualapps.pencom.pojo.AppPrivilege;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import com.intellectualapps.pencom.util.MD5;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author buls
 */
@Stateless
public class PrivilegeManager implements PrivilegeManagerLocal{
        
    @EJB
    private PrivilegeDataManagerLocal privilegeDataManager;
    
    @Override
    public Privilege getPrivilege(String privilegeId) {
        return privilegeDataManager.get(privilegeId);
    }
    
    
    @Override
    public List<AppPrivilege> getAllPrivileges() {
        List<AppPrivilege> appPrivileges = new ArrayList<>();
        List<Privilege> privileges = privilegeDataManager.getAllPrivileges();
        for (Privilege privilege : privileges) {
            appPrivileges.add(getAppPrivilege(privilege));
        }
        return appPrivileges;
    }
    
   
    @Override
    public AppPrivilege getAppPrivilege(Privilege privilege) {
        AppPrivilege appPrivilege = new AppPrivilege();
        
        appPrivilege.setPrivilegeId(privilege.getPrivilegeId());
        appPrivilege.setDescription(privilege.getDescription());
        appPrivilege.setMenuLink(privilege.getMenuLink());
        appPrivilege.setMenuIcon(privilege.getMenuIcon());
                
        return appPrivilege;
    }
    
    @Override
    public AppPrivilege addPrivilege(String privilegeId, String description, String menuLink) {        
        AppPrivilege appPrivilege = null; 
        
        if (privilegeId != null && description != null && menuLink != null) {
            Privilege privilege = new Privilege();
            
            privilege.setPrivilegeId(privilegeId);
            privilege.setDescription(description);
            privilege.setMenuLink(menuLink);
                        
            privilegeDataManager.create(privilege);
            appPrivilege = getAppPrivilege(privilege);
            return appPrivilege;
        } 
        return appPrivilege;
    }
   
    @Override
    public Boolean deletePrivilege(String privilegeId) {
        if (privilegeId != null){
            Privilege privilege = privilegeDataManager.get(privilegeId);
                       
            privilegeDataManager.delete(privilege);
            return true;
        }
        return false;    
    }
    
    @Override
    public AppPrivilege updatePrivilege(String privilegeId, String description, String menuLink, String menuIcon) {        
        AppPrivilege appPrivilege = null;         
        
        if (privilegeId != null && description != null && menuLink != null) {
            Privilege privilege = privilegeDataManager.get(privilegeId);
            
            privilege.setDescription(description);
            privilege.setMenuLink(menuLink);
            if (menuIcon != null) {
                privilege.setMenuIcon(menuIcon);
            }
            
            privilegeDataManager.update(privilege);
            appPrivilege = getAppPrivilege(privilege);
            return appPrivilege;
        } 
        return appPrivilege;
    }
}
