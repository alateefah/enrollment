/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.manager;

import com.intellectualapps.pencom.model.Privilege;
import com.intellectualapps.pencom.model.User;
import com.intellectualapps.pencom.model.UserRole;
import com.intellectualapps.pencom.pojo.AppUser;
import java.text.ParseException;
import java.util.List;

/**
 *
 * @author buls
 */
public interface UserManagerLocal {
    
    AppUser authenticateUser(String username, String password);
    
    AppUser getAppUser (String userId);
    
    AppUser getAppUser (User user);
    
    List<UserRole> getUserRoles (String userId);

    List<AppUser> getAllUsers();
    
    AppUser addUser(String username, String firstname, String othernames);
    
    Boolean deleteUser(String username);
    
    AppUser updateUser(String username, String firstname, String othernames);
    
    Boolean addUserRole(String username, String roleId);
    
    Boolean deleteUserRole(String username, String roleId);
}
