/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.manager;

import com.intellectualapps.pencom.data.manager.PensionerDataManagerLocal;
import com.intellectualapps.pencom.data.manager.PensionerEmploymentDataManagerLocal;
import com.intellectualapps.pencom.data.manager.PrivilegeDataManagerLocal;
import com.intellectualapps.pencom.data.manager.RoleDataManagerLocal;
import com.intellectualapps.pencom.data.manager.UserDataManagerLocal;
import com.intellectualapps.pencom.data.manager.UserRoleDataManagerLocal;
import com.intellectualapps.pencom.model.PensionerBasic;
import com.intellectualapps.pencom.model.PensionerEmployment;
import com.intellectualapps.pencom.model.Privilege;
import com.intellectualapps.pencom.model.RolePrivilege;
import com.intellectualapps.pencom.model.User;
import com.intellectualapps.pencom.model.UserRole;
import com.intellectualapps.pencom.pojo.AppPensionerBasic;
import com.intellectualapps.pencom.pojo.AppPensionerEmployment;
import com.intellectualapps.pencom.pojo.AppPrivilege;
import com.intellectualapps.pencom.pojo.AppRole;
import com.intellectualapps.pencom.pojo.AppUser;
import com.intellectualapps.pencom.util.DateParser;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import com.intellectualapps.pencom.util.MD5;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author buls
 */
@Stateless
public class PensionerEmploymentManager implements PensionerEmploymentManagerLocal{
        
    @EJB
    private PensionerEmploymentDataManagerLocal pensionerEmploymentDataManager;    
            
    @Override
    public AppPensionerEmployment savePensionerEmployment (Integer employmentId, String employerId, String employerAddress, String stateOfService, 
            String staffId, String expectedRetirementDate, String firstAppointmentDate, 
            String retirementModeId, String salaryStructure2004, String gradeLevel2004, 
            String step2004, String salaryStructureConsolidated, String gradeLevelConsolidated, 
            String stepConsolidated, String salaryStructureEnhancedConsolidated, 
            String gradeLevelEnhancedConsolidated, String stepEnhancedConsolidated) throws ParseException{
        
        AppPensionerEmployment appPensionerEmployment = null;

        Date formattedExpectedDateOfRetriement = null;
        Date formattedFirstAppointmentDate = null;
        if (employerAddress != null && !employerAddress.equals("") && staffId != null &&
                !staffId.equals("") && expectedRetirementDate != null && !expectedRetirementDate.equals("") &&
                firstAppointmentDate != null && !firstAppointmentDate.equals("")) {

            formattedExpectedDateOfRetriement = new DateParser().parseDate(expectedRetirementDate);
            formattedFirstAppointmentDate = new DateParser().parseDate(firstAppointmentDate);


            PensionerEmployment pensionerEmployment = null; 
            if (employmentId != null) {
                pensionerEmployment = pensionerEmploymentDataManager.get(employmentId);
            } else {
                pensionerEmployment = new PensionerEmployment(); 
            }
            pensionerEmployment.setEmployerId(Integer.parseInt(employerId));
            pensionerEmployment.setEmployerFullAddress(employerAddress);
            pensionerEmployment.setStateOfService(stateOfService);
            pensionerEmployment.setStaffId(staffId);
            pensionerEmployment.setExpectedRetirementDate(formattedExpectedDateOfRetriement);
            pensionerEmployment.setFirstAppointmentDate(formattedFirstAppointmentDate);
            pensionerEmployment.setRetirementModeId(Integer.parseInt(retirementModeId));
            pensionerEmployment.setSalaryStructure2004(Integer.parseInt(salaryStructure2004));
            pensionerEmployment.setGradeLevel2004(Integer.parseInt(gradeLevel2004));
            pensionerEmployment.setStep2004(Integer.parseInt(step2004));
            pensionerEmployment.setSalaryStructureConsolidated(Integer.parseInt(salaryStructureConsolidated));
            pensionerEmployment.setGradeLevelConsolidated(Integer.parseInt(gradeLevelConsolidated));
            pensionerEmployment.setStepConsolidated(Integer.parseInt(stepConsolidated));
            pensionerEmployment.setEnhancedSalaryStructureConsolidated(Integer.parseInt(salaryStructureEnhancedConsolidated));
            pensionerEmployment.setEnhancedGradeLevelConsolidated(Integer.parseInt(gradeLevelEnhancedConsolidated));
            pensionerEmployment.setEnhancedStepConsolidated(Integer.parseInt(stepEnhancedConsolidated));

            if (employmentId != null) {
                appPensionerEmployment = getAppPensionerEmployment(pensionerEmploymentDataManager.update(pensionerEmployment));                    
            } else {
                appPensionerEmployment = getAppPensionerEmployment(pensionerEmploymentDataManager.create(pensionerEmployment));                    
            }
        }

        return appPensionerEmployment;      
        
    }

    @Override
    public AppPensionerEmployment getAppPensionerEmployment(Integer pensionerEmploymentId) {
        return getAppPensionerEmployment(pensionerEmploymentDataManager.get(pensionerEmploymentId));
    }    
    
    public AppPensionerEmployment getAppPensionerEmployment(PensionerEmployment pensionerEmployment) {
        AppPensionerEmployment appPensionerEmployment = null;
        if (pensionerEmployment != null) {
            appPensionerEmployment = new AppPensionerEmployment();

            appPensionerEmployment.setEmploymentId(pensionerEmployment.getEmploymentId());
            appPensionerEmployment.setEmployerId(pensionerEmployment.getEmployerId());
            appPensionerEmployment.setEmployerFullAddress(pensionerEmployment.getEmployerFullAddress());
            appPensionerEmployment.setStateOfService(pensionerEmployment.getStateOfService());
            appPensionerEmployment.setStaffId(pensionerEmployment.getStaffId());
            appPensionerEmployment.setExpectedRetirementDate(pensionerEmployment.getExpectedRetirementDate());
            appPensionerEmployment.setFirstAppointmentDate(pensionerEmployment.getFirstAppointmentDate());
            appPensionerEmployment.setRetirementModeId(pensionerEmployment.getRetirementModeId());
            appPensionerEmployment.setSalaryStructure2004(pensionerEmployment.getSalaryStructure2004());
            appPensionerEmployment.setGradeLevel2004(pensionerEmployment.getGradeLevel2004());
            appPensionerEmployment.setStep2004(pensionerEmployment.getStep2004());
            appPensionerEmployment.setSalaryStructureConsolidated(pensionerEmployment.getSalaryStructureConsolidated());
            appPensionerEmployment.setGradeLevelConsolidated(pensionerEmployment.getGradeLevelConsolidated());
            appPensionerEmployment.setStepConsolidated(pensionerEmployment.getStepConsolidated());
            appPensionerEmployment.setEnhancedSalaryStructureConsolidated(pensionerEmployment.getEnhancedSalaryStructureConsolidated());
            appPensionerEmployment.setEnhancedGradeLevelConsolidated(pensionerEmployment.getEnhancedGradeLevelConsolidated());
            appPensionerEmployment.setEnhancedStepConsolidated(pensionerEmployment.getEnhancedStepConsolidated());
            appPensionerEmployment.setRankId(pensionerEmployment.getRankId());
        }
        return appPensionerEmployment;
    }    
    
}
