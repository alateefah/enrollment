/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.manager;

import com.intellectualapps.pencom.model.SalaryStructureLookUp;
import com.intellectualapps.pencom.pojo.AppSalaryStructureLookUp;
import java.util.List;

/**
 *
 * @author Lateefah
 */
public interface SalaryStructureLookUpManagerLocal {
    List<AppSalaryStructureLookUp> getAllSalaryStructures();
    
    List<AppSalaryStructureLookUp> getAll2004SalaryStructures();
    
    List<AppSalaryStructureLookUp> getAllConsolidatedSalaryStructures();
    
    List<AppSalaryStructureLookUp> getAllEnhancedConsolidatedSalaryStructures();
    
    AppSalaryStructureLookUp getAppSalaryStructureLookUp(SalaryStructureLookUp salaryStructure);
}
