/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.manager;

import com.intellectualapps.pencom.model.GenderLookUp;
import com.intellectualapps.pencom.pojo.AppGenderLookUp;
import java.util.List;

/**
 *
 * @author Lateefah
 */
public interface GenderLookUpManagerLocal {
    List<AppGenderLookUp> getGenders();
    
    AppGenderLookUp getAppGenderLookUp(GenderLookUp gender);
    
}
