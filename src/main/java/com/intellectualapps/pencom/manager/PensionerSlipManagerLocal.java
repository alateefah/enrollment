/*
 */
package com.intellectualapps.pencom.manager;

import java.net.URL;
import javax.ejb.Local;

/**
 *
 * @author buls
 */
@Local
public interface PensionerSlipManagerLocal {
    byte[] getPensionerSlip(String pensionerId, URL logoUrl, URL watermarkUrl);    
}
