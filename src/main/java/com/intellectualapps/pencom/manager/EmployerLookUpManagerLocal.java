/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.manager;

import com.intellectualapps.pencom.model.EmployerLookUp;
import com.intellectualapps.pencom.pojo.AppEmployerLookUp;
import java.util.List;

/**
 *
 * @author Lateefah
 */
public interface EmployerLookUpManagerLocal {
    List<AppEmployerLookUp> getEmployers();
    
    AppEmployerLookUp getAppEmployerLookUp(EmployerLookUp employer);
    
    AppEmployerLookUp getAppEmployerLookUp(Integer employerId);
}
