/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.manager;

import com.intellectualapps.pencom.model.HonorificLookUp;
import com.intellectualapps.pencom.pojo.AppHonorificLookUp;
import java.util.List;

/**
 *
 * @author Lateefah
 */
public interface HonorificLookUpManagerLocal {
    List<AppHonorificLookUp> getHonorifics();
    
    AppHonorificLookUp getAppHonorificLookUp(HonorificLookUp honorific);
    
}
