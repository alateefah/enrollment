/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.intellectualapps.pencom.manager;

import java.io.OutputStream;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;

/**
 *
 * @author buls
 */
public interface ReportManager {
    
    void exportToPdf(String destFileName) throws JRException;

    void exportToStream(OutputStream outputStream) throws JRException;

    JasperPrint getJasperPrint() throws JRException;

    byte[] getPdf() throws JRException;

    byte[] getExcel() throws JRException;
    
    void printPreview() throws JRException;

}
