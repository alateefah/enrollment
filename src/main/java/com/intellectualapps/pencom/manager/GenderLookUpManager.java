/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.manager;

import com.intellectualapps.pencom.data.manager.GenderLookUpDataManagerLocal;
import com.intellectualapps.pencom.data.manager.GenderLookUpDataManagerLocal;
import com.intellectualapps.pencom.model.GenderLookUp;
import com.intellectualapps.pencom.pojo.AppGenderLookUp;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Lateefah
 */
@Stateless
public class GenderLookUpManager implements GenderLookUpManagerLocal{
    @EJB
    private GenderLookUpDataManagerLocal genderLookUpDataManager;
    
    @Override
    public List<AppGenderLookUp> getGenders() {
        List<AppGenderLookUp> appGenders = new ArrayList<AppGenderLookUp>();
        List<GenderLookUp> genders = genderLookUpDataManager.getAllGenders();
        for (GenderLookUp gender : genders) {
            appGenders.add(getAppGenderLookUp(gender));
        }
        return appGenders;
    }
           
    @Override
    public AppGenderLookUp getAppGenderLookUp(GenderLookUp gender){
        AppGenderLookUp appGender = new AppGenderLookUp();
        
        appGender.setGenderId(gender.getGenderId());
        appGender.setDescription(gender.getDescription());
        
        return appGender;
    }
    
}
