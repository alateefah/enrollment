/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.manager;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.intellectualapps.pencom.data.manager.ServiceOrganisationLookUpDataManagerLocal;
import com.intellectualapps.pencom.model.ServiceOrganisationLookUp;
import com.intellectualapps.pencom.pojo.AppServiceOrganisationLookUp;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Lateefah
 */
@Stateless
public class ServiceOrganisationLookUpManager implements ServiceOrganisationLookUpManagerLocal {
    
    @EJB
    private ServiceOrganisationLookUpDataManagerLocal serviceOrganisationLookUpDataManager;
    
    @Override
    public List<AppServiceOrganisationLookUp> getServiceOrganisations() {
        List<AppServiceOrganisationLookUp> appServiceOrganisations = new ArrayList<>();
        List<ServiceOrganisationLookUp> serviceOrganisations = serviceOrganisationLookUpDataManager.getAllServiceOrganisations();
        for (ServiceOrganisationLookUp serviceOrganisation : serviceOrganisations) {
            appServiceOrganisations.add(getAppServiceOrganisationLookUp(serviceOrganisation));
        }
        return appServiceOrganisations;
    }
           
    @Override
    public AppServiceOrganisationLookUp getAppServiceOrganisationLookUp(ServiceOrganisationLookUp serviceOrganisation){
        AppServiceOrganisationLookUp appServiceOrganisation = new AppServiceOrganisationLookUp();
        
        appServiceOrganisation.setServiceOrgId(serviceOrganisation.getServiceOrgId());
        appServiceOrganisation.setDescription(serviceOrganisation.getDescription());
                
        return appServiceOrganisation;
    }
}
