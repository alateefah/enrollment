/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.pencom.manager;

import com.intellectualapps.pencom.model.RankLookUp;
import com.intellectualapps.pencom.pojo.AppRankLookUp;
import java.util.List;

/**
 *
 * @author Lateefah
 */
public interface RankLookUpManagerLocal {
    List<AppRankLookUp> getRanks();
    
    AppRankLookUp getAppRankLookUp(RankLookUp rank);
    
}
