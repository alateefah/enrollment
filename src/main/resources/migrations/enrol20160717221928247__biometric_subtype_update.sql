DELETE FROM biometric_subtype WHERE `subtype_id`='l4' and`type_id`='fp';
DELETE FROM biometric_subtype WHERE `subtype_id`='r5' and`type_id`='fp';

INSERT INTO biometric_subtype (`subtype_id`, `descr`, `type_id`) VALUES ('ri', 'right index', 'fp');
INSERT INTO biometric_subtype (`subtype_id`, `descr`, `type_id`) VALUES ('rm', 'right middle', 'fp');
INSERT INTO biometric_subtype (`subtype_id`, `descr`, `type_id`) VALUES ('rr', 'right ring', 'fp');
INSERT INTO biometric_subtype (`subtype_id`, `descr`, `type_id`) VALUES ('rl', 'right little', 'fp');
INSERT INTO biometric_subtype (`subtype_id`, `descr`, `type_id`) VALUES ('li', 'left index', 'fp');
INSERT INTO biometric_subtype (`subtype_id`, `descr`, `type_id`) VALUES ('lm', 'left middle', 'fp');
INSERT INTO biometric_subtype (`subtype_id`, `descr`, `type_id`) VALUES ('lr', 'left ring', 'fp');
INSERT INTO biometric_subtype (`subtype_id`, `descr`, `type_id`) VALUES ('ll', 'left little', 'fp');



