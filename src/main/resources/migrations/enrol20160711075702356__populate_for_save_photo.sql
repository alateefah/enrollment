-- Script to populate tables to test save photo
-- Lateefah Abdulkareem 07/11/2016

set foreign_key_checks = 0;
alter table service_org modify column service_org_id int not null auto_increment;
set foreign_key_checks = 1;

insert into service_org (descr) value('Service org 1');

insert into pensioner (first_name, middle_name, surname, gender_id, service_org_id, mother_maiden_name, honorific_id, pfa_id, rsa_number, marital_status_id, dob, email, contact_address, contact_phone_number, academic) 
	values ('Abdulkareem','Adunfe','Adesokan','m',1,'Rahma Adesokan','mr',1,'PEN809939489','m','1950-09-09','abdulkareemadesokan@gmail.com','15 Obiokosi Circular rad, works and housing, Gwarinpa, Abuja','08073384437','N');