set foreign_key_checks = 0;
ALTER TABLE pensioner MODIFY academic char(5);
ALTER TABLE pensioner MODIFY gender_id char(5);
ALTER TABLE gender MODIFY gender_id char(5);
ALTER TABLE pensioner MODIFY marital_status_id char(5);
ALTER TABLE marital_status MODIFY marital_status_id char(5);
set foreign_key_checks = 1;
insert into employer (employer_id, employer_name) values (1, 'IT');
INSERT INTO state (`state_id`, `name`) VALUES ('lg', 'Lagos');
INSERT INTO retirement_mode (`retirement_mode_id`, `descr`) VALUES ('1', 'Normal');
INSERT INTO gl (`gl_id`, `descr`) VALUES ('1', '01');
INSERT INTO salary_structure (`salary_structure_id`, `descr`) VALUES ('1', 'UASS');


