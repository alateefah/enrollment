-- additional columns to pensioners table
-- Lateefah Abdulkareem 07/02/2016

CREATE TABLE honorifics (
	honorific_id varchar(4) NOT NULL,
	descr varchar(20) NOT NULL,
	PRIMARY KEY(honorific_id)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE service_org(
	service_org_id INT NOT NULL,
	descr VARCHAR(255) NOT NULL,
	PRIMARY KEY(service_org_id)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE pfa(
	pfa_id INT AUTO_INCREMENT NOT NULL,
	name VARCHAR(255) NOT NULL,
	PRIMARY KEY(pfa_id)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE marital_status(
	marital_status_id CHAR(2) NOT NULL,
	descr VARCHAR(50) NOT NULL,
	PRIMARY KEY(marital_status_id)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 0; 
DROP TABLE IF EXISTS pensioners;
SET FOREIGN_KEY_CHECKS = 1; 

CREATE TABLE pensioners(
	pensioner_id INT AUTO_INCREMENT NOT NULL,
	first_name VARCHAR(50) NOT NULL,
	middle_name VARCHAR(50),
	other_names VARCHAR(150) NOT NULL,
	gender CHAR(1) NOT NULL,
	service_org_id INT NOT NULL,
	mother_maiden_name VARCHAR(150) NOT NULL,
	honorific_id VARCHAR(4) NOT NULL,
	pfa_id INT NOT NULL,
	rsa_number VARCHAR(20) NOT NULL,
	marital_status_id CHAR(2) NOT NULL,
	dob DATE NOT NULL,
	email VARCHAR(100),
	contact_address VARCHAR(1000) NOT NULL,
	contact_phone_number VARCHAR(20) NOT NULL,
	academic CHAR(1) NOT NULL,
	PRIMARY KEY (pensioner_id),
	CONSTRAINT fk_pensioners_gender FOREIGN KEY (gender) REFERENCES gender(sex),
	CONSTRAINT fk_pensioners_serviceorg FOREIGN KEY (service_org_id) REFERENCES service_org(service_org_id),
	CONSTRAINT fk_pensioners_honorific FOREIGN KEY (honorific_id) REFERENCES honorifics(honorific_id),
	CONSTRAINT fk_pensioners_rsa FOREIGN KEY (pfa_id) REFERENCES pfa(pfa_id),
	CONSTRAINT fk_pensioners_maritalstatus FOREIGN KEY (marital_status_id) REFERENCES marital_status(marital_status_id)
)ENGINE=INNODB DEFAULT CHARSET=utf8;
