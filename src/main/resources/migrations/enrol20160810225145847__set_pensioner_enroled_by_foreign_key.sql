ALTER TABLE `pensioner` 
ADD INDEX `fk_pensioner_enroled_by_idx` (`enroled_by` ASC)  COMMENT '';
ALTER TABLE `pensioner` 
ADD CONSTRAINT `fk_pensioner_enroled_by`
  FOREIGN KEY (`enroled_by`)
  REFERENCES `user` (`user_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

