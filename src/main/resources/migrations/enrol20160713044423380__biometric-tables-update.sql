-- ==============================================================
-- update biometric,biometric type and biometric subtype tables
-- Lateefah Abdulkareem(queenlattie720@gmail.com)
-- 13/07/2016
-- ================================================================

DROP TABLE biometric;
DROP TABLE biometric_subtype;
DROP TABLE biometric_type;

CREATE TABLE `biometric_type` (
 `type_id` char(5) NOT NULL,
 `descr` varchar(28) DEFAULT NULL,
 PRIMARY KEY (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `biometric_subtype` (
 `subtype_id` char(5) NOT NULL,
 `descr` varchar(64) NOT NULL,
 `type_id` char(5) NOT NULL,
 PRIMARY KEY (`subtype_id`,`type_id`),
 KEY `fk_biometicsubtype_typeid` (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `biometric` (
 `pensioner_id` int(11) NOT NULL,
 `type_id` char(5) NOT NULL,
 `subtype_id` char(5) NOT NULL,
 `biometric_data` mediumblob NOT NULL,
 PRIMARY KEY (`pensioner_id`,`type_id`,`subtype_id`),
 KEY `fk_biometric_biometricsubtypeid` (`type_id`,`subtype_id`),
 CONSTRAINT `fk_biometric_biometricsubtypeid` FOREIGN KEY (`type_id`, `subtype_id`) REFERENCES `biometric_subtype` (`type_id`, `subtype_id`),
 CONSTRAINT `fk_biometric_pensionerid` FOREIGN KEY (`pensioner_id`) REFERENCES `pensioner` (`pensioner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT  INTO biometric_type(type_id,descr) VALUES ('fp','Fingerprint'),('ph','Photograph'),('si','Signature');

INSERT  INTO biometric_subtype(type_id,subtype_id,descr) VALUES ('fp','l4','left four fingers'),('fp','r5','right four finger'),('fp','lt','left thumb'),('fp','rt','right thumb'),('ph','ps','passport'),('si','si','signature');