set foreign_key_checks = 0;

ALTER TABLE `biometric` 
DROP FOREIGN KEY `fk_biometric_pensionerid`;
ALTER TABLE `biometric` 
CHANGE COLUMN `pensioner_id` `pensioner_id` VARCHAR(50) NOT NULL COMMENT '' ;


ALTER TABLE `pensioner` 
CHANGE COLUMN `pensioner_id` `pensioner_id` VARCHAR(50) NOT NULL COMMENT '' ;

ALTER TABLE `biometric` 
ADD CONSTRAINT `fk_biometric_pensionerid`
  FOREIGN KEY (`pensioner_id`)
  REFERENCES `pensioner` (`pensioner_id`);


set foreign_key_checks = 1;
