ALTER TABLE `pensioner` 
ADD COLUMN `enroled_by` VARCHAR(45) NOT NULL COMMENT '' AFTER `employment_id`;
ALTER TABLE `biometric` 
ADD COLUMN `enroled_by` VARCHAR(45) NOT NULL COMMENT '' AFTER `biometric_data`;
ALTER TABLE `pensioner` 
ADD COLUMN `date_enroled` DATE NOT NULL COMMENT '' AFTER `enroled_by`;
ALTER TABLE `biometric` 
ADD COLUMN `date_enroled` DATE NOT NULL COMMENT '' AFTER `enroled_by`;


