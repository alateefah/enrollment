ALTER TABLE employment 
DROP FOREIGN KEY `fk_employment_pensionerid`;

ALTER TABLE employment 
DROP COLUMN `pensioner_id`;


ALTER TABLE pensioner 
ADD COLUMN `employment_id` INT(11) NOT NULL COMMENT '' AFTER `academic`;

set foreign_key_checks = 0;
ALTER TABLE pensioner 
ADD CONSTRAINT `fk_pensioner_employmentid`
  FOREIGN KEY (`employment_id`)
  REFERENCES employment (`employment_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
set foreign_key_checks = 1;

