-- ================================================
-- initial database structure for pencom enrol app
-- Lateefah Abudulkareem(queenlattie720@gmail.com)
-- 01/07/2016
-- ================================================

CREATE TABLE roles(
	role_name VARCHAR(6) NOT NULL,
	descr VARCHAR(20) NOT NULL,
	PRIMARY KEY (role_name)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE users(
	username VARCHAR(64) NOT NULL,
	firstname VARCHAR(50) NOT NULL,
	othernames VARCHAR(50) NOT NULL,
	password VARCHAR(255) NOT NULL,
	PRIMARY KEY (username)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE user_logins (
	connection_id INT AUTO_INCREMENT NOT NULL,
	username VARCHAR(64) NOT NULL,
	remote_host VARCHAR(255) NOT NULL,
	login_time TIMESTAMP NOT NULL,
	logout_time DATETIME DEFAULT NULL,
	PRIMARY KEY(connection_id),
	CONSTRAINT fk_userlogins_username FOREIGN KEY (username) REFERENCES users (username) ON DELETE NO ACTION
)ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE user_roles (
	username VARCHAR(64) NOT NULL,
	role_name VARCHAR(6) NOT NULL,
	PRIMARY KEY (username, role_name),
	CONSTRAINT fk_userroles_username FOREIGN KEY (username) REFERENCES users(username) ON DELETE NO ACTION,	
	CONSTRAINT fk_userroles_rolename FOREIGN KEY (role_name) REFERENCES roles(role_name) ON DELETE NO ACTION
)ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE priveleges (
	privelege_id VARCHAR(32) NOT NULL,
	description VARCHAR(255) NOT NULL,
	menu_link VARCHAR(255) NOT NULL,
	PRIMARY KEY (privelege_id)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE roles_priveleges (
	privelege_id VARCHAR(32) NOT NULL,
	role_name VARCHAR(6) NOT NULL,
	PRIMARY KEY(privelege_id,role_name),
	CONSTRAINT fk_rolespriveleges_privelege_id FOREIGN KEY(privelege_id) REFERENCES priveleges(privelege_id) ON DELETE CASCADE,
	CONSTRAINT fk_rolespriveleges_rolename FOREIGN KEY(role_name) REFERENCES roles(role_name) ON DELETE CASCADE
)ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE gender(
    sex CHAR(1) NOT NULL,
    descr VARCHAR(7) NOT NULL,
    PRIMARY KEY (sex)
)ENGINE=INNODB DEFAULT CHARSET=utf8;


CREATE TABLE states(
    state_code CHAR(2) NOT NULL,
    name VARCHAR(64) NOT NULL,
    PRIMARY KEY (state_code)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE lgas(
	lga_code INT NOT NULL,
    name VARCHAR(64) NOT NULL,
    state_code CHAR(2) NOT NULL,
    PRIMARY KEY (lga_code),
    CONSTRAINT fk_lgas_statecode FOREIGN KEY (state_code) REFERENCES states(state_code)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE relationships (
	relationship_id VARCHAR(3) NOT NULL,
	descr VARCHAR(32) NOT NULL,
	sex CHAR(1) NOT NULL,
	PRIMARY KEY (relationship_id),
	CONSTRAINT fk_relationships_sex FOREIGN KEY (sex) REFERENCES gender (sex)
) ENGINE=INNODB DEFAULT CHARSET=utf8;


CREATE TABLE pensioners(
	pensioner_id INT AUTO_INCREMENT NOT NULL,
	firstname VARCHAR(50) NOT NULL,
	othernames VARCHAR(150) NOT NULL,
	gender CHAR(1) NOT NULL,
	phone_number1 VARCHAR(20) NOT NULL,
	phone_number2 VARCHAR(20) NOT NULL,
	home_address VARCHAR(1000) NOT NULL,
	home_state CHAR(2) NOT NULL,
	home_lga INT NOT NULL,
	home_city VARCHAR(60) NOT NULL,
	origin_address VARCHAR(1000) NOT NULL,
	origin_state CHAR(2) NOT NULL,
	origin_lga INT NOT NULL,
	origin_city VARCHAR(60) NOT NULL,
	PRIMARY KEY (pensioner_id),
	CONSTRAINT fk_pensioners_gender FOREIGN KEY (gender) REFERENCES gender(sex),
	CONSTRAINT fk_pensioners_homestate FOREIGN KEY (home_state) REFERENCES states(state_code),
	CONSTRAINT fk_pensioners_lga FOREIGN KEY (home_lga) REFERENCES lgas(lga_code),
	CONSTRAINT fk_pensioners_originstate FOREIGN KEY (origin_state) REFERENCES states(state_code),
	CONSTRAINT fk_pensioners_originlga FOREIGN KEY (origin_lga) REFERENCES lgas(lga_code)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE nok (
    nok_id INT AUTO_INCREMENT NOT NULL,
    fullname VARCHAR(200) NOT NULL, 
    gender CHAR(1) NOT NULL, 
    phone_number1 VARCHAR(20) NOT NULL, 
    phone_number2 VARCHAR(20) NOT NULL,
    address VARCHAR(1000) NOT NULL,
    state_code CHAR(2) NOT NULL,
    lga_code INT NOT NULL,
    relationship VARCHAR(3) NOT NULL,    
    PRIMARY KEY (nok_id),
    CONSTRAINT fk_nok_gender FOREIGN KEY (gender) REFERENCES gender(sex),
    CONSTRAINT fk_nok_statecode FOREIGN KEY (state_code) REFERENCES states(state_code),
	CONSTRAINT fk_nok_lgacode FOREIGN KEY (lga_code) REFERENCES lgas(lga_code),
    CONSTRAINT fk_nok_relationship FOREIGN KEY (relationship) REFERENCES relationships(relationship_id)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE employments(
	employment_id INT AUTO_INCREMENT,
	pensioner_id INT NOT NULL, 
	company VARCHAR(200) NOT NULL,
	address VARCHAR (1000) NOT NULL,
	state_code CHAR(2) NOT NULL,
	city VARCHAR(20) NOT NULL,
	supervisor VARCHAR(200) NOT NULL,
	period_from DATE NOT NULL,
	period_to DATE NOT NULL,
	PRIMARY KEY (employment_id),
	CONSTRAINT fk_employments_pensionerid FOREIGN KEY (pensioner_id) REFERENCES pensioners(pensioner_id),
	CONSTRAINT fk_employments_state FOREIGN KEY (state_code) REFERENCES states(state_code)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE biometric_types (
	type_id CHAR(2) NOT NULL,
	descr VARCHAR(28) DEFAULT NULL,
	PRIMARY KEY (type_id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE biometric_subtypes (
	subtype_id CHAR(2) NOT NULL,
	descr VARCHAR(64) NOT NULL,
	type_id CHAR(2) NOT NULL,	
	PRIMARY KEY (subtype_id,type_id),
	CONSTRAINT fk_biometicsubtypes_typeid FOREIGN KEY (type_id) REFERENCES biometric_types(type_id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE biometrics (
	pensioner_id INT NOT NULL,
	type_id CHAR(2) NOT NULL,
	subtype_id CHAR(2) NOT NULL,
	biometric_data MEDIUMBLOB NOT NULL,
	PRIMARY KEY (pensioner_id, type_id, subtype_id),
	CONSTRAINT `FK_biometrics_biometricsubtypes` FOREIGN KEY (type_id, subtype_id) REFERENCES `biometric_subtypes` (type_id, subtype_id),
	CONSTRAINT `FK_biometrics_pensioners` FOREIGN KEY (pensioner_id) REFERENCES pensioners (pensioner_id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
