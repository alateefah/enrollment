/**
 * Author:  sadiq
 * Created: 07-Aug-2016
 * Purpose: Alter privelege table to add menu_icon column, update the table with icon names for the priveleges
 */

ALTER TABLE privelege
ADD COLUMN `menu_icon` VARCHAR(255) NOT NULL DEFAULT 'link' AFTER `menu_link`;

UPDATE privelege SET `menu_icon`='add' WHERE `privelege_id`='ENROL';
UPDATE privelege SET `menu_icon`='settings' WHERE `privelege_id`='MANAGE';
UPDATE privelege SET `menu_icon`='search' WHERE `privelege_id`='SEARCH';
UPDATE privelege SET `menu_icon`='build' WHERE `privelege_id`='GENERATE';
