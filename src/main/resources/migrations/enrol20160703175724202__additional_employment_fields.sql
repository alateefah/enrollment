-- additional fields for employment page
-- Lateefah Abdulkareem(queenlattie720@gmail.com)
-- 03/07/2016
-- special_service tables records extra information for Permanent Secretaries, Professors, Generals e.t.c

CREATE TABLE employer (
	employer_id INT AUTO_INCREMENT NOT NULL,
	employer_name VARCHAR(255) NOT NULL,
	PRIMARY KEY(employer_id)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE retirement_mode (
	retirement_mode_id INT AUTO_INCREMENT NOT NULL,
	descr VARCHAR(100) NOT NULL,
	PRIMARY KEY (retirement_mode_id)
)ENGINE=INNODB DEFAULT CHARSET=utf8;
	
CREATE TABLE salary_structure(
	salary_structure_id  INT AUTO_INCREMENT NOT NULL,
	descr VARCHAR(100) NOT NULL,
	PRIMARY KEY (salary_structure_id)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE gl(
	gl_id INT AUTO_INCREMENT NOT NULL,
	descr VARCHAR(100) NOT NULL,
	PRIMARY KEY (gl_id)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE rank(
	rank_id INT AUTO_INCREMENT NOT NULL,
	descr VARCHAR(100) NOT NULL,
	PRIMARY KEY (rank_id)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 0; 
DROP TABLE IF EXISTS employments;
SET FOREIGN_KEY_CHECKS = 1; 

CREATE TABLE employment(
	employment_id INT AUTO_INCREMENT NOT NULL,
	employer_id INT NOT NULL,
	pensioner_id INT NOT NULL, 
	employer_full_address VARCHAR(2000) NOT NULL,
	state_of_service CHAR(2) NOT NULL,
	staff_id VARCHAR(50) NOT NULL,
	expected_retirement_date DATE NOT NULL,
	first_appointment_date DATE NOT NULL,
	retirement_mode_id INT NOT NULL,
	2004_sal_struct INT,
	2004_gl INT,
	2004_step INT,
	consol_sal_struct INT,
	consol_gl INT,
	consol_step INT,
	enh_consol_sal_struct INT,
	enh_consol_gl INT,
	enh_consol_step INT,
	rank_id INT,
	PRIMARY KEY (employment_id),
	CONSTRAINT fk_employment_employerid FOREIGN KEY (employer_id) REFERENCES employer(employer_id),
	CONSTRAINT fk_employment_pensionerid FOREIGN KEY (pensioner_id) REFERENCES pensioner(pensioner_id),
	CONSTRAINT fk_employment_stateofservice FOREIGN KEY (state_of_service) REFERENCES state(state_id),
	CONSTRAINT fk_employment_retirementmode FOREIGN KEY (retirement_mode_id) REFERENCES retirement_mode(retirement_mode_id),
	CONSTRAINT fk_employment_2004salstruct FOREIGN KEY (2004_sal_struct) REFERENCES salary_structure(salary_structure_id),
	CONSTRAINT fk_employment_2004gl FOREIGN KEY (2004_gl) REFERENCES gl(gl_id),
	CONSTRAINT fk_employment_consolsalstruct FOREIGN KEY (consol_sal_struct) REFERENCES salary_structure(salary_structure_id),
	CONSTRAINT fk_employment_consolgl FOREIGN KEY (consol_gl) REFERENCES gl(gl_id),
	CONSTRAINT fk_employment_ehnconsolsalstruct FOREIGN KEY (enh_consol_sal_struct) REFERENCES salary_structure(salary_structure_id),
	CONSTRAINT fk_employment_enhconsolgl FOREIGN KEY (enh_consol_gl) REFERENCES gl(gl_id),
	CONSTRAINT fk_employment_rankid FOREIGN KEY (rank_id) REFERENCES rank(rank_id)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

-- for permanent secretary,professors, general etc
CREATE TABLE special_service(
	special_service_id INT AUTO_INCREMENT NOT NULL,
	employment_id INT NOT NULL,
	date_of_appointment DATE NOT NULL,
	june_2004_rank INT NOT NULL,
	gl INT NOT NULL,
	step INT,
	PRIMARY KEY (special_service_id),
	CONSTRAINT fk_specialservice_employmentid FOREIGN KEY (employment_id) REFERENCES employment(employer_id),
	CONSTRAINT fk_specialservice_june2004rank FOREIGN KEY (june_2004_rank) REFERENCES rank(rank_id),
	CONSTRAINT fk_specialservice_gl FOREIGN KEY (gl) REFERENCES gl(gl_id)
)ENGINE=INNODB DEFAULT CHARSET=utf8;