-- insert script for honrifics(salutations), marital status and Pension fund administators
-- Lateefah Abdulkareem 07/02/2016


-- populate honorifics table
insert into honorifics(honorific_id, descr)values('miss','Miss'),('mr','Mr'),('mrs','Mrs'),('prof','Professor'),('dr','Dr');

-- populate marital status
insert into marital_status(marital_status_id, descr)values('m','Married'),('d','Divorced'),('s','Single'),('sp','Seperated');

-- populate PFA table with acredited PFAs
insert into pfa(name)values('AIICO Pension Managers Limited'),('APT Pension Fund Managers Limited'),('ARM Pension Managers Limited'),('AXA Masard Pension Limited'),('CrusaderSterling Pensions Limited'),('Fidelity Pension Managers'),('First Guarantee Pension Limited'),('Future Unity Glanvils Pensions Limited'),('Investment One Pension Managers Limited'),('IEI-Anchor Pension Managers Limited'),('IGI Pension Fund Managers Limited'),('Leadway Pensure PFA Limited'),('Legacy Pension Managers Limited'),('NLPC Pension Fund Administrators Limited'),('NPF Pensions Limited'),('OAK Pensions Limited'),('Pensions Alliance Limited'),('Premium Pension Limited'),('Sigma Pensions Limited'),('Stanbic IBTC Pension Managers Limited'),('Trustfund Pensions Plc');