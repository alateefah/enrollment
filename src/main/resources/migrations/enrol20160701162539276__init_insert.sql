-- ================================================
-- initial insert script for pencom enrol app
-- Lateefah Abudulkareem(queenlattie720@gmail.com)
-- 01/07/2016
-- ================================================
INSERT INTO roles (role_name, descr) VALUES ('wheel','Wheel User'),('admin','Administrators');

INSERT INTO users (username, firstname, othernames, password) VALUES ('alateefah','password','Abdulkareem',PASSWORD('password'));

INSERT INTO gender (sex, descr) VALUES ('f','Female'),('m','Male'),('u','Unknown');

INSERT  INTO relationships(`sex`,`relationship_id`,`descr`) VALUES ('f','aun','aunt'),('u','ba','bus assistant'),('m','bil','brother-in-law'),('m','bro','brother'),('u','bus','business partner'),('u','cou','cousin'),('u','emp','employer'),('m','fil','father-in-law'),('u','fri','friend'),('f','gma','grand mother'),('m','gpa','grand father'),('u','gua','guardian/benefactor'),('m','hus','husband'),('f','ma','mother'),('f','mil','mother-in-law'),('m','pa','father'),('f','sim','sister-in-law'),('f','sis','sister'),('u','t','teacher'),('m','unc','uncle'),('f','wif','wife'),('u','zzz','others');

INSERT  INTO biometric_types(type_id,descr) VALUES ('fp','Fingerprint'),('ph','Photograph'),('si','Signature');

INSERT  INTO biometric_subtypes(type_id,subtype_id,descr) VALUES ('fp','l4','left four fingers'),('fp','r5','right four finger'),('fp','lt','left thumb'),('fp','rt','right thumb'),('ph','ps','passport'),('si','si','signature');