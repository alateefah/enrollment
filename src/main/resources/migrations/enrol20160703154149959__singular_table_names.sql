-- ================================================
-- modify database tables to singular name
-- Lateefah Abdulkareem(queenlattie720@gmail.com)
-- 03/07/2016
-- ================================================

-- role table modification
RENAME TABLE roles TO role;
ALTER TABLE role CHANGE role_name role_id VARCHAR(6) NOT NULL;

-- user table modification
RENAME TABLE users TO user;
ALTER TABLE user CHANGE username user_id VARCHAR(64) NOT NULL;

-- user_login table modification
RENAME TABLE user_logins to user_login;

ALTER TABLE user_login DROP FOREIGN KEY fk_userlogins_username;
ALTER TABLE user_login CHANGE username user_id VARCHAR(64) NOT NULL;
ALTER TABLE user_login ADD CONSTRAINT fk_userlogin_userid FOREIGN KEY (user_id) REFERENCES user (user_id) ON DELETE NO ACTION;

-- user_role table modification
RENAME TABLE user_roles TO user_role;

ALTER TABLE user_role CHANGE username user_id VARCHAR(64) NOT NULL;

ALTER TABLE user_role DROP FOREIGN KEY fk_userroles_username;
ALTER TABLE user_role ADD CONSTRAINT fk_userrole_userid FOREIGN KEY (user_id) REFERENCES user (user_id) ON DELETE NO ACTION;

ALTER TABLE user_role DROP FOREIGN KEY fk_userroles_rolename;
ALTER TABLE user_role CHANGE role_name role_id VARCHAR(6) NOT NULL;
ALTER TABLE user_role ADD CONSTRAINT fk_userrole_roleid FOREIGN KEY (role_id) REFERENCES role (role_id) ON DELETE NO ACTION;

-- priveleges table modification
RENAME TABLE priveleges to privelege;

-- roles_priveleges table modification
RENAME TABLE roles_priveleges TO role_privelege;

ALTER TABLE role_privelege DROP FOREIGN KEY fk_rolespriveleges_privelege_id;
ALTER TABLE role_privelege ADD CONSTRAINT fk_roleprivelege_privelegeid FOREIGN KEY (privelege_id) REFERENCES privelege(privelege_id);

ALTER TABLE role_privelege DROP FOREIGN KEY fk_rolespriveleges_rolename;
ALTER TABLE role_privelege CHANGE role_name role_id VARCHAR(6) NOT NULL;
ALTER TABLE role_privelege ADD CONSTRAINT fk_roleprivelege_roleid FOREIGN KEY (role_id) REFERENCES role(role_id) ON DELETE CASCADE;

-- states table modification
RENAME TABLE states to state;

ALTER TABLE state CHANGE state_code state_id CHAR(2) NOT NULL;

-- lga table modification
RENAME TABLE lgas TO lga;

ALTER TABLE lga CHANGE lga_code lga_id INT NOT NULL;

ALTER TABLE lga CHANGE state_code state_id CHAR(2) NOT NULL;

ALTER TABLE lga DROP FOREIGN KEY fk_lgas_statecode;

ALTER TABLE lga ADD CONSTRAINT fk_lga_stateid FOREIGN KEY (state_id) REFERENCES state(state_id);

-- gender table modification
ALTER TABLE gender CHANGE sex gender_id CHAR(1);

-- relationships table modification
RENAME TABLE relationships to relationship;

ALTER TABLE relationship DROP FOREIGN KEY fk_relationships_sex;

ALTER TABLE relationship CHANGE sex gender_id CHAR(1) NOT NULL;
ALTER TABLE relationship ADD CONSTRAINT fk_relationship_genderid FOREIGN KEY (gender_id) REFERENCES gender (gender_id);

-- honorific table modification
RENAME TABLE honorifics TO honorific;

-- pensioner table modification
RENAME TABLE pensioners TO pensioner;

ALTER TABLE pensioner CHANGE other_names surname VARCHAR(50) NOT NULL;

ALTER TABLE pensioner DROP FOREIGN KEY fk_pensioners_gender;
ALTER TABLE pensioner CHANGE gender gender_id CHAR(1) NOT NULL;

ALTER TABLE pensioner ADD CONSTRAINT fk_pensioner_genderid FOREIGN KEY (gender_id) REFERENCES gender(gender_id);

ALTER TABLE pensioner DROP FOREIGN KEY fk_pensioners_serviceorg;
ALTER TABLE pensioner ADD CONSTRAINT fk_pensioner_serviceorg FOREIGN KEY (service_org_id) REFERENCES service_org(service_org_id);

ALTER TABLE pensioner DROP FOREIGN KEY fk_pensioners_honorific;
ALTER TABLE pensioner ADD CONSTRAINT fk_pensioner_honorific FOREIGN KEY (honorific_id) REFERENCES honorific(honorific_id);

ALTER TABLE pensioner DROP FOREIGN KEY fk_pensioners_rsa;
ALTER TABLE pensioner ADD CONSTRAINT fk_pensioner_pfa FOREIGN KEY (pfa_id) REFERENCES pfa(pfa_id);

ALTER TABLE pensioner DROP FOREIGN KEY fk_pensioners_maritalstatus;
ALTER TABLE pensioner ADD CONSTRAINT fk_pensioner_maritalstatus FOREIGN KEY (marital_status_id) REFERENCES marital_status(marital_status_id);

-- biometric_type table modification
RENAME TABLE biometric_types TO biometric_type;

-- biometric_subtype table modification
RENAME TABLE biometric_subtypes TO biometric_subtype;

ALTER TABLE biometric_subtype DROP FOREIGN KEY fk_biometicsubtypes_typeid;
ALTER TABLE biometric_subtype ADD CONSTRAINT fk_biometicsubtype_typeid FOREIGN KEY (type_id) REFERENCES biometric_type(type_id);

-- biometric table modification
RENAME TABLE biometrics TO biometric;

ALTER TABLE biometric DROP FOREIGN KEY FK_biometrics_biometricsubtypes;
ALTER TABLE biometric ADD CONSTRAINT `fk_biometric_biometricsubtypeid` FOREIGN KEY (type_id, subtype_id) REFERENCES `biometric_subtype` (type_id, subtype_id);
	
ALTER TABLE biometric DROP FOREIGN KEY FK_biometrics_pensioners;
ALTER TABLE biometric ADD CONSTRAINT `fk_biometric_pensionerid` FOREIGN KEY (pensioner_id) REFERENCES pensioner (pensioner_id);
