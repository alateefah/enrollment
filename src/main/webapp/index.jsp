<%-- 
    Document   : index
    Created on : Jul 3, 2016, 11:46:08 PM
    Author     : buls
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Login &middot; PENCOM Enrollment</title>
        <link href="favicon.ico" rel="shortcut icon" type="image/x-icon">
        
        <!--Import Google Icon Font-->
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="assets/css/materialize.css"  media="screen,projection"/>
        <!-- Other styles -->
        <link type="text/css" rel="stylesheet" href="assets/css/style.css" media="screen,projection"/>

        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
    <%
        String errorMsg = (String) request.getAttribute("errormsg");
        if (errorMsg == null) {
            errorMsg = "";
        } else {
    %>
            <a class="btn hide" id='errorbtn' onclick="Materialize.toast('<div id=\'error\'><%= errorMsg %></div>', 20000, 'rounded');">Toast!</a>
    <%  } %>
    
    <body class="bg" onLoad="setFocus('username'), toggleVisibility('preloader')">
       
        <main style="margin-top: 5%;">
            <div class="container">
                <div class="row">
                    <div class="col s12 m4 offset-m4">
                        <div class="section">
                            <!-- card -->
                            <div class="card">
                                <div class="card-image center centered">
                                    <img src="assets/images/grey-pattern.jpg" alt="">
                                    <div class="row">
                                        <div class="col m4">
                                            <span class="card-logo">
                                                <img style="width: 80px;" src="assets/images/logo.png" alt="PENCOM LOGO">
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="row">
                                        <div class="col s12 m12 center">
                                            <div id="preloader" class="preloader-wrapper active">
                                                <div class="spinner-layer spinner-red-only">
                                                    <div class="circle-clipper left"><div class="circle"></div></div>
                                                    <div class="gap-patch"><div class="circle"></div></div>
                                                    <div class="circle-clipper right"><div class="circle"></div></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <form id="login" method="post" action="webbackend">
                                        <input name="intention" type="hidden" value="login">
                                        <div class="row">
                                            <div class="input-field">
                                                <input id="username" name="username" type="text" class="validate">
                                                <label for="username">Username</label>
                                            </div>
                                            <div class="input-field">
                                                <input id="password" name="password" type="password" class="validate">
                                                <label for="password">Password</label>
                                            </div>
                                            <div class="row center">
                                                <button type="submit" class="btn-floating btn-large waves-effect waves-light red tooltipped" data-delay="1000" data-tooltip="Click to login" onClick="submitLoginForm()">
                                                    <i class="large material-icons">vpn_key</i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>						   
                            </div>
                            <!-- end card -->
                        </div>
                    </div>
                </div>
            </div>
        </main>
        
        <!--Import jQuery before materialize.js-->
        <script type="text/javascript" src="assets/js/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="assets/js/materialize.js"></script>
        <script type="text/javascript" src="assets/js/customActions.js"></script>
        <script>
            $(function() {
                $("#errorbtn").trigger("click");
            });
        </script>
    </body>
</html>