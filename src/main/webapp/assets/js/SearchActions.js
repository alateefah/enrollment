/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function searchPensioner() {
    document.getElementById('progess').style.display = 'inline';
    var searchResult = [];
    $.ajax({
      method: "POST",
      url: "webbackend",
      data: $('#searchForm').serialize()
    })
    .done(function( serverResponse ) {
        var response = JSON.parse(serverResponse);
        if (response.status === "200") {
            $.each(response.payLoad, function(i,pensionersFound){
                $.each(pensionersFound, function(i,pensioner){
                    searchResult.push(pensioner);
                });                
            });
            updateSearchResultTable(searchResult);
            document.getElementById('progess').style.display = 'none';
        } else {
            Materialize.toast("Search failed. Error Code: " + response.status + ". Error Message: " + response.statusMessage, 3000, 'rounded');
        }
        })
        .fail(function( serverResponse ) {
            Materialize.toast("Something went wrong: " + serverResponse, 3000, 'rounded');
            document.getElementById('progess').style.display = 'none';
    });
      
  
}

function updateSearchResultTable(searchResult) {
    var count = 1;    
    var table = document.getElementById("result-list");
    table.innerHTML = "";
    
    var tHead = document.createElement("thead");
    var headerTr = document.createElement("tr");
    var serialNoHeaderTh = document.createElement("th");
    serialNoHeaderTh.innerHTML = "S/No.";
    var pensionerIdHeaderTh = document.createElement("th");
    pensionerIdHeaderTh.innerHTML = "Pensioner Id.";
    var firstNameHeaderTh = document.createElement("th");
    firstNameHeaderTh.innerHTML = "First Name";
    var middleNameHeaderTh = document.createElement("th");
    middleNameHeaderTh.innerHTML = "Middle Name";
    var surnameHeaderTh = document.createElement("th");
    surnameHeaderTh.innerHTML = "Surname";
    var contactPhoneHeaderTh = document.createElement("th");
    contactPhoneHeaderTh.innerHTML = "Phone Number";
    
    headerTr.appendChild(serialNoHeaderTh);
    headerTr.appendChild(pensionerIdHeaderTh);
    headerTr.appendChild(firstNameHeaderTh);
    headerTr.appendChild(middleNameHeaderTh);
    headerTr.appendChild(surnameHeaderTh);
    headerTr.appendChild(contactPhoneHeaderTh);
    tHead.appendChild(headerTr);
    table.appendChild(tHead);
    
    var tbody = document.createElement("tbody");
    tbody.id = "result-list-tbody";
    
    var searchResultLength = searchResult.length;
    if (searchResultLength > 0) {
	for(var i = 0; i < searchResultLength; i++){
            var pensionerId = searchResult[i].pensionerId;
            var firstName = searchResult[i].firstName;
            var middleName = searchResult[i].middleName;
            var surname = searchResult[i].surname;
            var contactPhoneNumber = searchResult[i].contactPhoneNumber;
            var tr = document.createElement("tr");
            
            var countTd = document.createElement("td");
            countTd.innerHTML = count++; 
            
            var pensionerIdTd = document.createElement("td");
            pensionerIdTd.innerHTML = pensionerId;
            
            var firstNameTd = document.createElement("td");
            firstNameTd.innerHTML = firstName;
            
            var middleNameTd = document.createElement("td");
            middleNameTd.innerHTML = middleName;
            
            var surnameTd = document.createElement("td");
            surnameTd.innerHTML = surname;
            
            var contactPhoneNumberTd = document.createElement("td");
            contactPhoneNumberTd.innerHTML = contactPhoneNumber;                        
            
            var editLink = document.createElement("a");
            editLink.className = "waves-effect waves-light btn-floating blue";
            editLink.innerHTML = "<i class='material-icons'>edit</i>";
            editLink.href = "webbackend?intention=view-pensioner&pensioner-id="+pensionerId;
            
            var actionTd = document.createElement("td");
            actionTd.appendChild(editLink);
            
            tr.appendChild(countTd);
            tr.appendChild(pensionerIdTd);
            tr.appendChild(firstNameTd);
            tr.appendChild(middleNameTd);
            tr.appendChild(surnameTd);
            tr.appendChild(contactPhoneNumberTd);
            tr.appendChild(actionTd);
            
            tbody.appendChild(tr);            
        }
    } else {
        var tr = document.createElement("tr");
        var td = document.createElement("td");
        td.innerHTML = "No pensioners found";
        td.className = "center";
        td.setAttribute("colspan", "6");
        tr.appendChild(td);
        tbody.appendChild(tr);
    }	
    table.appendChild(tbody);
}