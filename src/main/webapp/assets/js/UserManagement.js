/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var selectedUser = "";
var role = "";

function handleUserManagement() {
    document.getElementById("addUserForm").onsubmit = function () {
        addUser();
        return false;
    };
    document.getElementById("addRoleForm").onsubmit = function () {
        addRole();
        return false;
    };
    document.getElementById("addPrivilegeForm").onsubmit = function () {
        addPrivilege();
        return false;
    };

}

function getUsers() {
    var allUsers = [];
    $.ajax({
        method: "GET",
        url: "webbackend",
        async: false,
        data: {intention: "get-users"}
    })
            .done(function (result) {
                var response = JSON.parse(result);
                if (response.status === "200") {
                    $.each(response.payLoad, function (i, users) {
                        $.each(users, function (i, user) {
                            allUsers.push(user);
                        });
                    });
                    populateUsers(allUsers);
                } else {
                    Materialize.toast("Could not fetch users from database. Error Code: " + response.status + ". Error Message: " + response.statusMessage, 3000, 'rounded');
                }
            })
            .fail(function (result) {
                var response = JSON.parse(result);
                Materialize.toast("Error getting users. Error Code: " + response.status + ". Error Message: " + response.statusMessage, 3000, 'rounded');
            });
}

function populateUsers(users) {
    clearUserRoles();
    var count = 1;
    var table = document.getElementById("users_list_table");

    var tbody = document.getElementById("users_list_tbody");
    tbody.innerHTML = "";

    if (users.length > 0) {
        for (var i = 0; i < users.length; i++) {
            var username = users[i].username;
            var firstname = users[i].firstName;
            var othernames = users[i].otherNames;
            var userrolesArray = users[i].userRoles;

            var tr = document.createElement("tr");
            tr.setAttribute("onclick", "showUserRoles(this,'" + username + "'," + JSON.stringify(userrolesArray) + ");");
            tr.id = username;

            var countTd = document.createElement("td");
            countTd.innerHTML = count++;

            var usernameTd = document.createElement("td");
            usernameTd.id = username + "-username";
            usernameTd.innerHTML = username;

            var firstnameTd = document.createElement("td");
            firstnameTd.id = username + "-firstname";
            firstnameTd.innerHTML = firstname;

            var othernamesTd = document.createElement("td");
            othernamesTd.id = username + "-othernames";
            othernamesTd.innerHTML = othernames;

            var deleteLink = document.createElement("a");
            deleteLink.className = "waves-effect waves-light btn-floating red";
            deleteLink.innerHTML = "<i class='material-icons'>delete</i>";
            deleteLink.setAttribute("onclick", "showDeleteModal('deleteUser', '" + username + "')");

            var editLink = document.createElement("a");
            editLink.className = "waves-effect waves-light btn-floating blue";
            editLink.innerHTML = "<i class='material-icons'>edit</i>";
            editLink.setAttribute("onclick", "showUpdateUserModal('" + username + "');");

            var actionTd = document.createElement("td");
            actionTd.appendChild(editLink);
            actionTd.appendChild(document.createTextNode(" "));
            actionTd.appendChild(deleteLink);

            tr.appendChild(countTd);
            tr.appendChild(usernameTd);
            tr.appendChild(firstnameTd);
            tr.appendChild(othernamesTd);
            tr.appendChild(actionTd);

            tbody.appendChild(tr);
        }
    } else {
        var tr = document.createElement("tr");
        var td = document.createElement("td");
        td.innerHTML = "No users";
        td.className = "center";
        td.setAttribute("colspan", "5");
        tr.appendChild(td);
        tbody.appendChild(tr);
    }
    table.appendChild(tbody);
    /*
    $('tr#users_list_tbody').ready(function () {
        refreshTable(tableName);
    });
    */
}

function addUser() {
    $.ajax({
        method: "POST",
        url: "webbackend",
        data: $('#addUserForm').serialize()
    })
            .done(function (result) {
                getUsers();
                var response = JSON.parse(result);
                if (response.status === "200") {
                    Materialize.toast("New User Added", 3000, 'rounded');
                    document.getElementById('addUserForm').reset();
                    $('#addUserModal').closeModal();
                } else {
                    Materialize.toast("Could not add user to database. Error Code: " + response.status + ". Error Message: " + response.statusMessage, 3000, 'rounded');
                }
            })
            .fail(function (result) {
                var response = JSON.parse(result);
                Materialize.toast("Adding a user failed. Error Code: " + response.status + ". Error Message: " + response.statusMessage, 3000, 'rounded');
            });
}

function deleteUser(username) {
    $.ajax({
        method: "POST",
        url: "webbackend",
        data: {intention: "delete-user", userId: username}
    })
            .done(function (result) {
                getUsers();
                var response = JSON.parse(result);
                if (response.status === "200") {
                    Materialize.toast("User deleted", 3000, 'rounded');
                } else {
                    Materialize.toast("User cannot be deleted from database. Error Code: " + response.status + ". Error Message: " + response.statusMessage, 3000, 'rounded');
                }
            })
            .fail(function (result) {
                var response = JSON.parse(result);
                Materialize.toast("Deleting user failed. Error Code: " + response.status + ". Error Message: " + response.statusMessage, 3000, 'rounded');
            });
    $('#deleteModal').closeModal();
}

function showUpdateUserModal(username) {

    $('#usernameLabel').addClass('active');
    $('#onameLabel').addClass('active');
    $('#fnameLabel').addClass('active');

    $('#addUserModalHeader').text('Edit User');
    $('#addUserModalIntention').val('update-user'); //change form intention
    $('#addUserForm').attr('id', 'updateUserForm'); //chang form id

    $('#username').val($('#' + username + '-username').text());
    $('#username').attr("readOnly", "true");

    $('#fname').val($('#' + username + '-firstname').text());
    $('#oname').val($('#' + username + '-othernames').text());

    $('#addUserButton').html("<i class='material-icons left'>save</i>Update");

    document.getElementById("updateUserForm").onsubmit = function () {
        updateUser();
        return false;
    };

    $('#addUserModal').openModal({
        complete: function () {
            resetAddUserModal();
        }
    });
}

function resetAddUserModal() {
    $('#addUserModalHeader').text('Add User');
    $('#addUserModalIntention').val('add-user'); //reset add user form intention
    $('#updateUserForm').attr('id', 'addUserForm'); //reset add user forn id
    $('#username').val('');
    $('#username').removeAttr('readOnly');
    $('#fname').val('');
    $('#oname').val('');

    $('#addUserButton').html("<i class='material-icons left'>save</i>Add User");
}

function updateUser() {
    $.ajax({
        method: "POST",
        url: "webbackend",
        data: $('#updateUserForm').serialize()
    })
            .done(function (result) {
                getUsers();
                var response = JSON.parse(result);
                if (response.status === "200") {
                    Materialize.toast('User details successfully', 3000, 'rounded');
                    resetAddUserModal();
                    $('#addUserModal').closeModal();
                } else {
                    Materialize.toast("Could not update user details in the database. Error Code: " + response.status + ". Error Message: " + response.statusMessage, 3000, 'rounded');
                }
            })
            .fail(function (result) {
                var response = JSON.parse(result);
                Materialize.toast("Updating user details failed. Error Code: " + response.status + ". Error Message: " + response.statusMessage, 3000, 'rounded');
            });
}

function addRole() {
    $.ajax({
        method: "POST",
        url: "webbackend",
        data: $('#addRoleForm').serialize()
    })
            .done(function (result) {
                getRoles();
                var response = JSON.parse(result);
                if (response.status === "200") {
                    Materialize.toast("New Role Added.", 3000, 'rounded');
                    document.getElementById('addRoleForm').reset();
                    $('#addRoleModal').closeModal();
                } else {
                    Materialize.toast("Could not add role to database. Error Code: " + response.status + ". Error Message: " + response.statusMessage, 3000, 'rounded');
                }
            })
            .fail(function (result) {
                var response = JSON.parse(result);
                Materialize.toast("Adding a role failed. Error Code: " + response.status + ". Error Message: " + response.statusMessage, 3000, 'rounded');
            });
}

function getRoles() {
    var allRoles = [];
    $.ajax({
        method: "GET",
        url: "webbackend",
        async: false,
        data: {intention: "get-roles"}
    })
            .done(function (result) {
                var response = JSON.parse(result);
                if (response.status === "200") {
                    $.each(response.payLoad, function (i, roles) {
                        $.each(roles, function (i, role) {
                            allRoles.push(role);
                        });
                    });
                } else {
                    Materialize.toast("Could not fetch rolee from database. Error Code: " + response.status + ". Error Message: " + response.statusMessage, 3000, 'rounded');
                }
                populateRoles(allRoles);
            })
            .fail(function (result) {
                var response = JSON.parse(result);
                Materialize.toast("Could not get privileges. Error Code: " + response.status + ". Error Message: " + response.statusMessage, 3000, 'rounded');
            });
}

function populateRoles(roles) {
    clearRolePrivileges();
    var count = 1;

    var table = document.getElementById("roles_list_table");

    var tbody = document.getElementById("roles_list_tbody");
    tbody.innerHTML = "";

    if (roles.length > 0) {
        for (var i = 0; i < roles.length; i++) {
            var roleId = roles[i].roleId;
            var description = roles[i].description;
            var rolePrivilegesArray = roles[i].privileges;

            var tr = document.createElement("tr");
            tr.setAttribute("onclick", "showRolePrivileges(this,'" + roleId + "'," + JSON.stringify(rolePrivilegesArray) + ");");
            tr.id = roleId;

            var countTd = document.createElement("td");
            countTd.innerHTML = count++;

            var roleIdTd = document.createElement("td");
            roleIdTd.id = roleId + "-roleId";
            roleIdTd.innerHTML = roleId;

            var descriptionTd = document.createElement("td");
            descriptionTd.id = roleId + "-description";
            descriptionTd.innerHTML = description;

            var deleteLink = document.createElement("a");
            deleteLink.className = "waves-effect waves-light btn-floating red";
            deleteLink.innerHTML = "<i class='material-icons'>delete</i>";
            deleteLink.setAttribute("onclick", "showDeleteModal('deleteRole', '" + roleId + "')");

            var editLink = document.createElement("a");
            editLink.className = "waves-effect waves-light btn-floating blue";
            editLink.innerHTML = "<i class='material-icons'>edit</i>";
            editLink.setAttribute("onclick", "showUpdateRoleModal('" + roleId + "');");

            var actionTd = document.createElement("td");
            actionTd.appendChild(editLink);
            actionTd.appendChild(document.createTextNode(" "));
            actionTd.appendChild(deleteLink);

            tr.appendChild(countTd);
            tr.appendChild(roleIdTd);
            tr.appendChild(descriptionTd);
            tr.appendChild(actionTd);

            tbody.appendChild(tr);
        }
    } else {
        var tr = document.createElement("tr");
        var td = document.createElement("td");
        td.innerHTML = "No roles";
        td.className = "center";
        td.setAttribute("colspan", "4");
        tr.appendChild(td);
        tbody.appendChild(tr);
    }
    table.appendChild(tbody);
}

function showUpdateRoleModal(roleId) {

    $('#roleIdLabel').addClass('active');
    $('#roleDescriptionLabel').addClass('active');

    $('#addRoleModalHeader').text('Edit Role');
    $('#addRoleModalIntention').val('update-role'); //change form intention
    $('#addRoleForm').attr('id', 'updateRoleForm'); //chang form id

    $('#role-id').val($('#' + roleId + '-roleId').text());
    $('#role-id').attr("readOnly", "true");

    $('#role-description').val($('#' + roleId + '-description').text());

    $('#addRoleButton').html("<i class='material-icons left'>save</i>Update");

    document.getElementById("updateRoleForm").onsubmit = function () {
        updateRole();
        return false;
    };

    $('#addRoleModal').openModal({
        complete: function () {
            resetAddRoleModal();
        }
    });
}

function resetAddRoleModal() {
    $('#addRoleModalHeader').text('Add Role');
    $('#addRoleModalIntention').val('add-role');
    $('#updateRoleForm').attr('id', 'addRoleForm');
    $('#role-id').val('');
    $('#role-id').removeAttr('readOnly');
    $('#role-description').val('');

    $('#addRoleButton').html("<i class='material-icons left'>save</i>Add Role");
}

function updateRole() {
    $.ajax({
        method: "POST",
        url: "webbackend",
        data: $('#updateRoleForm').serialize()
    })
            .done(function (result) {
                getRoles();
                var response = JSON.parse(result);
                if (response.status === "200") {
                    Materialize.toast("Role description successfully updated", 3000, 'rounded');
                    resetAddRoleModal();
                    $('#addRoleModal').closeModal();
                } else {
                    Materialize.toast("Could not update role description in the database. Error Code: " + response.status + ". Error Message: " + response.statusMessage, 3000, 'rounded');
                }
            })
            .fail(function (result) {
                var response = JSON.parse(result);
                Materialize.toast("Updating role description failed. Error Code: " + response.status + ". Error Message: " + response.statusMessage, 3000, 'rounded');
            });
}

function deleteRole(roleId) {
    $.ajax({
        method: "POST",
        url: "webbackend",
        data: {intention: "delete-role", "role-id": roleId}
    })
            .done(function (result) {
                getRoles();
                var response = JSON.parse(result);
                if (response.status === "200") {
                    Materialize.toast("Role deleted", 3000, 'rounded');
                } else {
                    Materialize.toast("Role cannot be deleted from database. Error Message: " + response.statusMessage, 3000, 'rounded');
                }
            })
            .fail(function (result) {
                var response = JSON.parse(result);
                Materialize.toast("Deleting role failed. Error Message: " + response.statusMessage, 3000, 'rounded');
            });
    $("#deleteModal").closeModal();
}

function addPrivilege() {
    $.ajax({
        method: "POST",
        url: "webbackend",
        data: $('#addPrivilegeForm').serialize()
    })
            .done(function (result) {
                console.log(result);
                getPrivileges();
                var response = JSON.parse(result);
                if (response.status === "200") {
                    getPrivileges();
                    Materialize.toast("New Privilege Added", 3000, 'rounded');
                    document.getElementById('addPrivilegeForm').reset();
                    $('#addPrivilegeModal').closeModal();
                } else {
                    Materialize.toast("Could not add privilege to database. Error Code: " + response.status + ". Error Message: " + response.statusMessage, 3000, 'rounded');
                }
            })
            .fail(function (result) {
                console.log(result);
                var response = JSON.parse(result);
                Materialize.toast("Adding a privilege failed. Error Code: " + response.status + ". Error Message: " + response.statusMessage, 3000, 'rounded');
            });
}

function getPrivileges() {
    var allPrivileges = [];
    $.ajax({
        method: "GET",
        url: "webbackend",
        data: {intention: "get-privileges"}
    })
            .done(function (result) {
                var response = JSON.parse(result);
                if (response.status === "200") {
                    $.each(response.payLoad, function (i, privileges) {
                        $.each(privileges, function (i, privilege) {
                            allPrivileges.push(privilege);
                        });
                    });
                } else {
                    Materialize.toast("Could not fetch privileges from database. Error Code: " + response.status + ". Error Message: " + response.statusMessage, 3000, 'rounded');
                }
                populatePrivileges(allPrivileges);
            })
            .fail(function (result) {
                var response = JSON.parse(result);
                Materialize.toast("Could not get privileges. Error Code: " + response.status + ". Error Message: " + response.statusMessage, 3000, 'rounded');
            });
}

function populatePrivileges(privileges) {
    var count = 1;

    var table = document.getElementById("privileges_list_table");

    var tbody = document.getElementById("privileges_list_tbody");
    tbody.innerHTML = "";

    if (privileges.length > 0) {
        for (var i = 0; i < privileges.length; i++) {
            var privilegeId = privileges[i].privilegeId;
            var description = privileges[i].description;
            var menuLink = privileges[i].menuLink;

            var tr = document.createElement("tr");

            var countTd = document.createElement("td");
            countTd.innerHTML = count++;

            var privilegeIdTd = document.createElement("td");
            privilegeIdTd.id = privilegeId + "-privilegeId";
            privilegeIdTd.innerHTML = privilegeId;

            var descriptionTd = document.createElement("td");
            descriptionTd.id = privilegeId + "-description";
            descriptionTd.innerHTML = description;

            var menuLinkTd = document.createElement("td");
            menuLinkTd.id = privilegeId + "-menuLink";
            menuLinkTd.innerHTML = menuLink;

            var deleteLink = document.createElement("a");
            deleteLink.className = "waves-effect waves-light btn-floating red";
            deleteLink.innerHTML = "<i class='material-icons'>delete</i>";
            deleteLink.setAttribute("onclick", "showDeleteModal('deletePrivilege', '" + privilegeId + "')");

            var editLink = document.createElement("a");
            editLink.className = "waves-effect waves-light btn-floating blue";
            editLink.innerHTML = "<i class='material-icons'>edit</i>";
            editLink.setAttribute("onclick", "showUpdatePrivilegeModal('" + privilegeId + "')");

            var actionTd = document.createElement("td");
            actionTd.appendChild(editLink);
            actionTd.appendChild(document.createTextNode(" "));
            actionTd.appendChild(deleteLink);

            tr.appendChild(countTd);
            tr.appendChild(privilegeIdTd);
            tr.appendChild(descriptionTd);
            tr.appendChild(menuLinkTd);
            tr.appendChild(actionTd);

            tbody.appendChild(tr);
        }
    } else {
        var tr = document.createElement("tr");
        var td = document.createElement("td");
        td.innerHTML = "No Privileges";
        td.className = "center";
        td.setAttribute("colspan", "4");
        tr.appendChild(td);
        tbody.appendChild(tr);
    }
    table.appendChild(tbody);
}

function showUpdatePrivilegeModal(privelegeId) {

    $('#privilegeIdLabel').addClass('active');
    $('#privilegeDescriptionLabel').addClass('active');
    $('#privilegeMenuLinkLabel').addClass('active');

    $('#addPrivilegeModalHeader').text('Edit Privilege');
    $('#addPrivilegeModalIntention').val('update-privilege'); //change form intention
    $('#addPrivilegeForm').attr('id', 'updatePrivilegeForm'); //chang form id

    $('#privilege-id').val($('#' + privelegeId + '-privilegeId').text());
    $('#privilege-id').attr("readOnly", "true");

    $('#privilege-description').val($('#' + privelegeId + '-description').text());
    $('#privilege-menuLink').val($('#' + privelegeId + '-menuLink').text());

    $('#addPrivilegeButton').html("<i class='material-icons left'>save</i>Update");

    document.getElementById("updatePrivilegeForm").onsubmit = function () {
        updatePrivilege();
        return false;
    };

    //open modal
    $('#addPrivilegeModal').openModal({
        complete: function () {
            resetAddPrivilegeModal();
        }
    });
}

function resetAddPrivilegeModal() {
    $('#addPrivilegeModalHeader').text('Add Privilege');
    $('#addPrivilegeModalIntention').val('add-privilege');
    $('#updatePrivilegeForm').attr('id', 'addPrivilegeForm');
    $('#privilege-id').val('');
    $('#privilege-id').removeAttr('readOnly');
    $('#privilege-description').val('');
    $('#privilege-menuLink').val('');

    $('#addPrivelegeButton').html("<i class='material-icons left'>save</i>Add Privilege");
}

function updatePrivilege() {
    $.ajax({
        method: "POST",
        url: "webbackend",
        data: $('#updatePrivilegeForm').serialize()
    })
            .done(function (result) {
                getPrivileges();
                var response = JSON.parse(result);
                if (response.status === "200") {
                    Materialize.toast("Privilege successfully updated", 3000, 'rounded');
                    resetAddPrivilegeModal();
                    $('#addPrivilegeModal').closeModal();
                } else {
                    Materialize.toast("Could not update privilege in the database. Error Code: " + response.status + ". Error Message: " + response.statusMessage, 3000, 'rounded');
                }
            })
            .fail(function (result) {
                var response = JSON.parse(result);
                Materialize.toast("Updating privilege failed. Error Code: " + response.status + ". Error Message: " + response.statusMessage, 3000, 'rounded');
            });
}

function deletePrivilege(privilegeId) {
    $.ajax({
        method: "POST",
        url: "webbackend",
        data: {intention: "delete-privilege", "privilege-id": privilegeId}
    })
            .done(function (msg) {
                getPrivileges();
                var response = JSON.parse(msg);
                if (response.status === "200") {
                    Materialize.toast("Privilege deleted", 3000, 'rounded');
                } else {
                    Materialize.toast("Privilege cannot be deleted from database. Error Code: " + response.status + ". Error Message: " + response.statusMessage, 3000, 'rounded');
                }
            })
            .fail(function (result) {
                var response = JSON.parse(result);
                Materialize.toast("Deleting privilege failed. Error Code: " + response.status + ". Error Message: " + response.statusMessage, 3000, 'rounded');
            });
    $("#deleteModal").closeModal();
}

function showUserRoles(item, username, userrole) {
    selectedUser = username;
    $('tbody#users_list_tbody tr').removeClass('active-tr');
    $(item).addClass('active-tr');
    populateUserRoles(userrole);
}

function populateUserRoles(userroles) {
    var userRolesSelect = document.getElementById('user-roles');
    userRolesSelect.options.length = 0;

    $.each(userroles, function (i, userrole) {
        userRolesSelect.options[userRolesSelect.options.length] = new Option(userrole.roleId, userrole.roleId, false, false);
    });
    getOtherRoles(userroles);
}

function getOtherRoles(allUserroles) {
    var allRoles = [], userroles = [];
    $.ajax({
        method: "GET",
        url: "webbackend",
        data: {intention: "get-roles"}
    })
            .done(function (result) {
                var response = JSON.parse(result);
                if (response.status === "200") {
                    $.each(response.payLoad, function (i, roles) {
                        $.each(roles, function (i, role) {
                            allRoles.push(role.roleId);
                        });
                    });
                } else {
                    Materialize.toast("Could not fetch roles from database. Error Code: " + response.status + ". Error Message: " + response.statusMessage, 3000, 'rounded');
                }
                $.each(allUserroles, function (i, role) {
                    userroles.push(role.roleId);
                });
                var unassignedRoles = [];
                $.grep(allRoles, function (el) {
                    if ($.inArray(el, userroles) === -1)
                        unassignedRoles.push(el);
                });
                var otherRolesSelect = document.getElementById('other-roles');
                otherRolesSelect.options.length = 0;

                $.each(unassignedRoles, function (i, unassignedRole) {
                    otherRolesSelect.options[otherRolesSelect.options.length] = new Option(unassignedRole, unassignedRole, false, false);
                });
            })
            .fail(function (result) {
                var response = JSON.parse(result);
                Materialize.toast("Could not get privileges. Error Code: " + response.status + ". Error Message: " + response.statusMessage, 3000, 'rounded');
            });
}

function assignSelectedRoles() {
    $('#other-roles :selected').each(function (i, selected) {
        addUserRole(selectedUser, $(selected).text());
    });
    refreshTable('users');
}

function assignAllRoles() {
    $('#other-roles option').each(function (i, option) {
        addUserRole(selectedUser, $(option).text());
    });
    refreshTable('users');
}

function unassignSelectedRoles() {
    $('#user-roles :selected').each(function (i, selected) {
        removeUserRole(selectedUser, $(selected).text());
    });
    refreshTable('users');
}

function unassignAllRoles() {
    $('#user-roles option').each(function (i, option) {
        removeUserRole(selectedUser, $(option).text());
    });
    refreshTable('users');
}

function addUserRole(username, role) {
    $.ajax({
        method: "POST",
        url: "webbackend",
        data: {intention: "add-user-role", userId: username, "role-id": role}
    })
            .done(function (result) {
                var response = JSON.parse(result);
                if (response.status !== "200") {
                    Materialize.toast("Cannot add user role", 3000, 'rounded');
                    return;
                }
            })
            .fail(function (result) {
                Materialize.toast(result, 3000, 'rounded');
            });
}

function removeUserRole(username, role) {
    $.ajax({
        method: "POST",
        url: "webbackend",
        data: {intention: "delete-user-role", userId: username, "role-id": role}
    })
            .done(function (result) {
                var response = JSON.parse(result);
                if (response.status !== "200") {
                    Materialize.toast("Cannot remove user role", 3000, 'rounded');
                    return;
                }
            })
            .fail(function (result) {
                alert(result);
            });
}

function showRolePrivileges(item, role, rolePrivileges) {
    selectedRole = role;
    $('tbody#roles_list_tbody tr').removeClass('active-tr');
    $(item).addClass('active-tr');
    populateRolePrivileges(rolePrivileges);
}

function populateRolePrivileges(rolePrivileges) {
    var rolePrivilegeSelect = document.getElementById('role-privileges');
    rolePrivilegeSelect.options.length = 0;

    $.each(rolePrivileges, function (i, rolePrivilege) {
        rolePrivilegeSelect.options[rolePrivilegeSelect.options.length] = new Option(rolePrivilege.privilegeId, rolePrivilege.privilegeId, false, false);
    });
    getOtherPrivileges(rolePrivileges);
}

function getOtherPrivileges(allRolePrivileges) {
    var allPrivileges = [], rolePrivileges = [];
    $.ajax({
        method: "GET",
        url: "webbackend",
        data: {intention: "get-privileges"}
    })
            .done(function (result) {
                var response = JSON.parse(result);
                if (response.status === "200") {
                    $.each(response.payLoad, function (i, privileges) {
                        $.each(privileges, function (i, privilege) {
                            allPrivileges.push(privilege.privilegeId);
                        });
                    });
                } else {
                    Materialize.toast("Could not fetch privileges from database. Error Code: " + response.status + ". Error Message: " + response.statusMessage, 3000, 'rounded');
                }
                $.each(allRolePrivileges, function (i, privilege) {
                    rolePrivileges.push(privilege.privilegeId);
                });
                var unassignedPrivileges = [];
                $.grep(allPrivileges, function (el) {
                    if ($.inArray(el, rolePrivileges) === -1)
                        unassignedPrivileges.push(el);
                });
                var otherPrivilegesSelect = document.getElementById('other-privileges');
                otherPrivilegesSelect.options.length = 0;

                $.each(unassignedPrivileges, function (i, unassignedPrivilege) {
                    otherPrivilegesSelect.options[otherPrivilegesSelect.options.length] = new Option(unassignedPrivilege, unassignedPrivilege, false, false);
                });
            })
            .fail(function (result) {
                var response = JSON.parse(result);
                Materialize.toast("Could not get privileges. Error Code: " + response.status + ". Error Message: " + response.statusMessage, 3000, 'rounded');
            });
}

function assignSelectedPrivileges() {
    $('#other-privileges :selected').each(function (i, selected) {
        addRolePrivilege(selectedRole, $(selected).text());
    });
    refreshTable('roles');
}

function assignAllPrivileges() {
    $('#other-privileges option').each(function (i, option) {
        addRolePrivilege(selectedRole, $(option).text());
    });
    refreshTable('roles');
}

function unassignSelectedPrivileges() {
    $('#role-privileges :selected').each(function (i, selected) {
        removeRolePrivilege(selectedRole, $(selected).text());
    });
    refreshTable('roles');
}

function unassignAllPrivileges() {
    $('#role-privileges option').each(function (i, option) {
        removeRolePrivilege(selectedRole, $(option).text());
    });
    refreshTable('roles');
}

function addRolePrivilege(role, privilege) {
    $.ajax({
        method: "POST",
        url: "webbackend",
        data: {intention: "add-role-privilege", "role-id": role, "privilege-id": privilege}
    })
            .done(function (result) {
                var response = JSON.parse(result);
                if (response.status !== "200") {
                    Materialize.toast("Cannot add role privilege", 3000, 'rounded');
                    return;
                }
            })
            .fail(function (result) {
                Materialize.toast(result, 3000, 'rounded');
            });
}

function removeRolePrivilege(role, privilege) {
    $.ajax({
        method: "POST",
        url: "webbackend",
        data: {intention: "delete-role-privilege", "role-id": role, "privilege-id": privilege}
    })
            .done(function (result) {
                var response = JSON.parse(result);
                if (response.status !== "200") {
                    Materialize.toast("Cannot remove role privilege", 3000, 'rounded');
                    return;
                }
            })
            .fail(function (result) {
                Materialize.toast(result, 3000, 'rounded');
            });
}

function clearUserRoles() {
    var userRolesSelect = document.getElementById('user-roles');
    userRolesSelect.options.length = 0;

    var otherRolesSelect = document.getElementById('other-roles');
    otherRolesSelect.options.length = 0;
}

function clearRolePrivileges() {
    var rolePrivilegeSelect = document.getElementById('role-privileges');
    rolePrivilegeSelect.options.length = 0;

    var otherPrivilegeSelect = document.getElementById('other-privileges');
    otherPrivilegeSelect.options.length = 0;
}

function refreshTable(tableName) {
    if (tableName === "users") {
        setTimeout(function () {
            getUsers();
            $("tr#" + selectedUser).click();
        }, 1000);

    } else if (tableName === "roles") {
        setTimeout(function () {
            getRoles();
            $("tr#" + selectedRole).click();
        }, 1000);
    }
}

function showDeleteModal(action, variable) {
    if (action === 'deleteUser') {
        $("#deleteModalContent").html("<p>Are you sure you want to delete user " + variable + " ?<i class='material-icons left'>help</i></p>");
        $("#yesButton").attr("onclick", "deleteUser('" + variable + "')");
        $('#deleteModal').openModal();
    } else if (action === 'deleteRole') {
        $("#deleteModalContent").html("<p>Are you sure you want to delete " + variable + " role?<i class='material-icons left'>help</i></p>");
        $("#yesButton").attr("onclick", "deleteRole('" + variable + "')");
        $('#deleteModal').openModal();
    } else if (action === 'deletePrivilege') {
        $("#deleteModalContent").html("<p>Are you sure you want to delete " + variable + " privilege?<i class='material-icons left'>help</i></p>");
        $("#yesButton").attr("onclick", "deletePrivilege('" + variable + "')");
        $('#deleteModal').openModal();
    } else {
        $('#deleteModal').closeModal();
        return false;
    }
}