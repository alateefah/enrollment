(function($){
    $(function(){
        $('.button-collapse').sideNav({'edge': 'left'});
        $('ul.tabs').tabs();
        $('select').material_select();
        $('.datepicker').pickadate({
          selectMonths: true, // Creates a dropdown to control month
          selectYears: 15 // Creates a dropdown of 15 years to control year
        });
        $('.modal-trigger').leanModal();
        $('#users_list_table').DataTable({
            searching: false,
            ordering:  false
        });
            
      
    }); // end of document ready
})(jQuery); // end of jQuery name space