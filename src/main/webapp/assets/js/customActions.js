/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function setFocus(componentId) {
    $("#"+componentId ).focus();
}

function toggleVisibility(componentId) {    
    $("#"+componentId).toggle();
}

function disableLoginFields() {
    $("#username").prop('disabled', true);
    $("#password").prop('disabled', true);
    toggleVisibility('preloader');
    return false;
}

function enableLoginFields() {
    $("#username").prop('disabled', false);
    $("#password").prop('disabled', false);
    toggleVisibility('preloader');
}

function submitLoginForm() {
    if ($("#preloader").is(":visible")) {
        Materialize.toast('Trying to log you in. Please refresh page if this persists.', 3000, 'rounded');
    } else {        
        $("#login").submit();

        //do Login
        //enable login fields if login fails
        //otherwise show the user's dasboard
    }        
}
