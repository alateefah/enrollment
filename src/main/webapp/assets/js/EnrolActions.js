/* 
    Document   : cameraAction
    Created on : Jul 7, 2016, 4:10: PM
    Author     : Lateefah
*/

var canvas, context, video, localStream, videoSelect;
var capturePhoto;

function handleCapturePhoto(){
    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
    
    if (!navigator.getUserMedia) {        
        Materialize.toast("Media not supported", 3000, 'rounded');
        return;
    }
    
    navigator.mediaDevices.enumerateDevices()
        .then(function(devices) {
            getSources(devices);
        })
        .catch(function(err) {
           Materialize.toast(err.name + ": " + err.message, 3000, 'rounded');
        });
    
    document.getElementById('cameraChoice').onchange = function(){ 
        startCamera(); 
    };
       
    document.getElementById("stopCameraBtn").addEventListener("click", function(){
	stopCamera();
    });
        
    document.getElementById("capturePhotoBtn").addEventListener("click", function(){
        context.drawImage(video, 0, 0, 300, 150);        
        capturePhoto = canvas.toDataURL("image/png"); 
        if (capturePhoto.length > 0) {
            $('#passport').val(capturePhoto);
        }
        document.getElementById("saved-photo").src="";
    });        
    
    document.getElementById("enrolleeForm").onsubmit = function(){
        savePensioner();
        return false;
    };
}

function getSources(sourceInfos){
    videoSelect = document.getElementById('cameraChoice');
    videoSelect.length = 0;
    videoSelect.options[0] = new Option("Select camera...", "", false, false);
    
    for(var i=1; i !== sourceInfos.length; ++i){
        var sourceInfo = sourceInfos[i];
        var option = document.createElement('option');
        option.value = sourceInfo.deviceId;
            if(sourceInfo.kind === 'videoinput'){
                option.text = sourceInfo.label || 'Camera' + (videoSelect.length + 1);
                videoSelect.appendChild(option);
            }
    }
}

function startCamera(){    
    var videoSource = videoSelect.value;
    if (videoSource === null || videoSource === ""){
        stopCamera();
        handleCapturePhoto();
        return;
    }
    if(localStream){
        localStream.getVideoTracks()[0].stop();        
        localStream = null;
    }
    canvas = document.getElementById("capture_canvas");
    context = canvas.getContext("2d");
    video = document.getElementById("capture_video");
    var constraints = {
        video:  {optional: [{
                    sourceId: videoSource
                }]
        }
    };     
    if (navigator.getUserMedia.name === "mozGetUserMedia"){
        navigator.mediaDevices.getUserMedia(constraints)    
         .then(function(stream) {
            video.src = window.URL.createObjectURL(stream);
            video.onloadedmetadata = function(e){
                video.play();
            };
            localStream = stream;
        })
         .catch(function(err) {
            Materialize.toast(err.name + ": " + err.message, 3000, 'rounded');
        });
    } else {
        navigator.getUserMedia(constraints, successCallback, errorCallback);
    }
    show_elements([document.getElementById("capturePhotoBtn"), document.getElementById("stopCameraBtn")], "inline");
}

function successCallback(stream){
    window.stream = stream; 
    video.src = window.URL.createObjectURL(stream);
    video.play();
    localStream = stream;
}
 
function errorCallback(error) {
    Materialize.toast('navigator.getUserMedia error: ' + error, 3000, 'rounded');    
}

function stopCamera(){
    video.src= "";
    hide_elements([document.getElementById("capturePhotoBtn"), document.getElementById("stopCameraBtn")]);
    document.getElementById("cameraChoice").value = "";
    
    if(localStream){
        localStream.getVideoTracks()[0].stop();        
        localStream = null;
    }
}

function show_elements(elements, style){
    for(var i = 0; i < elements.length; i++){
        if(elements[i]){
            elements[i].style.display = style;
        }
    }
}

function hide_elements(elements){
    for(var i = 0; i < elements.length; i++){
        if(elements[i]){
            elements[i].style.display = "none";
        }
    }
}

function savePensioner() {    
    document.getElementById('progess').style.display = 'inline';        
    $.ajax({
      method: "POST",
      url: "webbackend",
      data: $('#enrolleeForm').serialize()
    })
      .done(function( msg ) {
        Materialize.toast('Data Saved: ' + msg, 3000, 'rounded');
        document.getElementById('progess').style.display = 'none';  
        var response = JSON.parse(msg);
        if (response.status === "200") {
            var pensionerId = response.payLoad[0].pensionerId;
            alert("Pensioner ID: " + pensionerId);
            var printSlipButton = document.getElementById("print-slip-button");
            printSlipButton.onclick = function() { generateSlip(pensionerId) };
        }
      })
      .fail(function( msg ) {
        Materialize.toast('Data not saved: ' + msg, 3000, 'rounded');
        document.getElementById('progess').style.display = 'none';        
      });
  
}

function generateSlip(pensionerId) {
    window.open("webbackend?intention=print-slip&pensioner-id="+pensionerId,'_blank');
}

function toggleProfPermSecGenSection() {
    if (document.getElementById('profPermSecGenSectionLabel').checked) {
        document.getElementById("profPermSecGenSection").disabled = false;
    } else {
        document.getElementById("profPermSecGenSection").disabled = true;
    }
}
