<%-- 
    Document   : dashboard
    Created on : Jul 4, 2016, 10:50:50 PM
    Author     : buls
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.List, java.util.ArrayList, com.intellectualapps.pencom.pojo.AppRole, com.intellectualapps.pencom.pojo.AppPrivilege" %>
<!DOCTYPE html>
    <% 
        List<AppRole> userRoles = (List<AppRole>) request.getAttribute("privileges");
            if (userRoles == null) {
                userRoles = new ArrayList<AppRole>();
            }
    %>
    <div class="row">
        <% for (AppRole userRole : userRoles) {
                for (AppPrivilege userPrivilege : userRole.getPrivileges()) {    
        %>
        <div class="col s12 m2">
            <a href="webbackend?intention=<%= userPrivilege.getMenuLink() %>" class="waves-effect waves-block waves-grey">
                <!-- card -->
                <div class="card hoverable">
                    <div class="card-content">
                        <div class="row">
                            <div class="col s12 center-align">
                                <i class="material-icons large"><%= userPrivilege.getMenuIcon() %></i>
                            </div>
                            <div class="col s12 center-align">
                                <div><%= userPrivilege.getPrivilegeId() %></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end card -->
            </a>
        </div>
        <%     }

          } %>
    </div>
