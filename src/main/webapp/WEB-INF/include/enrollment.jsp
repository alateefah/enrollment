<%-- 
    Document   : enrollment
    Created on : Jul 7, 2016, 2:49:14 AM
    Author     : buls
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ page import="java.util.List, java.util.ArrayList, java.text.SimpleDateFormat,
                    com.intellectualapps.pencom.pojo.AppPensionerBasic,
                    com.intellectualapps.pencom.pojo.AppPensionerEmployment,
                    com.intellectualapps.pencom.pojo.AppServiceOrganisationLookUp,
                    com.intellectualapps.pencom.model.ServiceOrganisationLookUp,
                    com.intellectualapps.pencom.pojo.AppPfaLookUp,
                    com.intellectualapps.pencom.model.PfaLookUp,
                    com.intellectualapps.pencom.pojo.AppHonorificLookUp,
                    com.intellectualapps.pencom.model.HonorificLookUp,
                    com.intellectualapps.pencom.pojo.AppGenderLookUp,
                    com.intellectualapps.pencom.model.GenderLookUp,
                    com.intellectualapps.pencom.pojo.AppMaritalStatusLookUp,
                    com.intellectualapps.pencom.model.MaritalStatusLookUp,
                    com.intellectualapps.pencom.pojo.AppEmployerLookUp,
                    com.intellectualapps.pencom.model.EmployerLookUp,
                    com.intellectualapps.pencom.pojo.AppStateLookUp,
                    com.intellectualapps.pencom.model.StateLookUp,
                    com.intellectualapps.pencom.pojo.AppRetirementModeLookUp,
                    com.intellectualapps.pencom.model.RetirementModeLookUp,
                    com.intellectualapps.pencom.pojo.AppSalaryStructureLookUp,
                    com.intellectualapps.pencom.model.SalaryStructureLookUp,
                    com.intellectualapps.pencom.pojo.AppGlLookUp,
                    com.intellectualapps.pencom.model.GlLookUp,
                    com.intellectualapps.pencom.pojo.AppRankLookUp,
                    com.intellectualapps.pencom.model.RankLookUp" %>
<% 
    ArrayList<AppServiceOrganisationLookUp> allServiceOrganisations = (ArrayList<AppServiceOrganisationLookUp>) request.getAttribute("service-organisations");
    if (allServiceOrganisations == null || allServiceOrganisations.size() == 0) {
        allServiceOrganisations = new ArrayList<AppServiceOrganisationLookUp>();
    }
    
    ArrayList<AppPfaLookUp> allPfas = (ArrayList<AppPfaLookUp>) request.getAttribute("pfas");
    if (allPfas == null || allPfas.size() == 0) {
        allPfas = new ArrayList<AppPfaLookUp>();
    }
    
    ArrayList<AppHonorificLookUp> allHonorifics = (ArrayList<AppHonorificLookUp>) request.getAttribute("honorifics");
    if (allHonorifics == null || allHonorifics.size() == 0) {
        allHonorifics = new ArrayList<AppHonorificLookUp>();
    }
    
    ArrayList<AppGenderLookUp> allGenders = (ArrayList<AppGenderLookUp>) request.getAttribute("genders");
    if (allGenders == null || allGenders.size() == 0) {
        allGenders = new ArrayList<AppGenderLookUp>();
    }
    
    ArrayList<AppMaritalStatusLookUp> allMaritalStatuses = (ArrayList<AppMaritalStatusLookUp>) request.getAttribute("marital-statuses");
    if (allMaritalStatuses == null || allMaritalStatuses.size() == 0) {
        allMaritalStatuses = new ArrayList<AppMaritalStatusLookUp>();
    }
    
    ArrayList<AppEmployerLookUp> allEmployers = (ArrayList<AppEmployerLookUp>) request.getAttribute("employers");
    if (allEmployers == null || allEmployers.size() == 0) {
        allEmployers = new ArrayList<AppEmployerLookUp>();
    }
    
    ArrayList<AppStateLookUp> allStates = (ArrayList<AppStateLookUp>) request.getAttribute("states");
    if (allStates == null || allStates.size() == 0) {
        allStates = new ArrayList<AppStateLookUp>();
    }
    
    ArrayList<AppRetirementModeLookUp> allRetirementModes = (ArrayList<AppRetirementModeLookUp>) request.getAttribute("retirement-modes");
    if (allRetirementModes == null || allRetirementModes.size() == 0) {
        allRetirementModes = new ArrayList<AppRetirementModeLookUp>();
    }
    
    ArrayList<AppSalaryStructureLookUp> allSalaryStructures = (ArrayList<AppSalaryStructureLookUp>) request.getAttribute("salary-structures");
    if (allSalaryStructures == null || allSalaryStructures.size() == 0) {
        allSalaryStructures = new ArrayList<AppSalaryStructureLookUp>();
    }
    
    ArrayList<AppSalaryStructureLookUp> allConsolidatedSalaryStructures = (ArrayList<AppSalaryStructureLookUp>) request.getAttribute("consolidated-salary-structures");
    if (allConsolidatedSalaryStructures == null || allConsolidatedSalaryStructures.size() == 0) {
        allConsolidatedSalaryStructures = new ArrayList<AppSalaryStructureLookUp>();
    }
    
    ArrayList<AppSalaryStructureLookUp> allEnhancedConsolidatedSalaryStructures = (ArrayList<AppSalaryStructureLookUp>) request.getAttribute("enhanced-consolidated-salary-structures");
    if (allEnhancedConsolidatedSalaryStructures == null || allEnhancedConsolidatedSalaryStructures.size() == 0) {
        allEnhancedConsolidatedSalaryStructures = new ArrayList<AppSalaryStructureLookUp>();
    }
    
    ArrayList<AppGlLookUp> allGls = (ArrayList<AppGlLookUp>) request.getAttribute("gls");
    if (allGls == null || allGls.size() == 0) {
        allGls = new ArrayList<AppGlLookUp>();
    }
    
    ArrayList<AppRankLookUp> allRanks = (ArrayList<AppRankLookUp>) request.getAttribute("ranks");
    if (allRanks == null || allRanks.size() == 0) {
        allRanks = new ArrayList<AppRankLookUp>();
    }

    AppPensionerBasic appPensionerBasic = (AppPensionerBasic) request.getAttribute("pensioner");
    if (appPensionerBasic == null) {
        appPensionerBasic = new AppPensionerBasic();
    }
    
    SimpleDateFormat dateFormatter = new SimpleDateFormat("dd MMMM, yyyy");
%>

<script type="text/javascript">
    document.title = "Enrol \xB7 PENCOM Enrollment";
</script>

<script type="text/javascript" src="assets/js/EnrolActions.js"></script>
<script type="text/javascript">
   window.onload = function() { handleCapturePhoto(); };
</script>
<!DOCTYPE html>
            <div class="row">
                <div class="col s12 m12">
                    <a class="waves-effect waves-light btn grey lighten-2 grey-text text-darken-2" href="webbackend?intention=dashboard"><i class="material-icons left">arrow_back</i>back</a>
                </div>
            </div>

            <div class="row">
                <div class="col s12 m12">
                    <div id="progess" style="display: none;">
                        <div class="progress">
                            <div class="indeterminate"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div id="enrolleeDiv" class="col s12 m12 l12 white">
                            <ul class="tabs">
                                <li class="tab col s12 m3 l3"><a class="active" href="#personal">Personal Data</a></li>
                                <li class="tab col s12 m3 l3"><a href="#employment">Employment Details</a></li>
                                <li class="tab col s12 m3 l3"><a href="#biometrics">Biometrics Information</a></li>
                                <li class="tab col s12 m3 l3"><a href="#fingerprints">Finger Prints</a></li>
                            </ul>
                            <div class="divider"></div>
                            <br>
                            <form method="post" style="padding:0;" id="enrolleeForm" action="webbackend">
                                <input type="hidden" name="intention" value="register">
                                
                                <input type="hidden" name="pensioner-id" value="<%= appPensionerBasic.getPensionerId() %>">
                                <!-- Personal Data -->
                                <div id="personal" class="row">
                                    <div class="col s12 m12 l6">
                                        <fieldset>
                                            <legend>Category</legend>
                                                <div class="col s12 m12 l12">
                                                    <label>Service Organisation</label>
                                                    <select class="browser-default" name="service-org-id">
                                                        <option value="">Select</option>
                                                        <% for (AppServiceOrganisationLookUp serviceOrg : allServiceOrganisations) { 
                                                               if (appPensionerBasic.getServiceOrgId() == serviceOrg.getServiceOrgId()) {
                                                        %>
                                                                    <option value='<%= serviceOrg.getServiceOrgId() %>' selected><%= serviceOrg.getDescription() %></option>
                                                                <% } else { %>
                                                                <option value='<%= serviceOrg.getServiceOrgId() %>'><%= serviceOrg.getDescription() %></option>
                                                        <% } } %>
                                                    </select>
                                                </div>
                                        </fieldset>
                                    </div>
                                    <div class="col s12 m12 l6">
                                        <fieldset>
                                            <legend>Retirement Savings Account (RSA) Details</legend>
                                                <div class="col s12 m12 l6">
                                                    <label>RSA Number</label>
                                                    <input type="text" name="rsa-number" value="<%= appPensionerBasic.getRsaNumber() %> ">
                                                </div>
                                                <div class="col s12 m12 l6">
                                                    <label>Pension Fund Administrator</label>
                                                    <select class="browser-default" name="pfa-id">
                                                        <option value="">Select</option>
                                                        <% for (AppPfaLookUp pfa : allPfas) {
                                                              if (appPensionerBasic.getPfaId() == pfa.getPfaId()) {
                                                        %>
                                                                  <option value='<%= pfa.getPfaId() %>' selected><%= pfa.getName() %></option>
                                                              <% } else { %>
                                                                  <option value='<%= pfa.getPfaId() %>'><%= pfa.getName() %></option>
                                                        <% } } %>
                                                    </select>
                                                </div>
                                        </fieldset>
                                    </div>
                                    <div class="col s12 m12 l12">
                                        <fieldset>
                                            <legend>Personal Data</legend>
                                            <div class="row">
                                                <div class="col s12 m12 l4">
                                                    <label>Title</label>
                                                    <select id="title" class="browser-default" name="honorific-id">
                                                        <option value="">Select</option>
                                                        <% for (AppHonorificLookUp honorific : allHonorifics) { 
                                                               if (appPensionerBasic.getHonorificId().equals(honorific.getHonorificId())) {   
                                                        %>
                                                               <option value='<%= honorific.getHonorificId() %>' selected><%= honorific.getDescription() %></option>
                                                            <% } else { %>
                                                               <option value='<%= honorific.getHonorificId() %>'><%= honorific.getDescription() %></option>
                                                        <% } } %>
                                                    </select>
                                                </div>
                                                <div class="col s12 m4 l4">
                                                    <label for="first_name">First Name</label>
                                                    <input name="first-name" id="first_name" type="text" value="<%= appPensionerBasic.getFirstName() %>" >
                                                </div>
                                                <div class="col s12 m4 l4">
                                                    <label for="middle_name">Middle Name</label>
                                                    <input name="middle-name" id="middle_name" type="text" value="<%= appPensionerBasic.getMiddleName() %>" >
                                                </div>                                       
                                                <div class="col s12 m4 l4">
                                                    <label for="surname">Surname</label>
                                                    <input name="surname" id="surname" type="text" value="<%= appPensionerBasic.getSurname() %>" >
                                                </div>
                                                <div class="col s12 m12 l4">
                                                    <label for="maiden_name">Mother's Maiden Name</label>
                                                    <input name="mother-maiden-name" id="maiden_name" type="text"  value="<%= appPensionerBasic.getMotherMaidenName() %>" >
                                                </div>

                                                <div class="col s12 m12 l4">
                                                    <label for="date_of_birth">Date of Birth</label>
                                                    <input name="date-of-birth" id="date_of_birth" type="date" class="datepicker"  <% if (appPensionerBasic.getDateOfBirth() != null) { %> value="<%= dateFormatter.format(appPensionerBasic.getDateOfBirth()) %>"  <% } %> >
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col s12 m12 l4">
                                                    <div class="col s12 m12 l12">
                                                        <label>Academic?</label>
                                                    </div>
                                                    <div class="col s12 m12 l12">
                                                        <input name="academic" value="true" type="radio" id="academic_yes" <% if (appPensionerBasic.getAcademic().equals("true")) { %> checked <% } %> />
                                                        <label for="academic_yes">Yes</label>
                                                        <input name="academic" value="false" type="radio" id="academic_no" <% if (appPensionerBasic.getAcademic().equals("false")) { %> checked <% } %> />
                                                        <label for="academic_no">No</label>
                                                    </div>
                                                </div>
                                                <div class="col s12 m12 l4">
                                                    <label>Gender</label>                                                    
                                                    <select class="browser-default" name="gender-id">
                                                        <option value="" >Select</option>
                                                        <% for (AppGenderLookUp gender : allGenders) { 
                                                               if (appPensionerBasic.getGenderId().equals(gender.getGenderId())) { 
                                                        %>
                                                                    <option value='<%= gender.getGenderId() %>' selected ><%= gender.getDescription() %></option>
                                                                <% } else { %>
                                                                    <option value='<%= gender.getGenderId() %>'><%= gender.getDescription() %></option>
                                                           <% } } %>
                                                    </select>
                                                </div>
                                                <div class="col s12 m12 l4">
                                                    <label>Marital Status</label>
                                                    <select class="browser-default" name="marital-status-id">
                                                        <option value="" >Select</option>
                                                        <% for (AppMaritalStatusLookUp maritalStatus : allMaritalStatuses) { 
                                                               if (appPensionerBasic.getMaritalStatusId().equals(maritalStatus.getMaritalStatusId())) { 
                                                        %>
                                                                    <option value='<%= maritalStatus.getMaritalStatusId() %>' selected><%= maritalStatus.getDescription() %></option>
                                                                <% } else { %>
                                                                    <option value='<%= maritalStatus.getMaritalStatusId() %>'><%= maritalStatus.getDescription() %></option>
                                                            <% } } %>
                                                    </select>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col s12 m6 l4">
                                                    <label for="c_phone">Phone No</label>
                                                    <input name="contact-phone-number" id="c_phone" type="tel" size="11" value="<%= appPensionerBasic.getContactPhoneNumber() %>" >
                                                </div>
                                                <div class="col s12 m6 l4">
                                                    <label for="c_email">Email</label>
                                                    <input name="email" id="c_email" type="email" value="<%= appPensionerBasic.getEmail() %>">
                                                </div>
                                                <div class="col s12 m6 l4">
                                                    <label for="c_address">Contact Address</label>
                                                    <input name="contact-address" id="c_address" type="text" value="<%= appPensionerBasic.getContactAddress() %>">
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>

                                <% AppPensionerEmployment appPensionerEmployment = appPensionerBasic.getPensionerEmployment(); %>
                                <!-- Employment Record -->
                                <div id="employment" class="row">
                                    <div class="col s12 m12 l12">
                                        <fieldset>
                                            <legend>Employment Record</legend>
                                            <div class="col s12 m12 l12">
                                                <div class="row">
                                                    <div class="col s12 m12 l4">
                                                        <label>Full Name of Employer</label>
                                                        <select class="browser-default" name="employer-id">
                                                            <option value="">Select</option>
                                                            <% for (AppEmployerLookUp employer : allEmployers) { 
                                                                   if (appPensionerEmployment.getEmployerId() == employer.getEmployerId()) {
                                                            %>
                                                                       <option value='<%= employer.getEmployerId() %>' selected><%= employer.getEmployerName() %></option>
                                                                   <% } else { %>
                                                                       <option value='<%= employer.getEmployerId() %>'><%= employer.getEmployerName() %></option>
                                                            <% } } %>
                                                        </select>
                                                    </div>
                                                    <div class="col s12 m12 l4">
                                                        <label>Full Address of Employer</label>
                                                        <input type="text" name="employer-address" value="<%= appPensionerEmployment.getEmployerFullAddress() %>" >
                                                    </div>
                                                    <div class="col s12 m6 l4">
                                                        <label>State of Service</label>
                                                        <select class="browser-default" name="state-of-service">
                                                            <option value="" >Select</option>
                                                            <% for (AppStateLookUp state : allStates) { 
                                                                   if (appPensionerEmployment.getStateOfService().equals(state.getStateId())) {
                                                            %>
                                                                       <option value='<%= state.getStateId() %>' selected><%= state.getName() %></option>
                                                                   <% } else { %>    
                                                                       <option value='<%= state.getStateId() %>'><%= state.getName() %></option>
                                                            <% } } %>
                                                        </select>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="col s12 m6 l3">
                                                        <label>File/Staff ID/Service Number</label>
                                                        <input type="text" name="staff-id" value="<%= appPensionerEmployment.getStaffId() %>">
                                                    </div>
                                                    <div class="col s12 m6 l3">
                                                        <label>Date of First Appointment</label>
                                                        <input name="first-appointment-date" type="date" class="datepicker" <% if (appPensionerEmployment.getFirstAppointmentDate() != null) { %> value="<%= dateFormatter.format(appPensionerEmployment.getFirstAppointmentDate()) %>" <% } %>>
                                                    </div>
                                                    <div class="col s12 m6 l3">
                                                        <label>Expected Date of Retirement</label>
                                                        <input name="expected-retirement-date" type="date" class="datepicker" <% if (appPensionerEmployment.getExpectedRetirementDate() != null) { %> value="<%= dateFormatter.format(appPensionerEmployment.getExpectedRetirementDate()) %>" <% } %>>
                                                    </div>
                                                    <div class="col s12 m6 l3">
                                                        <label>Retirement Mode</label>
                                                        <select class="browser-default" name="retirement-mode-id">
                                                            <option value="" >Select</option>
                                                            <% for (AppRetirementModeLookUp retirementMode : allRetirementModes) { 
                                                                   if (appPensionerEmployment.getRetirementModeId() == retirementMode.getRetirementModeId()) {
                                                            %>
                                                                       <option value='<%= retirementMode.getRetirementModeId() %>' selected ><%= retirementMode.getDescription() %></option>
                                                                   <% } else { %>
                                                                       <option value='<%= retirementMode.getRetirementModeId() %>'><%= retirementMode.getDescription() %></option>
                                                            <% } } %>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col s12 m12 l6">
                                                <fieldset>
                                                    <div class="row">
                                                        <div class="col s12 m7 l7">
                                                            <label>2004 Salary Structure</label>
                                                            <select class="browser-default" name="sal-structure-2004">
                                                                <option value="" >Select</option>
                                                                <% for (AppSalaryStructureLookUp salaryStructure : allSalaryStructures) {
                                                                       if (appPensionerEmployment.getSalaryStructure2004() == salaryStructure.getSalaryStructureId()) {    
                                                                %>
                                                                           <option value='<%= salaryStructure.getSalaryStructureId() %>' selected ><%= salaryStructure.getDescription() %></option>
                                                                <% } else { %>   
                                                                           <option value='<%= salaryStructure.getSalaryStructureId() %>'><%= salaryStructure.getDescription() %></option>
                                                                <% } } %>
                                                            </select>
                                                        </div>
                                                        <div class="col s12 m3 l3">
                                                            <label>GL</label>
                                                            <select class="browser-default" name="grade-level-2004">
                                                                <option value="" >-</option>
                                                                <% for (AppGlLookUp gl : allGls) { 
                                                                    if (appPensionerEmployment.getGradeLevel2004() == gl.getGlId()) {
                                                                %>
                                                                           <option value='<%= gl.getGlId() %>' selected><%= gl.getDescription() %></option>
                                                                <% } else { %>
                                                                           <option value='<%= gl.getGlId() %>'><%= gl.getDescription() %></option>
                                                                <% } } %>
                                                            </select>
                                                        </div>
                                                        <div class="col s12 m2 l2">
                                                            <label>Step</label>
                                                            <input type="number" name="step-2004" value="<%= appPensionerEmployment.getStep2004() %>">
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="col s12 m7 l7">
                                                            <label>Consolidated Salary Structure</label>
                                                            <select class="browser-default" name="sal-structure-con">
                                                                <option value="" >Select</option>
                                                                <% for (AppSalaryStructureLookUp salaryStructure : allConsolidatedSalaryStructures) { 
                                                                       if (appPensionerEmployment.getSalaryStructureConsolidated() == salaryStructure.getSalaryStructureId()) {
                                                                %>
                                                                           <option value='<%= salaryStructure.getSalaryStructureId() %>' selected><%= salaryStructure.getDescription() %></option>
                                                                <% } else { %>
                                                                    <option value='<%= salaryStructure.getSalaryStructureId() %>'><%= salaryStructure.getDescription() %></option>
                                                                <% } } %>
                                                            </select>
                                                        </div>
                                                        <div class="col s12 m3 l3">
                                                            <label>GL</label>
                                                            <select class="browser-default" name="grade-level-con">
                                                                <option value="" >-</option>
                                                                <% for (AppGlLookUp gl : allGls) { 
                                                                       if (appPensionerEmployment.getGradeLevelConsolidated() == gl.getGlId()) {
                                                                %>
                                                                           <option value='<%= gl.getGlId() %>' selected><%= gl.getDescription() %></option>
                                                                <% } else { %>
                                                                           <option value='<%= gl.getGlId() %>'><%= gl.getDescription() %></option>
                                                                <% } } %>
                                                            </select>
                                                        </div>
                                                        <div class="col s12 m2 l2">
                                                            <label>Step</label>
                                                            <input type="number" name="step-con" value="<%= appPensionerEmployment.getStepConsolidated() %>">
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="col s12 m7 l7">
                                                            <label>Enhanced Consolidated Salary Structure</label>
                                                            <select class="browser-default" name="sal-structure-ehn-con">
                                                                <option value="" >Select</option>
                                                                <% for (AppSalaryStructureLookUp salaryStructure : allEnhancedConsolidatedSalaryStructures) { 
                                                                       if (appPensionerEmployment.getEnhancedSalaryStructureConsolidated() == salaryStructure.getSalaryStructureId()) {
                                                                %>
                                                                           <option value='<%= salaryStructure.getSalaryStructureId() %>' selected><%= salaryStructure.getDescription() %></option>
                                                                <% } else { %>           
                                                                           <option value='<%= salaryStructure.getSalaryStructureId() %>'><%= salaryStructure.getDescription() %></option>
                                                                <% } } %>
                                                            </select>
                                                        </div>
                                                        <div class="col s12 m3 l3">
                                                            <label>GL</label>
                                                            <select class="browser-default" name="grade-level-ehn-con">
                                                                <option value="" >-</option>
                                                                <% for (AppGlLookUp gl : allGls) { 
                                                                    if (appPensionerEmployment.getEnhancedGradeLevelConsolidated() == gl.getGlId()) {
                                                                %>
                                                                           <option value='<%= gl.getGlId() %>' selected><%= gl.getDescription() %></option>
                                                                <% } else { %>
                                                                           <option value='<%= gl.getGlId() %>'><%= gl.getDescription() %></option>
                                                                <% } } %>
                                                            </select>
                                                        </div>
                                                        <div class="col s12 m2 l2">
                                                            <label>Step</label>
                                                            <input type="number" name="step-ehn-con" value="<%= appPensionerEmployment.getEnhancedStepConsolidated() %>">
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <div class="col s12 m12 l6">
                                                <div class="row">
                                                    <div class="col s12 m12 l12">
                                                        <input id="profPermSecGenSectionLabel" type="checkbox"  onclick="toggleProfPermSecGenSection()"/>
                                                        <label for="profPermSecGenSectionLabel"><small>ONLY FOR PROFESSORS, PERMANENT SECRETARIES, GENERALS.</small></label>
                                                    </div>
                                                </div>
                                                <fieldset id="profPermSecGenSection" disabled="true">
                                                    <div class="row">
                                                        <div class="col s12 m12 l12">
                                                            <label>Date of Appointment as PROF/PERM SEC/GENERAL</label>
                                                            <input type="date" class="datepicker" />
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="col s12 m12 l7">
                                                            <label>Rank (as at 30th June 2004)</label>
                                                            <select class="browser-default">
                                                                <option value="">Select</option>
                                                                <% for (AppRankLookUp rank : allRanks) { %>
                                                                    <option value='<%= rank.getRankId() %>'><%= rank.getDescription() %></option>
                                                                <% } %>
                                                            </select>
                                                        </div>
                                                        <div class="col s12 m12 l3">
                                                            <label>GL</label>
                                                            <select class="browser-default">
                                                                <option value="">-</option>
                                                                <% for (AppGlLookUp gl : allGls) { %>
                                                                    <option value='<%= gl.getGlId() %>'><%= gl.getDescription() %></option>
                                                                <% } %>
                                                            </select>
                                                        </div>
                                                        <div class="col s12 m12 l2">
                                                            <label>Step</label>
                                                            <input type="number" value="0">
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>

                                <!-- Biometrics Information -->
                                <div id="biometrics" class="row">
                                    <div class="col s12 m12 l6 push-l3">
                                        <fieldset>
                                            <legend>Photo</legend>
                                            <input type="hidden" name="subtype-id" value="ps" />
                                            <input type="hidden" name="type-id" value="ph" />
                                            <% if (appPensionerBasic.getPensionerBiometrics().size() > 0) { %>
                                                <input type="hidden" name="biometric-data" id="passport" value="<%= appPensionerBasic.getPensionerBiometrics().get("ps").getBiometricData() %>"/>
                                            <% } else { %>
                                                <input type="hidden" name="biometric-data" id="passport" />
                                            <% } %>
                                            <div class="row">
                                                <div class="col s12 m12 l12 center">
                                                    <label>Camera</label>
                                                    <select class="browser-default" id="cameraChoice"> </select>
                                                </div>
                                                <div class="col s12 center">
                                                    <br>
                                                    <video class="material-icons large" id="capture_video" autoplay></video>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <canvas class="material-icons large" id="capture_canvas"></canvas>
                                                    <% if (appPensionerBasic.getPensionerBiometrics().size() > 0) { %>
                                                        <img id="saved-photo" src="<%= appPensionerBasic.getPensionerBiometrics().get("ps").getBiometricData() %>" >
                                                    <% } %>
                                                    <br/> 
                                                    <a class="waves-effect waves-light btn" id="capturePhotoBtn">Capture photo</a>
                                                    <a class="waves-effect waves-light btn" id="stopCameraBtn" >Stop camera</a>
                                                </div>                                                          
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>

                                <!-- Finger Prints -->
                                <div id="fingerprints" class="row">
                                    <div class="col s12 m12 l12">
                                        <fieldset>
                                            <legend>Finger Prints</legend>
                                            <div class="row">                    
                                                <div class="col s12 m6 l3 center">
                                                    Left thumb<br>
                                                    <i class="material-icons large">fingerprint</i>
                                                </div>                  
                                                <div class="col s12 m6 l3 center">
                                                    Left index<br>
                                                    <i class="material-icons large">fingerprint</i>
                                                </div>                  
                                                <div class="col s12 m6 l3 center">
                                                    Right index<br>
                                                    <i class="material-icons large">fingerprint</i>
                                                </div>                  
                                                <div class="col s12 m6 l3 center">
                                                    Right thumb<br>
                                                    <i class="material-icons large">fingerprint</i>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                        
                                <!-- save button -->
                                <div class="row center-align">
                                    <div class="col s12 m2 l2 push-m8 push-l8">
                                        <button class="btn btn-block waves-effect waves-light" type="submit" name="action" >Save Information
                                            <i class="material-icons left">save</i>
                                        </button>
                                    </div>
                                        <div class="col s12 m2 l2 push-m8 push-l8">
                                            <button class="btn btn-block waves-effect waves-light" type="button" id="print-slip-button" onclick="generateSlip(<%= appPensionerBasic.getPensionerId() %>)" >Print Slip
                                                <i class="material-icons left">save</i>
                                            </button>
                                        </div>
                                    
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
