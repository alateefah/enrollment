<%-- 
    Document   : management
    Created on : Jul 13, 2016, 4:55:38 PM
    Author     : Lateefah
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script type="text/javascript">
    document.title = "Manage \xB7 PENCOM Enrollment";
</script>
<script type="text/javascript" src="assets/js/UserManagement.js"></script>
<%@ page import="java.util.List, java.util.ArrayList, 
                    com.intellectualapps.pencom.pojo.AppUser, 
                    com.intellectualapps.pencom.model.User,
                    com.intellectualapps.pencom.pojo.AppRole, 
                    com.intellectualapps.pencom.model.Role,
                    com.intellectualapps.pencom.pojo.AppPrivilege,
                    com.intellectualapps.pencom.model.Privilege" %>
<% 
    ArrayList<AppUser> allUsers = (ArrayList<AppUser>) request.getAttribute("system-users");
    if (allUsers == null || allUsers.size() == 0) {
        allUsers = new ArrayList<AppUser>();
    }
    
    ArrayList<AppRole> allRoles = (ArrayList<AppRole>) request.getAttribute("system-roles");
    if (allRoles == null || allRoles.size() == 0) {
        allRoles = new ArrayList<AppRole>();
    }
    
    ArrayList<AppPrivilege> allPrivileges = (ArrayList<AppPrivilege>) request.getAttribute("system-privileges");
    if (allPrivileges == null || allPrivileges.size() == 0) {
        allPrivileges = new ArrayList<AppPrivilege>();
    }
%>
<script type="text/javascript">
    window.onload = function() { handleUserManagement(); };
</script>
<style type="text/css">
    #users_list_table td, #users_list_table th {
        border-bottom: inherit;
    }
</style>
<!DOCTYPE html>
    <div class="row">
        <div class="col s12 m12">
            <a class="waves-effect waves-light btn grey lighten-2 grey-text text-darken-2" href="webbackend?intention=dashboard"><i class="material-icons left">arrow_back</i>back</a>
        </div>
    </div>
    <div class="row">
        <div class="col s12 m12">
            <div class="row">
                
                <jsp:include page="modals/deleteModal.html" />
                
                <div class="col s12 m12 l12 white">
                    <ul class="tabs">
                        <li class="tab col s12 m3 l3"><a class="active" href="#users">User Management</a></li>
                        <li class="tab col s12 m3 l3"><a href="#roles">Roles</a></li>
                        <li class="tab col s12 m3 l3"><a href="#privileges">Privileges</a></li>
                    </ul>
                    <div class="divider"></div>
                    <br>
                    <!-- User Management -->
                    <div id="users" class="row">
                        <div class="row right-align">
                            <a class="red waves-effect waves-light btn modal-trigger" href="#addUserModal">
                                <i class="large material-icons left">add</i>Add User
                            </a>
                        </div>
                        
                        <jsp:include page="modals/addUserModal.html" />
                        
                        <div class="col s12 m12 l6" id="users_list_tableContainer">
                            <form action="#">
                                <table class="bordered responsive-table tablesorter highlight display" id="users_list_table">
                                    <thead>
                                        <tr>                               
                                            <th data-field="username">S/No</th>
                                            <th data-field="username">Username</th>
                                            <th data-field="fname">First Name</th>
                                            <th data-field="oname">Other Names</th>
                                            <th data-field="actions" style="width: 20%;">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody id="users_list_tbody">                                    
                                        <% int userCounter=1; 
                                           for (AppUser user : allUsers) { 
                                        %>
                                            <tr id="<%= user.getUsername() %>" onclick='showUserRoles(this, "<%= user.getUsername() %>", <%= user.getUserRolesAsJson() %>)'>
                                                <td><%= userCounter++ %></td>
                                                <td id="<%= user.getUsername() %>-username"><%= user.getUsername() %></td>
                                                <td id="<%= user.getUsername() %>-firstname"><%= user.getFirstName() %></td>
                                                <td id="<%= user.getUsername() %>-othernames"><%= user.getOtherNames() %></td>
                                                <td>
                                                    <a class="waves-effect waves-light btn-floating blue" onClick="showUpdateUserModal('<%= user.getUsername() %>')"><i class="material-icons">edit</i></a>
                                                    <a class="waves-effect waves-light btn-floating red" onclick="showDeleteModal('deleteUser', '<%= user.getUsername() %>')"><i class="material-icons">delete</i></a>
                                                </td>
                                            </tr>
                                        <% } %>
                                    </tbody>
                                </table>
                            </form>
                        </div>                        
                        <div class="col s12 m12 l2">
                            <fieldset>
                                <legend>Roles</legend>
                                <div class="row">  
                                    <select class="browser-default multiselect-option" size="8" multiple="multiple" id="user-roles">
                                        
                                    </select>                                    
                                </div>
                            </fieldset>
                        </div>
                        <div class="col s12 m12 l2">
                            <fieldset class="center" style="width: 50%; margin-left: auto; margin-right: auto;">
                                <legend class="left-align">Action</legend>
                                <button class="btn waves-effect waves-light grey lighten-2 grey-text text-darken-2" onclick="assignAllRoles();"><i class="material-icons">fast_rewind</i></button>
                                <br><br>
                                <button class="btn waves-effect waves-light grey lighten-2 grey-text text-darken-2" onclick="assignSelectedRoles();"><i class="material-icons">navigate_before</i></button>
                                <br><br>
                                <button class="btn waves-effect waves-light grey lighten-2 grey-text text-darken-2" onclick="unassignSelectedRoles();"><i class="material-icons">navigate_next</i></button>
                                <br><br>
                                <button class="btn waves-effect waves-light grey lighten-2 grey-text text-darken-2" onclick="unassignAllRoles();"><i class="material-icons">fast_forward</i></button>
                            </fieldset>
                        </div>
                        <div class="col s3 m12 l2">
                            <fieldset>
                                <legend>Available Roles</legend>
                                <div class="row">                    
                                    <select class="browser-default multiselect-option" size="8" multiple="multiple" id="other-roles">
                                        
                                    </select>
                                </div>
                            </fieldset>
                        </div>                            
                    </div>

                    <!-- Roles -->
                    <div id="roles" class="row">
                        <div class="row right-align">
                            <a class="red waves-effect waves-light btn modal-trigger" href="#addRoleModal">
                                <i class="large material-icons left">add</i>Add Role
                            </a>
                        </div>
                        
                        <jsp:include page="modals/addRoleModal.html" />
                    
                        <div class="col s12 m12 l6">
                            <form action="#">
                                <table class="bordered responsive-table tablesorter highlight" id="roles_list_table">
                                    <thead>
                                      <tr>
                                            <th data-field="rsno">S/No</th>
                                            <th data-field="rname">Role Name</th>
                                            <th data-field="rdesc">Description</th>
                                            <th data-field="ractions" style="width: 20%;">Actions</th>
                                      </tr>
                                    </thead>
                                    <tbody id="roles_list_tbody">                                    
                                        <% int rolesCounter=1; 
                                           for (AppRole role : allRoles) { 
                                        %>
                                            <tr id="<%= role.getRoleId() %>" onclick='showRolePrivileges(this, "<%= role.getRoleId() %>", <%= role.getRolePrivilegesAsJson() %>)'>
                                                <td><%= rolesCounter++ %></td>
                                                <td id="<%= role.getRoleId() %>-roleId"><%= role.getRoleId() %></td>
                                                <td id="<%= role.getRoleId() %>-description"><%= role.getDescription() %></td>
                                                <td>
                                                    <a class="waves-effect waves-light btn-floating blue" onClick="showUpdateRoleModal('<%= role.getRoleId() %>')"><i class="material-icons">edit</i></a>
                                                    <a class="waves-effect waves-light btn-floating red" onClick="showDeleteModal('deleteRole', '<%= role.getRoleId() %>')"><i class="material-icons">delete</i></a>
                                                </td>
                                            </tr>
                                        <% } %>
                                    </tbody>                                    
                                </table>
                            </form>
                        </div>
                        <div class="col s12 m12 l2">
                            <fieldset>
                                <legend>Privileges</legend>
                                <div class="row">                    
                                    <select class="browser-default multiselect-option" size="8" multiple="multiple" id="role-privileges">
                                        
                                    </select> 
                                </div>
                            </fieldset>
                        </div>
                        <div class="col s12 m12 l2">
                            <fieldset class="center">
                                <legend class="left-align">Action</legend>
                                <button class="btn waves-effect waves-light grey lighten-2 grey-text text-darken-2" onclick="assignAllPrivileges()"><i class="material-icons">fast_rewind</i></button>
                                <br><br>
                                <button class="btn waves-effect waves-light grey lighten-2 grey-text text-darken-2" onclick="assignSelectedPrivileges()"><i class="material-icons">navigate_before</i></button>
                                <br><br>
                                <button class="btn waves-effect waves-light grey lighten-2 grey-text text-darken-2" onclick="unassignSelectedPrivileges()"><i class="material-icons">navigate_next</i></button>
                                <br><br>
                                <button class="btn waves-effect waves-light grey lighten-2 grey-text text-darken-2" onclick="unassignAllPrivileges()"><i class="material-icons">fast_forward</i></button>
                            </fieldset>
                        </div>
                        <div class="col s12 m12 l2">
                            <fieldset>
                                <legend>Available Privileges</legend>
                                <div class="row">                    
                                    <select class="browser-default multiselect-option" size="8" multiple="multiple" id="other-privileges">
                                        
                                    </select>
                                </div>
                            </fieldset>
                        </div>
                    </div>

                    <!-- Privileges -->
                    <div id="privileges" class="row">
                        <div class="row right-align">
                            <a class="red waves-effect waves-light btn modal-trigger" href="#addPrivilegeModal">
                                <i class="large material-icons left">add</i>Add Privilege
                            </a>
                        </div>
                        
                        <jsp:include page="modals/addPrivilegeModal.html" />

                        <form action="#">
                            <table class="bordered responsive-table tablesorter highlight" id="privileges_list_table">
                                <thead>
                                  <tr>
                                        <th data-field="psno">S/No</th>
                                        <th data-field="pname">Privilege Name</th>
                                        <th data-field="pdesc">Privilege Description</th>
                                        <th data-field="pmenu">Privilege Menu</th>
                                        <th data-field="pactions" style="width: 10%;">Actions</th>
                                  </tr>
                                </thead>
                                <tbody id="privileges_list_tbody">                                    
                                    <% int privilegesCounter=1; 
                                       for (AppPrivilege privilege : allPrivileges) { 
                                    %>
                                        <tr>
                                            <td><%= privilegesCounter++ %></td>
                                            <td id="<%= privilege.getPrivilegeId() %>-privilegeId"><%= privilege.getPrivilegeId() %></td>
                                            <td id="<%= privilege.getPrivilegeId() %>-description"><%= privilege.getDescription() %></td>
                                            <td id="<%= privilege.getPrivilegeId() %>-menuLink"><%= privilege.getMenuLink() %></td>
                                            <td>
                                                <a class="waves-effect waves-light btn-floating blue" onClick="showUpdatePrivilegeModal('<%= privilege.getPrivilegeId() %>')"><i class="material-icons">edit</i></a>
                                                <a class="waves-effect waves-light btn-floating red" onClick="showDeleteModal('deletePrivilege', '<%= privilege.getPrivilegeId() %>')"><i class="material-icons">delete</i></a>
                                            </td>
                                        </tr>
                                    <% } %>
                                </tbody>                                
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>