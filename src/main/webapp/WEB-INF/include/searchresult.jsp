<%-- 
    Document   : searchresult
    Created on : 26-Jul-2016, 19:01:38
    Author     : sadiq
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script type="text/javascript">
    document.title = "Search \xB7 PENCOM Enrollment";
</script>
<script type="text/javascript" src="assets/js/SearchActions.js"></script>
<!DOCTYPE html>
    <div class="row">
        <div class="col s12 m12">
            <a class="waves-effect waves-light btn grey lighten-2 grey-text text-darken-2" href="webbackend?intention=dashboard"><i class="material-icons left">arrow_back</i>back</a>
        </div>
        
        <div class="col s12 m12">
            <%@ include file="search.html" %>
        </div>                
        
        <div class="col s12 m12 l12">
            <div id="progess" style="display: none;">
                <div class="progress">
                    <div class="indeterminate"></div>
                </div>
            </div>
            <div class="col s12 m12 l12 white" id="result_list_tableContainer">      
                <table class="striped responsive-table tablesorter" id="result-list">
                    <thead>
                        <tr>                               
                            <th>S/No.</th>
                            <th>Pensioner Id.</th>
                            <th>First Name</th>
                            <th>Middle Name</th>
                            <th>Surname</th>
                            <th>Phone Number</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody id="result-list-tbody">
                    </tbody>

                </table>
            </div>
        </div>
    </div>