<%-- 
    Document   : header
    Created on : Jul 4, 2016, 11:09:41 PM
    Author     : buls
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.List, java.util.ArrayList, com.intellectualapps.pencom.pojo.AppRole, com.intellectualapps.pencom.pojo.AppPrivilege" %>
<!DOCTYPE html>
    <% 
        List<AppRole> userRoles = (List<AppRole>) request.getAttribute("privileges");
        if (userRoles == null) {
            userRoles = new ArrayList<AppRole>();
        }
        
        String intention = (String) request.getAttribute("intention");
        if (intention == null) {
            intention = "";
        }
    %>
<header>
    <nav class="top-nav grey lighten-2">
        <div class="nav-wrapper">
            <a class="page-title" style="margin-left: 10px; color: black;">PENCOM</a>
            <ul class="right hide-on-med-and-down">
                <li>
                    <a href="webbackend?intention=logout" class="grey-text text-darken-3"><i class="material-icons left" style="font-size: 1.3rem !important;">power_settings_new</i>Log out</a>
                </li>
            </ul>
      </div>      
    </nav>
    <div class="container">
        <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
    </div>
    <ul id="nav-mobile" class="side-nav fixed grey lighten-2">
        <li class="logo">
            <a id="logo-container" href="#">
                <img id="front-page-logo" class="responsive-img" src="assets/images/logo.png" style="height: 80px;" alt="PENCOM LOGO">
            </a>
        </li>
        <li class="no-padding">
            <ul class="collapsible collapsible-accordion">
            <% for (AppRole userRole : userRoles) { %>
                <li class="bold">
                    <a class="collapsible-header waves-effect waves-grey">
                        <i class="material-icons left">people</i><%= userRole.getRoleId() %>
                    </a>
                    <%  int count = 0;
                        for (AppPrivilege userPrivilege : userRole.getPrivileges()) { 
                            if (count == 0) { %>
                            <div class="collapsible-body">
                                <ul>
                                    <%}%>
                                    <li <% if (intention.equals(userPrivilege.getMenuLink())) {%> class="active" <%}%> ><a href="webbackend?intention=<%=userPrivilege.getMenuLink()%>" ><%= userPrivilege.getPrivilegeId() %></a></li>
                                    <% if (count == (userRole.getPrivileges().size() - 1)) { %> 
                                </ul>
                            </div>
                        <% } count++; }%>
                </li>
                <% } %>
            </ul>
        </li>        
    </ul>
</header>