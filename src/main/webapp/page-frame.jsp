<%-- 
    Document   : page-frame
    Created on : Jul 3, 2016, 11:44:44 AM
    Author     : buls
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Dashboard &middot; PENCOM Enrollment</title>
        <link href="favicon.ico" rel="shortcut icon" type="image/x-icon">
        
        <!--Import Google Icon Font-->
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="assets/css/materialize.css" media="screen,projection">
        <link type="text/css" rel="stylesheet" href="assets/css/ghpages-materialize.css" media="screen,projection">
        <!-- Other styles -->
        <link type="text/css" rel="stylesheet" href="assets/css/style.css" media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="assets/css/inputMods.css" media="screen,projection">
        <link rel="stylesheet" type="text/css" href="assets/css/datatables.min.css"/> 
        
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <style type="text/css"></style>
    </head>
    <% 
        String main = (String) request.getAttribute("main");
        if (main == null) {
            main = "";
        }
    %>
    <body class="bg">
        <jsp:include page="WEB-INF/template/header.jsp" />        
                
        <main>            
            <jsp:include page="<%= main %>" />            
        </main>
        
        <%@ include file="WEB-INF/template/footer.html" %>
        
        <!--Import jQuery before materialize.js-->
        <script type="text/javascript" src="assets/js/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="assets/js/materialize.js"></script>
        <script type="text/javascript" src="assets/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/js/init.js"></script>
        <div class="drag-target" style="left: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></div>   
        <div class="hiddendiv common"></div>
    </body>
</html>
